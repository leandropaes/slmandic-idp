<?php
	include_once("../includes/topo.php");
	$Forbidden = $I_Access['A_Matricula'];
	if (!$Forbidden || $Forbidden < 1) {
		header("Location: ../user/403.php");
	}
	
	$vetor_estados = array(
		'AC'=>'','AL'=>'','AM'=>'','AP'=>'','BA'=>'','CE'=>'','DF'=>'','ES'=>'','GO'=>'',
		'MA'=>'','MG'=>'','MS'=>'','MT'=>'','PA'=>'','PB'=>'','PE'=>'','PI'=>'','PR'=>'',
		'RJ'=>'','RN'=>'','RO'=>'','RR'=>'','RS'=>'','SC'=>'','SE'=>'','SP'=>'','TO'=>''
	);
	
	// recupera a acao desejada
	$chave = trim($_REQUEST['chave']);
	$acao = trim($_REQUEST['acao']);
	$submit = trim($_REQUEST['submit']);
	$id_didocurso = max(0,trim($_REQUEST['curso']));
	$botao_salvar = 'Salvar';
	
	$dados = array();
	
	$dados['str_nome'] = trim($_REQUEST['str_nome']);
	$dados['str_email'] = trim($_REQUEST['str_email']);
	$dados['str_cpf'] = trim($_REQUEST['str_cpf']);
	$dados['str_telefone_1'] = trim($_REQUEST['str_telefone_1']);
	$dados['str_telefone_2'] = trim($_REQUEST['str_telefone_2']);
	$dados['str_telefone_3'] = trim($_REQUEST['str_telefone_3']);
	$dados['id_didocurso'] = trim($_REQUEST['id_didocurso']);
	$dados['str_unidadefederativa'] = trim($_REQUEST['str_unidadefederativa']);
	$dados['num_logradouro'] = trim($_REQUEST['num_logradouro']);
	$dados['str_cep'] = trim($_REQUEST['str_cep']);
	$dados['str_logradouro'] = trim($_REQUEST['str_logradouro']);
	$dados['str_complemento'] = trim($_REQUEST['str_complemento']);
	$dados['str_bairro'] = trim($_REQUEST['str_bairro']);
	$dados['str_cidade'] = trim($_REQUEST['str_cidade']);
	
	if ($dados['id_didocurso'] > 0) {
		$id_didocurso = $dados['id_didocurso'];
	}
	// se enviou
	if (strtolower($submit) == strtolower($botao_salvar)) {
		if ($acao == 'C') {
			#insere os dados do matriculado
			#insere o boleto no banco
			#abre popup para impress�o do boleto
			#dispara email com link para 
			#volta para a index
			/* sql revisado */
			$comando = "
				INSERT INTO
					tcad_matriculas (
						str_nome,
						str_email,
						str_cpf,
						str_telefone_1,
						str_telefone_2,
						str_telefone_3,
						id_didocurso,
						str_unidadefederativa,
						num_logradouro,
						str_cep,
						str_logradouro,
						str_complemento,
						str_bairro,
						str_cidade
					)
				VALUES (
					'".$dados['str_nome']."',
					'".$dados['str_email']."',
					'".$dados['str_cpf']."',
					'".$dados['str_telefone_1']."',
					'".$dados['str_telefone_2']."',
					'".$dados['str_telefone_3']."',
					".max(0,$dados['id_didocurso']).",
					'".$dados['str_unidadefederativa']."',
					'".$dados['num_logradouro']."',
					'".$dados['str_cep']."',
					'".$dados['str_logradouro']."',
					'".$dados['str_complemento']."',
					'".$dados['str_bairro']."',
					'".$dados['str_cidade']."'
				);
			";
			$consulta = mssql_query($comando, $dbintranet) or die('<pre>'.$comando.'</pre><br/>'.mssql_get_last_message());
			
            /* sql revisado */
			$comando = "select SCOPE_IDENTITY() AS id;";
			$consulta = mssql_query($comando, $dbintranet) or die('<pre>'.$comando.'</pre><br/>'.mssql_get_last_message());
			
			$id_matricula = mssql_fetch_assoc($consulta);
			$id_matricula = $id_matricula['id'];

			#header('location: boleto_itau.php?acao=C&matricula='.$id_matricula.'&dicurso='.$dados['id_didocurso']);
			header('location: ./../cursos/cursos_view.php?id='.$dados['id_didocurso'].'&query=&rw=nm');
			exit;
		}
		else if ($acao == 'U') {
        /* sql revisado */
			$comando = "
				UPDATE tcad_matriculas SET
					str_nome = '".$dados['str_nome']."',
					str_email = '".$dados['str_email']."',
					str_cpf = '".$dados['str_cpf']."',
					str_telefone_1 = '".$dados['str_telefone_1']."',
					str_telefone_2 = '".$dados['str_telefone_2']."',
					str_telefone_3 = '".$dados['str_telefone_3']."',
					-- id_didocurso = ".max(0,$dados['id_didocurso']).",
					str_unidadefederativa = '".$dados['str_unidadefederativa']."',
					num_logradouro = '".$dados['num_logradouro']."',
					str_cep = '".$dados['str_cep']."',
					str_logradouro = '".$dados['str_logradouro']."',
					str_complemento = '".$dados['str_complemento']."',
					str_bairro = '".$dados['str_bairro']."',
					str_cidade = '".$dados['str_cidade']."'
				WHERE
					id_matricula = ".max(0, $chave)."
					;
			";
			$consulta = mssql_query($comando, $dbintranet) or die('<pre>'.$comando.'</pre><br/>'.mssql_get_last_message());
			
		}
	}
    /* sql revisado */
	// se foi enviado o formul�rio
	#if (isset($_POST['submit'])) { print_r($_POST); }
	if ($acao == 'U') {
		$comando = "
			select *
			from tcad_matriculas
			where id_matricula = ".max(0, $chave)."
		";
		$consulta = mssql_query($comando, $dbintranet) 
			or die('<pre>'.$comando.'</pre><br/>'.mssql_get_last_message());
		$dados = mssql_fetch_assoc($consulta);
	}
	// l�nha de t�tulo
	print(Titulo('Nova Matr�cula', null));
?>
	<style type="text/css">
		tr.topo {
			background-color: #CFCFCF;
		}
		tr.clara {
		}
		tr.escura {
			background-color: #DFDFDF;
		}
		
		table.formulario tbody tr th {
			text-align: right;
		}
	</style>
	<br />
<form method="post" action="?acao=<?=$acao?>&curso=<?=$id_didocurso?>&chave=<?=$chave?>">
	
	<table cellspacing="1" cellpadding="0" border="0" style="width: 100%;" class="formulario">
		<tbody>
			<tr>
				<th>Curso:</th>
				<td>
<!-- INICIO CURSO -->
<?php
	// Inicio dos filtros dos cursos
	if ($Forbidden != 1)
	$Sql_Search = "AND StatusView >= 2 ";

	if (isset($_GET['query']) && $_GET['query'])
	{
		$query = @sql_slashes($_GET['query']);

		if (strlen($query) > 0)
		{
			$Palavras = str_replace(",", " ", $query);
			$Palavras = explode(" ", $Palavras);
            $NewQuery = "";

			for ($l = 0; $l < count($Palavras); $l++)
			{
				$PalavraNum = $Palavras[$l];

				$tmp1 = $Palavras[$l];
				$tmp2 = explode("-", $tmp1, 2);
				$tmp3 = explode("-", $tmp1, 2);

				if (is_numeric($PalavraNum) && $Palavras[$l] > 0)
				{
					if (!$NQ_num)
					{
						$NewQuery.= "AND (cur.ID = '".$Palavras[$l]."')";
						$NQ_num = 1;
					} else {
						$NewQuery.= "OR (cur.ID = '".$Palavras[$l]."')";
					}

				} else if ($tmp2[0] && $tmp2[1] && is_numeric($tmp3[0]) && is_numeric($tmp3[1]) && ($tmp2[0] < $tmp2[1]))
				{
					if (!$NQ_num)
					{
						$NewQuery.= "AND (cur.ID >= '".$tmp2[0]."' AND cur.ID <= '".$tmp2[1]."')";
						$NQ_num = 1;
					} else {
						$NewQuery.= "OR (cur.ID >= '".$tmp2[0]."' AND cur.ID <= '".$tmp2[1]."')";
					}

				} else if (strlen($Palavras[$l]) > 2)
				{
                    $NewQuery.= "AND (";
						$NewQuery.= "niv.Nivel LIKE ('%".$Palavras[$l]."%') OR ";
						$NewQuery.= "area.Area LIKE ('%".$Palavras[$l]."%') OR ";

						$NewQuery.= "cur.Coord_Programa LIKE ('%".$Palavras[$l]."%') OR ";
						$NewQuery.= "cur.Coord_Nome LIKE ('%".$Palavras[$l]."%') OR ";
						$NewQuery.= "cur.Coord_Titulo LIKE ('%".$Palavras[$l]."%') OR ";
						$NewQuery.= "cur.Coord_Telefone LIKE ('%".$Palavras[$l]."%') OR ";
						$NewQuery.= "cur.Coord_Email LIKE ('%".$Palavras[$l]."%') OR ";

						$NewQuery.= "cur.Minicurriculo LIKE ('%".$Palavras[$l]."%') OR ";
						$NewQuery.= "cur.CorpoDocente LIKE ('%".$Palavras[$l]."%') OR ";
						$NewQuery.= "cur.DocentesConvidados LIKE ('%".$Palavras[$l]."%') OR ";
						$NewQuery.= "cur.LinhaPesquisa LIKE ('%".$Palavras[$l]."%') OR ";

						$NewQuery.= "cur.Curso_Natureza LIKE ('%".$Palavras[$l]."%') OR ";
						$NewQuery.= "cur.Curso_Inicio LIKE ('%".$Palavras[$l]."%') OR ";

						reset($BR_Meses);
						for($iBR_M = 1; $iBR_M <= 12; $iBR_M++)
						{
							if (strtoupper($Palavras[$l]) == strtoupper($BR_Meses[$iBR_M]))
								$NewQuery.= "cur.Curso_Inicio LIKE ('%-".sprintf("%02d", $iBR_M)."-%') OR ";
						}

						$NewQuery.= "cur.Curso_Periodicidade LIKE ('%".$Palavras[$l]."%') OR ";

						$NewQuery.= "cur.Objetivos LIKE ('%".$Palavras[$l]."%') OR ";
						$NewQuery.= "cur.Programacao LIKE ('%".$Palavras[$l]."%') OR ";
						$NewQuery.= "cur.Atrativos LIKE ('%".$Palavras[$l]."%') OR ";
						$NewQuery.= "cur.Tecnicas LIKE ('%".$Palavras[$l]."%') OR ";
						$NewQuery.= "cur.Requisitos LIKE ('%".$Palavras[$l]."%') OR ";
						$NewQuery.= "cur.Material LIKE ('%".$Palavras[$l]."%') OR ";
						$NewQuery.= "cur.Informacoes LIKE ('%".$Palavras[$l]."%')";
                    $NewQuery.= ")";
				}
			}

			$Sql_Search = "AND (
				1 = 1 ".$NewQuery."
			)";
		}
	}
	
	// Restricao de cursos � ADM
	if ($Forbidden != 1 && $Forbidden != 3)
	{
		$NewQuery .= "AND StatusView > 1";
	}

	// Inicio da ordenacao dos cursos
	# ALTERA��O DA ORDEM --- $Sql_Order = "niv.Nivel ASC, area.Area ASC";
	# ALTERA��O DA ORDEM --- $Sql_Order = "uni.Sigla ASC, cur.StatusCurso ASC, niv.Nivel ASC, cur.Curso_Inicio ASC";
	# ALTERA��O DA ORDEM --- $Sql_Order = "uni.Sigla ASC, niv.Nivel ASC";
	$Sql_Order = "uni.Sigla ASC, niv.Nivel ASC, area.Area ASC";

	if (isset($_GET['ordby']) && $_GET['ordby'])
	{
		switch($_GET['ordby'])
		{
			case "data":
				$Sql_Order = "cur.CreatedData ASC, cur.CreatedHora ASC, niv.Nivel ASC, area.Area ASC";
				break;

			case "id":
				$Sql_Order = "cur.ID ASC";
				break;

			case "period":
				$Sql_Order = "cur.Curso_Periodicidade ASC";
				break;

			case "inicio":
				$Sql_Order = "cur.Curso_Inicio ASC";
				break;

			case "unidade":
				$Sql_Order = "uni.Sigla ASC, niv.Nivel ASC, area.Area ASC";
				break;
			
			case "status":
				$Sql_Order = "uni.Sigla ASC, cur.StatusCurso ASC";
				break;
		}
	}
	$condicao_unidade = '';
	$condicao_unidade_valor = '';
	if (isset($_REQUEST['conduni']) != false){
		if ($_REQUEST['conduni'] != '*') {
			$condicao_unidade = " AND uni.ID = ". $_REQUEST['conduni'] ;
			$condicao_unidade_valor = $_REQUEST['conduni'];
		}
	}
	
    if (($I_ID == FABIOLA_MOURA) || ($I_ID == BELO_HORIZONTE)) {
		$condicao_unidade = " AND uni.ID = 2" ;
    }
    if($I_Access['a_unidade'] > 0) {
        $condicao_unidade .= " AND uni.id = ".$I_Access['a_unidade'];
    }
	/* sql revisado */
	// Execucao da query dos cursos
	$Sql_Manager = mssql_query("
	SELECT
		cur.ID, niv.Nivel, area.Area, cur.CreatedData, cur.CreatedHora,
		cur.Coord_Nome, cur.Curso_Alunos_Min, cur.Curso_Alunos_Max, cur.Curso_Periodicidade, cur.Curso_Inicio,
		cur.StatusView, cur.StatusCurso, cur.SophiA_TID, uni.Sigla,
		( SELECT COUNT(*) FROM cursos_site_inscricoes AS insc WHERE insc.Curso = cur.ID ) AS Inscricoes,
		sTur.Al_Vig, sTur.Al_Max, cur.Curso_Duracao, cur.Curso_Data_Dias
	FROM cursos_data AS cur
		INNER JOIN cursos_areas AS area ON cur.Area = area.ID
		INNER JOIN cursos_niveis AS niv ON cur.Nivel = niv.ID
		INNER JOIN slm_unidades AS uni ON cur.Unidade = uni.ID ".$condicao_unidade."
		LEFT JOIN sophia_turmas AS sTur ON sTur.TID = cur.SophiA_TID
	WHERE cur.ID = ".$id_didocurso." AND cur.Ativo = 1 ".$Sql_Search." AND uni.Ativo = 1 ORDER by ".$Sql_Order.";", $dbintranet);
?>
					<input type="hidden" name="id_didocurso" value="<?=$id_didocurso?>" />
<?php
	$vetor_cursos = mssql_fetch_assoc($Sql_Manager);
?>
						<strong>DI: <?=$vetor_cursos['ID']?></strong><br/>
						<strong><?=$vetor_cursos['Nivel']?></strong> em <strong><?=$vetor_cursos['Area']?></strong><br/>
						<?=$vetor_cursos['Coord_Nome']?>
<!-- TERMINO CURSO -->
				</td>
			</tr>
<?php
	if ($acao == 'U') {
?>
			<tr>
				<th>Matr�cula:</th>
				<td>
					<strong><?=$dados['id_matricula']?></strong>
				</td>
			</tr>
<?php
	}
?>
			<tr>
				<th>Nome:</th>
				<td>
					<input type="text" value="<?=$dados['str_nome']?>" name="str_nome" size="50" />
				</td>
			</tr>
			<tr>
				<th>E-mail:</th>
				<td>
					<input type="text" value="<?=$dados['str_email']?>" name="str_email" size="50" />
				</td>
			</tr>
			<tr>
				<th>CPF:</th>
				<td>
					<input type="text" value="<?=$dados['str_cpf']?>" name="str_cpf" size="15" />
				</td>
			</tr>
			<tr>
				<th>Telefone Residencial:</th>
				<td>
					<input type="text" value="<?=$dados['str_telefone_1']?>" name="str_telefone_1" size="15" />
				</td>
			</tr>
			<tr>
				<th>Telefone Comercial:</th>
				<td>
					<input type="text" value="<?=$dados['str_telefone_2']?>" name="str_telefone_2" size="15" />
				</td>
			</tr>
			<tr>
				<th>Celular:</th>
				<td>
					<input type="text" value="<?=$dados['str_telefone_3']?>" name="str_telefone_3" size="15" />
				</td>
			</tr>
			<tr>
				<th>CEP:</th>
				<td>
					<input type="text" value="<?=$dados['str_cep']?>" name="str_cep" size="10" />
				</td>
			</tr>
			<tr>
				<th>Logradouro:</th>
				<td>
					<input type="text" value="<?=$dados['str_logradouro']?>" name="str_logradouro" size="50" />
				</td>
			</tr>
			<tr>
				<th>N�mero:</th>
				<td>
					<input type="text" value="<?=$dados['num_logradouro']?>" name="num_logradouro" size="8" />
				</td>
			</tr>
			<tr>
				<th>Complemento:</th>
				<td>
					<input type="text" value="<?=$dados['str_complemento']?>" name="str_complemento" size="35" />
				</td>
			</tr>
			<tr>
				<th>Bairro:</th>
				<td>
					<input type="text" value="<?=$dados['str_bairro']?>" name="str_bairro" size="40" />
				</td>
			</tr>
			<tr>
				<th>Cidade:</th>
				<td>
					<input type="text" value="<?=$dados['str_cidade']?>" name="str_cidade" size="40" />
				</td>
			</tr>
			<tr>
				<th>Estado:</th>
				<td>
<?php
	$vetor_estados[$dados['str_unidadefederativa']] = 'selected="selected"';
?>
					<select name="str_unidadefederativa">
						<option value="AC" <?=$vetor_estados['AC']?>>Acre</option>
						<option value="AL" <?=$vetor_estados['AL']?>>Alagoas</option>
						<option value="AP" <?=$vetor_estados['AP']?>>Amap�</option>
						<option value="AM" <?=$vetor_estados['AM']?>>Amazonas</option>
						<option value="BA" <?=$vetor_estados['BA']?>>Bahia</option>
						<option value="CE" <?=$vetor_estados['CE']?>>Cear�</option>
						<option value="DF" <?=$vetor_estados['DF']?>>Distrito Federal</option>
						<option value="ES" <?=$vetor_estados['ES']?>>Esp�rito Santo</option>
						<option value="GO" <?=$vetor_estados['GO']?>>Goias</option>
						<option value="MA" <?=$vetor_estados['MA']?>>Maranh�o</option>
						<option value="MT" <?=$vetor_estados['MT']?>>Mato Grosso</option>
						<option value="MS" <?=$vetor_estados['MS']?>>Mato Grosso do Sul</option>
						<option value="MG" <?=$vetor_estados['MG']?>>Minas Gerais</option>
						<option value="PA" <?=$vetor_estados['PA']?>>Par�</option>
						<option value="PB" <?=$vetor_estados['PB']?>>Para�ba</option>
						<option value="PR" <?=$vetor_estados['PR']?>>Paran�</option>
						<option value="PE" <?=$vetor_estados['PE']?>>Pernambuco</option>
						<option value="PI" <?=$vetor_estados['PI']?>>Piau�</option>
						<option value="RJ" <?=$vetor_estados['RJ']?>>Rio de Janeiro</option>
						<option value="RN" <?=$vetor_estados['RN']?>>Rio Grande do Norte</option>
						<option value="RS" <?=$vetor_estados['RS']?>>Rio Grande do Sul</option>
						<option value="RO" <?=$vetor_estados['RO']?>>Rond�nia</option>
						<option value="RR" <?=$vetor_estados['RR']?>>Roraima</option>
						<option value="SC" <?=$vetor_estados['SC']?>>Santa Catarina</option>
						<option value="SP" <?=$vetor_estados['SP']?>>S�o Paulo</option>
						<option value="SE" <?=$vetor_estados['SE']?>>Sergipe</option>
						<option value="TO" <?=$vetor_estados['TO']?>>Tocantins</option>
					</select>
				</td>
			</tr>
		</tbody>
	</table>
	
	<p style="text-align: center;">
		<input type="submit" name="submit" value="<?=$botao_salvar?>" />
	</p>
	
</form>
</div>
<?php 
	include_once("../includes/rodape.php");
?>
