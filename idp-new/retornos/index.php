<!DOCTYPE HTML>
   <html lang="pt-br">
   <head>
   	<meta charset="UTF-8">
  </head>
  <body>
<?php
require_once('../inscricao/__lib__.php');

session_start();

if (!isset($_SESSION['sessao_logada'])) {
    header('location: login.php');
    exit();
}



if (!ini_get('safe_mode')) {
    ini_set('error_reporting', E_ALL);
    ini_set('display_errors', true);
}

$p = isset($_GET['p'])?$_GET['p']:null;
$r = isset($_GET['r'])?$_GET['r']:null;

if (isset($_POST['enviado']) && $_POST['enviado'] == '1') {
    $arquivo = $_FILES['arquivo'];
    if ($arquivo['error'] == 0) {
        if (file_exists('./conteiner/' . $arquivo['name'])) {
            header('location: index.php?p=e');
            exit;
        }
        if (!move_uploaded_file($arquivo['tmp_name'], './conteiner/' . $arquivo['name'])) {
            header('location: index.php?p=m');
            exit;
        }
        header('location: index.php?p=s');
        exit;
    }
}
?>
<style type="text/css">
    .e { background-color: #FFCFCF; padding: 5px 3px; font-weight: bold; border: 1px solid #333333; }
    .a { background-color: #FFF4BF; padding: 5px 3px; font-weight: bold; border: 1px solid #333333; }
    .s { background-color: #CFFFD1; padding: 5px 3px; font-weight: bold; border: 1px solid #333333; }
</style>
<script type="text/javascript">
    function exibe(str_arquivo, pgtos) {
    
        var obj = '';
        for (var i = 0; i <= pgtos; i++) {
            obj = document.getElementById('d_'+str_arquivo+'_'+i);
            if (obj.style.display == '') {
                obj.style.display = 'none';
            }
            else {
                obj.style.display = '';
            }
        }
    }
</script>

<?php
if ($r == 's') {
    require_once 'identifica-vestibular.php';
}
?>
<p><a href="logout.php">sair</a></p>
<fieldset>
    <legend>ENVIO DE ARQUIVOS</legend>
<?php
switch ($p) {
    case 'e': print '<p class="e">O arquivo já existe no servidor.</p>';
        break;
    case 'm': print '<p class="e">O arquivo não pode ser salvo, tente novamente em instantes.</p>';
        break;
    case 's': print '<p class="s">O arquivo foi salvo com sucesso.</p><p class="a">O arquivo ainda não foi processado.</p>';
        break;
}
?>
    <form action="?" method="post" enctype="multipart/form-data">
        <input type="hidden" name="enviado" value="1" />
        <p><label for="arquivo">Arquivo retorno:</label> <input type="file" name="arquivo" /> <input type="submit" value="Salvar" /> <span style="float:right;"><input type="button" value="Processar Arquivo(s)" onclick="window.location='index.php?r=s'" /></span></p>
    </form>
</fieldset>

<?php
//exit();
?>

<table border="0" cellpadding="2" cellspacing="1" style="width: 100%;">
    <caption>Arquivos Processados</caption>
    <tbody>

<?php
$comando = "
   
   SELECT 	    
		f.str_arquivo, f.dt_arquivo, f.num_pagamentos, f.dt_identificado_em,
		i.num_inscricao, m.str_pessoa_nome, m.str_pessoa_cpf,
		p.val_valor_pago, p.val_valor_credito, p.dt_pagamento,f.id_arquivoretorno, s.str_processo_nome as origem

	FROM proBoletosRetorno AS f

	LEFT JOIN proBoletosPagamentos AS p
	ON p.id_arquivoretorno = f.id_arquivoretorno
	INNER JOIN proInscricoes AS i
	ON i.num_inscricao = p.num_inscricao
	INNER JOIN proPessoasFisicas AS m
	ON m.id_pessoafisica = i.id_pessoafisica
	INNER JOIN proProcessos as s ON
	s.id_processo = i.id_processo
    
    ORDER BY id_arquivoretorno DESC, dt_identificado_em DESC, str_arquivo DESC, num_inscricao ASC
";
$consulta = mysql_query($comando);
$arquivo_nome = '';
$cores = array('#DFDFFF', '#EFEFFF', '#DFDFDF');
$fundo = array('#DFFFDF', '#EFFFEF');
$j = 0;
while ($campos = mysql_fetch_array($consulta)) {
    if ($arquivo_nome != $campos['str_arquivo']) {
        $i = 0;
        $arquivo_nome = $campos['str_arquivo'];
        print '<tr style="background-color: ' . $fundo[++$j % 2] . ';"><td colspan="8" style="padding: 8px; cursor: pointer;" onclick="exibe(\'' . $arquivo_nome . '\', \'' . $campos['num_pagamentos'] . '\');"><b>' . $arquivo_nome . '</b> em ' . $campos['dt_arquivo'] . ' (' . $campos['num_pagamentos'] . ' pagamento(s))</td></tr>';
        ?>
                <tr id="d_<?= $campos['str_arquivo'] ?>_0" style="display: none; background-color: <?= $cores[2] ?>;">
                    <th colspan="2">NOME</th>
                    <th>INSCRI&Ccedil;&Atilde;O</th>
                    <th>CPF</th>
                    <th>ORIGEM</th>
                    <th>VALOR PAGO</th>
                    <th>CREDITADO</th>
                    <th>PAGO EM</th>
                </tr>
        <?php
    }

    $campos['val_valor_pago']    = number_format($campos['val_valor_pago'] / 100, 2, ',', '.');
    $campos['val_valor_credito'] = number_format($campos['val_valor_credito'] / 100, 2, ',', '.');
    ++$i;
    ?>
            <tr id="d_<?= $campos['str_arquivo'] ?>_<?= $i ?>" style="display: none; background-color: <?= $cores[$i % 2] ?>;">
                <th style="text-align: right;"><span style="float: left;">#</span><?= $i ?></th>
                <td><?= utf8_encode($campos['str_pessoa_nome']) ?></td>
                <td><?= $campos['num_inscricao'] ?></td>
                <td><?= $campos['str_pessoa_cpf'] ?></td>
                <td style="text-align: center;"><?= utf8_encode($campos['origem']) ?></td>
                <td style="text-align: right;"><span style="float: left;">R$</span><?= $campos['val_valor_pago'] ?></td>
                <td style="text-align: right;"><span style="float: left;">R$</span><?= $campos['val_valor_credito'] ?></td>
                <td style="text-align: right;"><?= implode('/', array_reverse(explode('-', substr($campos['dt_pagamento'], 0, 10)))); ?></td>
            </tr>
    <?php
}
?>
</table>
        </body>
		
		</html>
