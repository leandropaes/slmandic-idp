
	// ----------------------------------------------------------------------
	// LOGIN PARA INICIAR INSCRICAO
	// ----------------------------------------------------------------------
	
	$("input[name='id_grupo']").click(function() {
		
		$("#msg_grupo").hide("slow");

	});
	
	$("#formGrupos").submit(function(event) {
	

		var id_inscricao 	= $("#id_inscricao").val();
		var id_grupo 		= $("input[name='id_grupo']:checked").val();

		// Considero que a validação esta OK
		var eValido = true;
		
		// Valido os Campos
		
		if (!id_grupo)	{ 
			eValido = false; 
			$("#msg_grupo").show("slow");
		}


		// Se for valido então salvo no banco
		if(eValido){
		
			$("#carregando_grupos").css("display", "inline");
		
			$.post('do/crud/updateGrupos.php', {id_inscricao: id_inscricao, id_grupo: id_grupo }, function(resposta) {
					
				if(resposta != false){
					
					//atualizo a pagina
					location.reload();

				}

			});
		
		}
		else{
			$("#carregando_grupos").css("display", "none");
		}
	
		event.preventDefault();
		
	});
