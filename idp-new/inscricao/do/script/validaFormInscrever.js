
	// ----------------------------------------------------------------------
	// LOGIN PARA INICIAR INSCRICAO
	// ----------------------------------------------------------------------
	
	$("#formInscrever").submit(function(event) {
	
		$("#carregando_begin1").css("display", "inline");
	
		// Recupero so valores
		var id_curso = $("#id_curso").val();	
		var str_cpf  = $("#str_cpf").val();	
		var id_processo  = $("#id_processo").val();	
		var txt_ano  = $("#txt_ano").val();	
		
		// Considero que a validação esta OK
		var eValido = true;
		
		// Valido os Campos
		
		if (!id_curso)	{ eValido = false; $("#id_curso").css("border", "1px solid red");} else{$("#id_curso").css("border", "1px solid #afafaf");}
		if (!str_cpf)	{ eValido = false; $("#str_cpf").css("border", "1px solid red");} 
		
		else{
			// ele não é nulo
			$("#str_cpf").css("border", "1px solid #afafaf");
		
			// verifico se ele é valido
			var value = str_cpf;
			value = value.replace('.','');  
			value = value.replace('.','');  
			cpf = value.replace('-','');  
			while(cpf.length < 11) cpf = "0"+ cpf;  
			var expReg = /^0+$|^1+$|^2+$|^3+$|^4+$|^5+$|^6+$|^7+$|^8+$|^9+$/;  
			var a = [];  
			var b = new Number;  
			var c = 11;  
			for (i=0; i<11; i++){  
				a[i] = cpf.charAt(i);  
				if (i < 9) b += (a[i] * --c);  
			}  
			if ((x = b % 11) < 2) { a[9] = 0 } else { a[9] = 11-x }  
			b = 0;  
			c = 11;  
			for (y=0; y<10; y++) b += (a[y] * c--);  
			if ((x = b % 11) < 2) { a[10] = 0; } else { a[10] = 11-x; }  
			if ((cpf.charAt(9) != a[9]) || (cpf.charAt(10) != a[10]) || cpf.match(expReg)){ 
				eValido = false; $("#str_cpf").css("border", "1px solid red");
				$('#msg-inscrever').html("* CPF Inv&aacute;lido");
			} 
			else{
				$("#str_cpf").css("border", "1px solid #afafaf");	
				$('#msg-inscrever').html("");
			}
		
		}
		

		// Se for valido então salvo no banco
		if(eValido){
		
			
		
			$.post('do/crud/selectCPF.php', {str_cpf: str_cpf }, function(resposta) {
					
				if(resposta != false){
					// removo o formulario atual
					$("#divformInscrever").remove();
					
					// mostro o novo formulario
					$("#divformInscreverSenha").show();
					
					// Atualizo o valor dos campos
					$("#id_curso").val(id_curso);
					$("#str_cpf").val(str_cpf);
					
					// Dou foco no campo senha
					$("#str_senha").css("border", "1px solid red");
					$("#str_senha").focus();
					
					//Recupero a mascara
					$('#str_cpf').mask('999.999.999-99');

					// Imprimo mensagem de já cadastrado
					$("#bemvindo").css("display", "inline");
					$("#bemvindo").html("<center><p>Seja Bem-vindo!<br/>"+resposta+"</p></center>");
					
					$("#carregando_begin1").css("display", "none");

				}
				
				else{
					// tenho que redirecionar para  apgina pois,
					// não tem cadastro e vai iniciar o processo
					
					// Mostro barra de carregando
				
					
					$.post('do/crud/validaInscricao.php', {str_cpf: str_cpf, id_curso: id_curso, id_processo:id_processo, txt_ano: txt_ano }, function(resposta) {
					
						if(resposta != false){
							
							//atualizo a pagina
							location.reload();

						}
						
						else{
							// Exibo mensagem de erro
							$("#carregando_begin1").css("display", "none");
						}
					});
					
				}
			});
		
		}
		else{
			$("#carregando_begin1").css("display", "none");
		}
	
		event.preventDefault();
		
	});
