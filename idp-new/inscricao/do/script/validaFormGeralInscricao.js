$(document).ready(function() {
    Main.init();

    $('#formGrupos').validate({
        errorElement: "span", // contain the error msg in a span tag
        errorClass: 'help-block',
        errorPlacement: function (error, element) { // render error placement for each input type
            if (element.attr("type") == "radio" || element.attr("type") == "checkbox") { // for chosen elements, need to insert the error after the chosen container
                error.insertAfter($(element).closest('.form-group').children('div').children().last());
            } else {
                error.insertAfter(element);
                // for other inputs, just perform default behavior
            }
        },
        ignore: ':hidden',
        highlight: function (element) {
            $(element).closest('.help-block').removeClass('valid');
            // display OK icon
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
            // add the Bootstrap error class to the control group
            $(element).closest('form').find('.help-block').addClass('no-margin-bottom');
        },
        unhighlight: function (element) { // revert the change done by hightlight
            $(element).closest('.form-group').removeClass('has-error');
            // set error class to the control group
        },
        success: function (label, element) {
            label.addClass('help-block valid');
            label.remove();
            // mark the current input as valid and display OK icon
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
        },
        submitHandler: function(form) {

            var id_inscricao 	= $("#id_inscricao").val();
            var id_grupo 		= $("input[name='id_grupo']:checked").val();

            $("#carregando_grupos").css("display", "block");

            $.post('do/crud/updateGrupos.php', {id_inscricao: id_inscricao, id_grupo: id_grupo }, function(resposta) {

                if(resposta != false){
                    //atualizo a pagina
                    location.reload();
                }

            });

            return false;
        }
    });

    $('#formInscrever').validate({
        errorElement: "span", // contain the error msg in a span tag
        errorClass: 'help-block',
        errorPlacement: function (error, element) { // render error placement for each input type
            if (element.attr("type") == "radio" || element.attr("type") == "checkbox") { // for chosen elements, need to insert the error after the chosen container
                error.insertAfter($(element).closest('.form-group').children('div').children().last());
            } else {
                error.insertAfter(element);
                // for other inputs, just perform default behavior
            }
        },
        ignore: ':hidden',
        rules: {
            id_curso: {
                required: true
            },
            str_cpf: {
                cpf: true,
                required: true
            }
        },
        messages: {
            str_cpf: {
                cpf: 'CPF Inválido'
            }
        },
        highlight: function (element) {
            $(element).closest('.help-block').removeClass('valid');
            // display OK icon
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
            // add the Bootstrap error class to the control group
            $(element).closest('form').find('.help-block').addClass('no-margin-bottom');
        },
        unhighlight: function (element) { // revert the change done by hightlight
            $(element).closest('.form-group').removeClass('has-error');
            // set error class to the control group
        },
        success: function (label, element) {
            label.addClass('help-block valid');
            label.remove();
            // mark the current input as valid and display OK icon
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
        },
        submitHandler: function(form) {

            // Recupero so valores
            var id_curso = $("#id_curso").val();
            var str_cpf  = $("#str_cpf").val();
            var id_processo  = $("#id_processo").val();
            var txt_ano  = $("#txt_ano").val();

            $.post('do/crud/selectCPF.php', {str_cpf: str_cpf }, function(resposta) {

                if(resposta != false){
                    // removo o formulario atual
                    $("#divformInscrever").remove();

                    // mostro o novo formulario
                    $("#divformInscreverSenha").show();

                    // Atualizo o valor dos campos
                    $("#id_curso").val(id_curso);
                    $("#str_cpf").val(str_cpf);

                    // Dou foco no campo senha
                    $("#str_senha").focus();

                    //Recupero a mascara
                    $('#str_cpf').mask('999.999.999-99');

                    // Imprimo mensagem de já cadastrado
                    $("#bemvindo").css("display", "inline");
                    $("#bemvindo").html("<center><p>Seja Bem-vindo!<br/>"+resposta+"</p></center>");

                    $("#carregando_begin1").css("display", "none");

                }

                else{
                    // tenho que redirecionar para  apgina pois,
                    // não tem cadastro e vai iniciar o processo

                    // Mostro barra de carregando


                    $.post('do/crud/validaInscricao.php', {str_cpf: str_cpf, id_curso: id_curso, id_processo:id_processo, txt_ano: txt_ano }, function(resposta) {

                        if(resposta != false){
                            //atualizo a pagina
                            location.reload();
                        }
                        else{
                            // Exibo mensagem de erro
                            $("#carregando_begin1").css("display", "none");
                        }
                    });

                }
            });

            return false;
        }
    });

    $('#formInscreverSenha').validate({
        errorElement: "span", // contain the error msg in a span tag
        errorClass: 'help-block',
        errorPlacement: function (error, element) { // render error placement for each input type
            if (element.attr("type") == "radio" || element.attr("type") == "checkbox") { // for chosen elements, need to insert the error after the chosen container
                error.insertAfter($(element).closest('.form-group').children('div').children().last());
            } else {
                error.insertAfter(element);
                // for other inputs, just perform default behavior
            }
        },
        ignore: ':hidden',
        rules: {
            id_curso: {
                required: true
            },
            str_cpf: {
                cpf: true,
                required: true
            },
            str_senha: {
                required: true
            }
        },
        messages: {
            str_cpf: {
                cpf: 'CPF Inválido'
            }
        },
        highlight: function (element) {
            $(element).closest('.help-block').removeClass('valid');
            // display OK icon
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
            // add the Bootstrap error class to the control group
            $(element).closest('form').find('.help-block').addClass('no-margin-bottom');
        },
        unhighlight: function (element) { // revert the change done by hightlight
            $(element).closest('.form-group').removeClass('has-error');
            // set error class to the control group
        },
        success: function (label, element) {
            label.addClass('help-block valid');
            label.remove();
            // mark the current input as valid and display OK icon
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
        },
        submitHandler: function(form) {

            // Recupero so valores
            var id_curso     = $("#id_curso").val();
            var str_cpf      = $("#str_cpf").val();
            var str_senha    = $("#str_senha").val();
            var id_processo  = $("#id_processo").val();
            var txt_ano  = $("#txt_ano").val();

            $("#carregando_begin").css("display", "inline");

            $.post('do/crud/validaLoginInscricao.php', {str_cpf: str_cpf, str_senha: str_senha, id_curso: id_curso, id_processo: id_processo, txt_ano: txt_ano }, function(resposta) {

                if(resposta != false){
                    window.location.href = resposta;
                }

                // erro ao efetuar login
                else{

                    $("#carregando_begin").css("display", "none");

                    // limpo o valor do campo senha
                    $('#str_senha').val("");

                    //dou foco no campo senha
                    $("#str_senha").focus();

                    // Imprimo mensagem
                    $('#msg-inscrever').removeClass('hidden').html("CPF ou Senha inv&aacute;lidos");

                }
            });

            return false;
        }
    });

    $('#formLogin').validate({
        errorElement: "span", // contain the error msg in a span tag
        errorClass: 'help-block',
        errorPlacement: function (error, element) { // render error placement for each input type
            if (element.attr("type") == "radio" || element.attr("type") == "checkbox") { // for chosen elements, need to insert the error after the chosen container
                error.insertAfter($(element).closest('.form-group').children('div').children().last());
            } else {
                error.insertAfter(element);
                // for other inputs, just perform default behavior
            }
        },
        ignore: ':hidden',
        rules: {
            str_cpfLogin: {
                cpf: true,
                required: true
            },
            str_senhaLogin: {
                required: true
            }
        },
        messages: {
            str_cpfLogin: {
                cpf: 'CPF Inválido'
            }
        },
        highlight: function (element) {
            $(element).closest('.help-block').removeClass('valid');
            // display OK icon
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
            // add the Bootstrap error class to the control group
            $(element).closest('form').find('.help-block').addClass('no-margin-bottom');
        },
        unhighlight: function (element) { // revert the change done by hightlight
            $(element).closest('.form-group').removeClass('has-error');
            // set error class to the control group
        },
        success: function (label, element) {
            label.addClass('help-block valid');
            label.remove();
            // mark the current input as valid and display OK icon
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
        },
        submitHandler: function(form) {

            // Recupero so valores
            var str_cpf      = $("#str_cpfLogin").val();
            var str_senha    = $("#str_senhaLogin").val();

            $("#carregando_login").css("display", "inline");

            $.post('do/crud/validaLogin.php', {str_cpf: str_cpf, str_senha: str_senha }, function(resposta) {

                if(resposta != false){

                    //atualizo a pagina
                    location.reload();

                }

                // erro ao efetuar login
                else{

                    $("#carregando_login").css("display", "none");

                    // Destaco os campos em questão
                    $("#str_cpfLogin").css("border", "1px solid red");
                    $("#str_senhaLogin").css("border", "1px solid red");

                    // limpo o valor do campo senha
                    $('#str_senhaLogin').val("");

                    //dou foco no campo senha
                    $("#str_senhaLogin").focus();

                    // Imprimo mensagem
                    $('#msg-inscrito').removeClass('hidden').html("CPF ou Senha inv&aacute;lidos");

                }
            });

            return false;
        }
    });

    $('#frmEtapaCadastro').validate({
        errorElement: "span", // contain the error msg in a span tag
        errorClass: 'help-block',
        errorPlacement: function (error, element) { // render error placement for each input type
            if (element.attr("type") == "radio" || element.attr("type") == "checkbox") { // for chosen elements, need to insert the error after the chosen container
                error.insertAfter($(element).closest('.form-group').children('div').children().last());
            } else {
                error.insertAfter(element);
                // for other inputs, just perform default behavior
            }
        },
        ignore: ':hidden',
        rules: {
            str_sistema_senha: {
                minlength: 6,
                required: true
            },
            confir_str_sistema_senha: {
                required: true,
                minlength: 6,
                equalTo: "#str_sistema_senha"
            },
            dt_pessoa_nascimento : {
                required: true,
                dateBR: true
            }
        },
        highlight: function (element, label) {
            $(element).closest('.help-block').removeClass('valid');
            // display OK icon
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
            // add the Bootstrap error class to the control group
            $(element).closest('form').find('.help-block').addClass('no-margin-bottom');
        },
        unhighlight: function (element) { // revert the change done by hightlight
            $(element).closest('.form-group').removeClass('has-error');
            // set error class to the control group
        },
        success: function (label, element) {
            label.addClass('help-block valid');
            label.remove();
            // mark the current input as valid and display OK icon
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
        }
    });

    $('#frmEtapaQuestionario').validate({
        errorElement: "span", // contain the error msg in a span tag
        errorClass: 'help-block',
        errorPlacement: function (error, element) { // render error placement for each input type
            if (element.attr("type") == "radio" || element.attr("type") == "checkbox") { // for chosen elements, need to insert the error after the chosen container
                $(element).closest('.form-group').children('.aux-erro-questionario').remove();
                error.insertAfter($(element).closest('.form-group').children('.page-header'));
            } else {
                error.insertAfter(element);
                // for other inputs, just perform default behavior
            }
        },
        ignore: ':hidden',
        rules: {
            str_sistema_senha: {
                minlength: 6,
                required: true
            },
            confir_str_sistema_senha: {
                required: true,
                minlength: 6,
                equalTo: "#str_sistema_senha"
            }
        },
        highlight: function (element, label) {
            $(element).closest('.help-block').removeClass('valid');
            // display OK icon
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
            // add the Bootstrap error class to the control group
            $(element).closest('form').find('.help-block').addClass('no-margin-bottom');
        },
        unhighlight: function (element) { // revert the change done by hightlight
            $(element).closest('.form-group').removeClass('has-error');
            // set error class to the control group
        },
        success: function (label, element) {
            label.addClass('help-block valid');
            label.remove();
            // mark the current input as valid and display OK icon
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
        }
    });

    $('#formRecuperar').validate({
        errorElement: "span", // contain the error msg in a span tag
        errorClass: 'help-block',
        errorPlacement: function (error, element) { // render error placement for each input type
            if (element.attr("type") == "radio" || element.attr("type") == "checkbox") { // for chosen elements, need to insert the error after the chosen container
                error.insertAfter($(element).closest('.form-group').children('div').children().last());
            } else {
                error.insertAfter(element);
                // for other inputs, just perform default behavior
            }
        },
        ignore: ':hidden',
        rules: {
            str_senha: {
                minlength: 6,
                required: true
            },
            str_senha_c: {
                required: true,
                minlength: 6,
                equalTo: "#str_senha"
            }
        },
        highlight: function (element) {
            $(element).closest('.help-block').removeClass('valid');
            // display OK icon
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
            // add the Bootstrap error class to the control group
            $(element).closest('form').find('.help-block').addClass('no-margin-bottom');
        },
        unhighlight: function (element) { // revert the change done by hightlight
            $(element).closest('.form-group').removeClass('has-error');
            // set error class to the control group
        },
        success: function (label, element) {
            label.addClass('help-block valid');
            label.remove();
            // mark the current input as valid and display OK icon
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
        },
        submitHandler: function(form) {

            // Recupero so valores
            var str_senha       = $("#str_senha").val();
            var str_senha_c     = $("#str_senha_c").val();
            var id_pessoafisica = $("#id_pessoafisica").val();

            $("#carregando_login").css("display", "inline");

            $.post('do/crud/validaRecuperarHash.php', {str_senha: str_senha, id_pessoafisica:id_pessoafisica}, function(resposta) {

                if(resposta == true){

                    console.log('oi');

                    $('#msg_cadastroSUCESSO').removeClass("hidden");
                    $('#msg_cadastroERRO').addClass("slow");
                    $('.inscritos').hide();
                    $('#msg-inscrito').addClass('hidden').html("");
                    $("#carregando_login").css("display", "none");

                }

                // erro ao efetuar login
                else{
                    $('#msg_cadastroERRO').removeClass("slow");
                    $('#msg-inscrito').addClass('hidden').html("");
                    $("#carregando_login").css("display", "none");

                }
            });

            return false;
        }
    });

    $('#formRecuperarSenha').validate({
        errorElement: "span", // contain the error msg in a span tag
        errorClass: 'help-block',
        errorPlacement: function (error, element) { // render error placement for each input type
            if (element.attr("type") == "radio" || element.attr("type") == "checkbox") { // for chosen elements, need to insert the error after the chosen container
                error.insertAfter($(element).closest('.form-group').children('div').children().last());
            } else {
                error.insertAfter(element);
                // for other inputs, just perform default behavior
            }
        },
        ignore: ':hidden',
        rules: {
            str_cpf: {
                cpf: true,
                required: true
            }
        },
        messages: {
            str_cpf: {
                cpf: 'CPF Inválido'
            }
        },
        highlight: function (element) {
            $(element).closest('.help-block').removeClass('valid');
            // display OK icon
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
            // add the Bootstrap error class to the control group
            $(element).closest('form').find('.help-block').addClass('no-margin-bottom');
        },
        unhighlight: function (element) { // revert the change done by hightlight
            $(element).closest('.form-group').removeClass('has-error');
            // set error class to the control group
        },
        success: function (label, element) {
            label.addClass('help-block valid');
            label.remove();
            // mark the current input as valid and display OK icon
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
        },
        submitHandler: function(form) {

            // Recupero so valores
            var str_cpf = $("#str_cpf").val();

            $("#carregando_login").css("display", "inline");

            $.post('do/crud/validaRecuperar.php', {str_cpf: str_cpf}, function(resposta) {

                if(resposta == true){

                    $('#msg_recuperarSUCESSO').removeClass("hidden");
                    $('.inscritos').hide();
                    $('#msg-inscrito').addClass('hidden').html("");
                    $("#carregando_login").css("display", "none");

                }

                // erro ao efetuar login
                else{
                    $('#msg_recuperarSUCESSO').addClass("hidden");
                    $('#msg-inscrito').removeClass('hidden').html(resposta);
                    $("#carregando_login").css("display", "none");

                }
            });

            return false;
        }
    });
});

$('input[name="id_grupo"]').on('ifChecked', function() {
    $('#box-options-grupo').removeClass('has-error')
    $('#box-options-grupo').find('.help-block').remove();
});

$('#ch_enemS').on('ifChecked', function(){
    $(".enem-details").removeClass('hidden');
});
$('#ch_enemS').on('ifUnchecked', function(){
    $(".enem-details").addClass('hidden');
});

$('#ch_neS').on('ifChecked', function(){
    $(".necessidade_especial").removeClass('hidden');
});
$('#ch_neS').on('ifUnchecked', function(){
    $(".necessidade_especial").addClass('hidden');
});

$('#ch_ofS').on('ifChecked', function(){
    $(".instituicao-details").removeClass('hidden');
});
$('#ch_ofS').on('ifUnchecked', function(){
    $(".instituicao-details").addClass('hidden');
});