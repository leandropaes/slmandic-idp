<?php require_once('__lib__.php');?>


<?php

	// Recuperamos os valores dos campos através do método POST

	$str_cpf      = $_POST['str_cpf'];	
	$id_curso     = $_POST['id_curso'];	
	$id_processo  = $_POST['id_processo'];	
	$txt_ano  	  = $_POST['txt_ano'];	
	$str_senha    = sha1($_POST['str_senha']);	
		
	// Verifico se o CPF já existe
	$query ="SELECT * FROM ".DB_PREFIXO."PessoasFisicas WHERE str_pessoa_cpf = '".$str_cpf."' AND str_sistema_senha = '".$str_senha."' LIMIT 1";	
	$resultado = mysql_query($query, $conectar); 
		
	// Se o login estiver válido
	if(mysql_num_rows($resultado)){
		
		// Recupero as informações do usuário
			$linha = mysql_fetch_object($resultado);
			
			$id_pessoafisica = $linha->id_pessoafisica;
			$str_pessoa_nome = $linha->str_pessoa_nome;
		
		// Recupero (id_inscricao / int_etapa)
		// Se o usuario já tiver inscrito, pego o ID
		// Senão gero a inscricao e pego o ID
		
			require_once('../forms/gerarInscricao.php');
			
		// Crio os arrays de controle

			// Autenticação
			$r_autenticacao = array(
				'painel' => 'inscricao'
			);
			
			// Formulario
			$r_form = array(
				'id_curso'		   => $id_curso,
				
				'id_pessoafisica'  => $id_pessoafisica,
				'str_pessoa_nome'  => $str_pessoa_nome,
				'str_pessoa_cpf'   => $str_cpf,
				'num_inscricao'    => $num_inscricao,
				
				'id_inscricao'     => $id_inscricao,
				'int_etapa'        => $int_etapa
			
			);
			
			// Etapas
			if ($r_form['int_etapa'] >= 3){
				
				($r_form['int_etapa'] > 3)? $permissao = 1:$permissao = 0;
				
				$r_etapa = array(
					'atual'	=> 4,
									
					1 => array ('nome' => 'Cadastro'	, 'status' => 0, 'permissao' => 0),
					2 => array ('nome' => 'Questionário Sócio-Econômico', 'status' => 0, 'permissao' => 0),
					3 => array ('nome' => 'Conferência'	, 'status' => 0, 'permissao' => 0),
					4 => array ('nome' => 'Gerar Boleto', 'status' => 1, 'permissao' => $permissao)
				);
			
			}
			else{
				require_once('etapas.php');
		
				// De acordo com a etapa do r_form atualizo a r_etapa
				if ($r_form['int_etapa']){
							
					for ($i = $r_form['int_etapa']; $i > 0; $i--){
					
						$r_etapa[$i]['status']    = 2;
						$r_etapa[$i]['permissao'] = 1;
					
					}
				
				}
				
				// Atualizo o usuario para a etapa que ele parou
				$r_etapa[$r_form['int_etapa']+1]['status'] = 1;
				$r_etapa['atual'] = $r_form['int_etapa']+1;
		
				
				$r_etapa[1]['permissao'] = 1;
			}
		
		// Se a pessoa ja concluiu a etapa 4,
		// direciono paea area do inscrito
		if($r_etapa[4]['permissao']){
		
			$link = 'index.php?inscricao='.base64_encode($r_form['id_inscricao']);
		
			// Autenticação
			$r_autenticacao = array(
				'painel' => 'inscrito'
			);
			
			// Formulario
			$r_form = array(
				
				'id_pessoafisica'  => $id_pessoafisica,
				'str_pessoa_nome'  => $str_pessoa_nome,
				'str_pessoa_cpf'   => $str_cpf
			
			);

		
			// CRIO AS SESSIONS COM OS REGISTROS
			$_SESSION['autenticacao'] = $r_autenticacao;
			$_SESSION['form'] 		  = $r_form;
		
		
		}	
		else{	

			$link = 'index.php';
		
			// CRIO AS SESSIONS COM OS REGISTROS
			$_SESSION['autenticacao'] = $r_autenticacao;
			$_SESSION['form'] 		  = $r_form;
			$_SESSION['etapa'] 		  = $r_etapa;
		}
		

		echo $link;
	}
	else
		
		echo false;
	
	
?>