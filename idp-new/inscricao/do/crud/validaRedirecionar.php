<?php require_once('__lib__.php');?>


<?php

	// Recuperamos os valores dos campos através do método POST
	$id_inscricao = $_POST['id_inscricao'];	

	$query = "	SELECT 
					  inscr.id_curso
					, pf.id_pessoafisica
					, pf.str_pessoa_nome
					, pf.str_pessoa_cpf
					, inscr.num_inscricao
					, inscr.id_inscricao
					, inscr.int_etapa
				FROM 
					".DB_PREFIXO."Inscricoes AS inscr
					
					INNER JOIN ".DB_PREFIXO."PessoasFisicas pf ON 
					inscr.id_pessoafisica = pf.id_pessoafisica
				
				WHERE 
					inscr.id_inscricao = '".$id_inscricao."'
			;";
	
	$resultado = mysql_query($query, $conectar); 
		
	// Se o login estiver válido
	if(mysql_num_rows($resultado)){
		
		$linha = mysql_fetch_object($resultado);

		// Crio os arrays de controle

			// Autenticação
			$r_autenticacao = array(
				'painel' => 'inscricao'
			);
			
			// Formulario
			$r_form = array(
				'id_curso'		   => $linha->id_curso,
				
				'id_pessoafisica'  => $linha->id_pessoafisica,
				'str_pessoa_nome'  => $linha->str_pessoa_nome,
				'str_pessoa_cpf'   => $linha->str_pessoa_cpf,
				'num_inscricao'    => $linha->num_inscricao,
				
				'id_inscricao'     => $linha->id_inscricao,
				'int_etapa'        => $linha->int_etapa
			
			);
			
			// Etapas
			if ($r_form['int_etapa'] >= 3){
				
				($r_form['int_etapa'] > 3)? $permissao = 1:$permissao = 0;
				
				$r_etapa = array(
					'atual'	=> 4,
									
					1 => array ('nome' => 'Cadastro'	, 'status' => 0, 'permissao' => 0),
					2 => array ('nome' => 'Questionário Sócio-Econômico', 'status' => 0, 'permissao' => 0),
					3 => array ('nome' => 'Conferência'	, 'status' => 0, 'permissao' => 0),
					4 => array ('nome' => 'Gerar Boleto', 'status' => 1, 'permissao' => $permissao)
				);
			
			}
			else{
				require_once('etapas.php');
		
				// De acordo com a etapa do r_form atualizo a r_etapa
				if ($r_form['int_etapa']){
							
					for ($i = $r_form['int_etapa']; $i > 0; $i--){
					
						$r_etapa[$i]['status']    = 2;
						$r_etapa[$i]['permissao'] = 1;
					
					}
				
				}
				
				// Atualizo o usuario para a etapa que ele parou
				$r_etapa[$r_form['int_etapa']+1]['status'] = 1;
				$r_etapa['atual'] = $r_form['int_etapa']+1;
		
				
				$r_etapa[1]['permissao'] = 1;
			}
		
		

			$link = 'index.php';
		
			// CRIO AS SESSIONS COM OS REGISTROS
			$_SESSION['autenticacao'] = $r_autenticacao;
			$_SESSION['form'] 		  = $r_form;
			$_SESSION['etapa'] 		  = $r_etapa;
		

			echo 'index.php';
	}
	else
		
		echo false;
	
	
?>