<form id="frmEtapaCadastro" action="index.php" method="POST" class="form-horizontal">

	<?php if (isset($goto)){echo "<input type='hidden' name='goto' value='{$goto}' />";} ?>
	<input type="hidden" name="salvarPessoaFisica" value="salvarPessoaFisica" />

	<div class="row">
		<div class="col-sm-12">

			<div id="msg_cadastroPessoaFisica" class="alert alert-danger <?php echo (($validacao) ? 'hidden' : '') ?>"><b>Alerta:</b> Os Campos em Vermelho são de preenchimento obrigatório.</div>

			<div class="panel-heading text-center" style="padding-left: 0 !important;">
				Dados Pessoais
				<a name="tb-dados-pessoais"> </a>
			</div>
			<br>

			<div class="form-group">
				<label for="str_pessoa_cpf" class="col-md-4 control-label">
					CPF
				</label>
				<div class="col-md-4">
					<input type="text" name="str_pessoa_cpf" id="str_pessoa_cpf" class="form-control" value="<?=$r_form['str_pessoa_cpf']?>"  <?=$str_pessoa_cpf?> disabled />
				</div>
			</div>
			<div class="form-group">
				<label for="str_pessoa_nome" class="col-md-4 control-label">
					Nome
				</label>
				<div class="col-md-4">
					<input placeholder="Nome Completo" type="text" class="form-control" name="str_pessoa_nome" id="str_pessoa_nome" value="<?=$r_cadastro['str_pessoa_nome']?>" <?=$str_pessoa_nome?> required/>
				</div>
			</div>
			<div class="form-group">
				<label for="str_pessoa_rgrne" class="col-md-4 control-label">
					RG
				</label>
				<div class="col-md-4">
					<input placeholder="RG" type="text" class="form-control" name="str_pessoa_rgrne" id="str_pessoa_rgrne" value="<?=$r_cadastro['str_pessoa_rgrne']?>" <?=$str_pessoa_rgrne?> required/>
				</div>
			</div>
			<div class="form-group">
				<label for="str_pessoa_ssp" class="col-md-4 control-label">
					Orgão Expedidor
				</label>
				<div class="col-md-4">
					<input placeholder="Orgão Expedidor" type="text" class="form-control" name="str_pessoa_ssp" id="str_pessoa_ssp" value="<?=$r_cadastro['str_pessoa_ssp']?>" <?=$str_pessoa_ssp?> required/>
				</div>
			</div>
			<div class="form-group">
				<label for="dt_pessoa_nascimento" class="col-md-4 control-label">
					Data de Nascimento
				</label>
				<div class="col-md-4">
					<input placeholder="dd/mm/aaaa" data-mask="00/00/0000" class="form-control" type="text" name="dt_pessoa_nascimento" id="dt_pessoa_nascimento" value="<?=$r_cadastro['dt_pessoa_nascimento']?>" <?=$dt_pessoa_nascimento?>  required/><?php if($msg_data) echo ' <span class="campo_erro">',$msg_data,'</span>';?>
				</div>
			</div>
			<div class="form-group">
				<label for="ch_pessoa_sexo" class="col-md-4 control-label">
					Sexo
				</label>
				<div class="col-md-4">
					<select class="form-control" name="ch_pessoa_sexo" id="ch_pessoa_sexo" <?=$ch_pessoa_sexo?> required>
						<option value="">(Selecione)</option>
						<?php
						foreach($r_sexo as $sigla => $sexo){
							($r_cadastro['ch_pessoa_sexo'] == $sigla)? $selected = 'selected' : $selected = NULL;
							echo "<option value='{$sigla}' {$selected}>{$sexo}</option>";
						}
						?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label for="str_pessoa_naturalde" class="col-md-4 control-label">
					Naturalidade
				</label>
				<div class="col-md-4">
					<input placeholder="Naturalidade" type="text" class="form-control" name="str_pessoa_naturalde" id="str_pessoa_naturalde" value="<?=$r_cadastro['str_pessoa_naturalde']?>" <?=$str_pessoa_naturalde?> required/>
				</div>
			</div>

			<div class="panel-heading text-center" style="padding-left: 0 !important;">
				Endereço Residencial
				<a name="tb-endereco-residencial"> </a>
			</div>
			<br>

			<div class="form-group">
				<label for="str_endereco_cep" class="col-md-4 control-label">
					CEP
				</label>
				<div class="col-md-4">
					<input placeholder="CEP" type="text" class="form-control" data-mask="00.000-000" name="str_endereco_cep" id="str_endereco_cep" value="<?=$r_cadastro['str_endereco_cep']?>" <?=$str_endereco_cep?> required/>
				</div>
			</div>
			<div class="form-group">
				<label for="str_endereco_logradouro" class="col-md-4 control-label">
					Logradouro
				</label>
				<div class="col-md-4">
					<input placeholder="Logradouro" type="text" class="form-control" name="str_endereco_logradouro" id="str_endereco_logradouro" value="<?=$r_cadastro['str_endereco_logradouro']?>" <?=$str_endereco_logradouro?> required/>
				</div>
			</div>
			<div class="form-group">
				<label for="str_endereco_numero" class="col-md-4 control-label">
					Número
				</label>
				<div class="col-md-4">
					<input placeholder="Número" type="text" class="form-control" name="str_endereco_numero" id="str_endereco_numero" value="<?=$r_cadastro['str_endereco_numero']?>" <?=$str_endereco_numero?> required/>
				</div>
			</div>
			<div class="form-group">
				<label for="str_endereco_bairro" class="col-md-4 control-label">
					Bairro
				</label>
				<div class="col-md-4">
					<input placeholder="Bairro" type="text" class="form-control" name="str_endereco_bairro" id="str_endereco_bairro" value="<?=$r_cadastro['str_endereco_bairro']?>" <?=$str_endereco_bairro?> required/>
				</div>
			</div>
			<div class="form-group">
				<label for="str_endereco_cidade" class="col-md-4 control-label">
					Cidade
				</label>
				<div class="col-md-4">
					<input placeholder="Cidade" type="text" class="form-control" name="str_endereco_cidade" id="str_endereco_cidade" value="<?=$r_cadastro['str_endereco_cidade']?>" <?=$str_endereco_cidade?> required/>
				</div>
			</div>
			<div class="form-group">
				<label for="str_endereco_estado" class="col-md-4 control-label">
					Estado
				</label>
				<div class="col-md-4">
					<select class="form-control" name="str_endereco_estado" id="str_endereco_estado" <?=$str_endereco_estado?> required>
						<option value="">(Selecione)</option>
						<?php
						foreach($r_estado as $sigla => $estado){
							($r_cadastro['str_endereco_estado'] == $sigla)? $selected = 'selected' : $selected = NULL;
							echo "<option value='{$sigla}' {$selected}>{$sigla}</option>";
						}
						?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label for="str_endereco_complemento" class="col-md-4 control-label">
					Complemento
				</label>
				<div class="col-md-4">
					<input placeholder="Complemento" type="text" class="form-control" name="str_endereco_complemento" id="str_endereco_complemento" value="<?=$r_cadastro['str_endereco_complemento']?>" <?=$str_endereco_complemento?> />
				</div>
			</div>
			<div class="form-group">
				<label for="str_endereco_pais" class="col-md-4 control-label">
					País
				</label>
				<div class="col-md-4">
					<input placeholder="País" type="text" class="form-control" name="str_endereco_pais" id="str_endereco_pais" value="<?=$r_cadastro['str_endereco_pais']?>" <?=$str_endereco_pais?> required/>
				</div>
			</div>

			<div class="panel-heading text-center" style="padding-left: 0 !important;">
				Dados para Contato
				<a name="tb-dados-contato"> </a>
			</div>
			<br>

			<div class="form-group">
				<label for="str_telefone_residencial" class="col-md-4 control-label">
					Telefone Residencial
				</label>
				<div class="col-md-4">
					<input placeholder="(99) 9999-9999" data-mask="(00) 0000-0000" class="form-control" type="text" name="str_telefone_residencial" id="str_telefone_residencial" value="<?=$r_cadastro['str_telefone_residencial']?>" <?=$str_telefone_residencial?> required/>
				</div>
			</div>
			<div class="form-group">
				<label for="str_telefone_comercial" class="col-md-4 control-label">
					Telefone Comercial
				</label>
				<div class="col-md-4">
					<input placeholder="(99) 9999-9999" data-mask="(00) 0000-0000" class="form-control" type="text" name="str_telefone_comercial" id="str_telefone_comercial" value="<?=$r_cadastro['str_telefone_comercial']?>" <?=$str_telefone_comercial?>/>
				</div>
			</div>
			<div class="form-group">
				<label for="str_telefone_celular" class="col-md-4 control-label">
					Celular
				</label>
				<div class="col-md-4">
					<input placeholder="(99) 99999-9999" maxlength="15" class="form-control" onkeyup="mascara( this, mtel );" type="text" name="str_telefone_celular" id="str_telefone_celular" value="<?=$r_cadastro['str_telefone_celular']?>" <?=$str_telefone_celular?> required/>
				</div>
			</div>
			<div class="form-group">
				<label for="str_pessoa_email" class="col-md-4 control-label">
					E-mail
				</label>
				<div class="col-md-4">
					<input placeholder="exemplo@dominio.com.br" type="email" class="form-control" name="str_pessoa_email" id="str_pessoa_email" value="<?=$r_cadastro['str_pessoa_email']?>" <?=$str_pessoa_email?> required/><?php if($msg_email) echo '  <span class="campo_erro">',$msg_email,'</span>';?>
				</div>
			</div>
			<div class="form-group">
				<label for="ch_comunicacao_sms" class="col-md-4 control-label">
					Aceita envio de informações do vestibular por SMS?
				</label>
				<div class="col-md-4">
					<select class="form-control" name="ch_comunicacao_sms" id="ch_comunicacao_sms" <?=$ch_comunicacao_sms?> >
						<?php
						foreach($r_sms as $silga => $sms){
							($r_cadastro['ch_comunicacao_sms'] == $silga)? $selected = 'selected' : $selected = NULL;
							echo "<option value='{$silga}' {$selected}>{$sms}</option>";
						}
						?>
					</select>
				</div>
			</div>

			<?php
			// Se existe opções do enem
			if($r_enem) {
			?>
			<div class="panel-heading text-center" style="padding-left: 0 !important;">
				Enem
				<a name="tb-enem"> </a>
			</div>
			<br>

			<div class="form-group">
				<label class="col-md-4 control-label">
					Deseja utilizar sua nota do ENEM?
				</label>
				<div class="col-md-4">
					<label class="radio-inline">
						<input class="square-green" type="radio" name="ch_enem" id="ch_enemS" value="S" <?=($r_cadastro['ch_enem']=='S')?'checked':NULL?>  >
						Sim
					</label>
					<label class="radio-inline">
						<input class="square-green" type="radio" name="ch_enem" id="ch_enemN" value="N" <?=($r_cadastro['ch_enem']=='N')?'checked':NULL?>  >
						Não
					</label>
				</div>
			</div>

			<div class="form-group enem-details <?=($r_cadastro['ch_enem']=='N')?'hidden': '' ?>">
				<label class="col-md-4 control-label">
					Ano de Exame <?=$r_cadastro['ch_enem_ano']?>
				</label>
				<div class="col-md-4">
					<?php foreach($r_enem as $key => $value){ ?>
					<label for="str_enem_ano<?=$key?>" class="radio-inline">
						<input class="square-green" type="radio" name="str_enem_ano" id="str_enem_ano<?=$key?>" value="<?=$value?>" <?=($r_cadastro['str_enem_ano']==$value)?'checked':NULL?> <?=$checked?>  required/>
						<?=$value?>
					</label>
					<?php } ?>
				</div>
			</div>

			<div class="form-group enem-details <?=($r_cadastro['ch_enem']=='N')?'hidden': '' ?>">
				<label for="str_enem_inscricao" class="col-md-4 control-label">
					Nº de Inscrição
				</label>
				<div class="col-md-4">
					<input placeholder="Nº de Inscrição" class="form-control" type="text" name="str_enem_inscricao" value="<?=$r_cadastro['str_enem_inscricao']?>" <?=$str_enem_inscricao?>  required>
				</div>
			</div>

			<?php } else { ?>
			<input type='hidden' name='ch_enem' value='N'>
			<?php } ?>

			<div class="panel-heading text-center" style="padding-left: 0 !important;">
				Informações
				<a name="tb-informacoes"> </a>
			</div>
			<br>

			<div class="form-group">
				<label for="str_ensinomedio_local" class="col-md-4 control-label">
					Colégio em que concluiu o Ensino Médio
				</label>
				<div class="col-md-4">
					<input placeholder="Nome do Colégio" type="text" class="form-control" name="str_ensinomedio_local" id="str_ensinomedio_local" value="<?=$r_cadastro['str_ensinomedio_local']?>" <?=$str_ensinomedio_local?>  required>
				</div>
			</div>
			<div class="form-group">
				<label for="str_ensinomedio_conclusao" class="col-md-4 control-label">
					Ano de Conclusão
				</label>
				<div class="col-md-4">
					<input placeholder="Ano de Conclusão" type="text" class="form-control" name="str_ensinomedio_conclusao" value="<?=$r_cadastro['str_ensinomedio_conclusao']?>" <?=$str_ensinomedio_conclusao?>  required>
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-4 control-label">
					Você é portador de alguma necessidade especial?
				</label>
				<div class="col-md-4">
					<label for="ch_neS" class="radio-inline">
						<input type="radio" class="square-green" name="ch_necessidade_especial" id="ch_neS" value="S" <?=($r_cadastro['ch_necessidade_especial']=='S')?'checked':NULL?> >
						Sim
					</label>
					<label for="ch_neN" class="radio-inline">
						<input type="radio" class="square-green" name="ch_necessidade_especial" id="ch_neN" value="N" <?=($r_cadastro['ch_necessidade_especial']=='N')?'checked':NULL?> >
						Não
					</label>
				</div>
			</div>
			<div class="form-group necessidade_especial <?=($r_cadastro['ch_necessidade_especial']=='N') ? 'hidden' : '' ?>">
				<label for="str_necessidade_especial" class="col-md-4 control-label">
					Necessidade
				</label>
				<div class="col-md-4">
					<input placeholder="Qual a sua necessidade" type="text" class="form-control" name="str_necessidade_especial" id="str_necessidade_especial" value="<?=$r_cadastro['str_necessidade_especial']?>" <?=$str_necessidade_especial?>  required />
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-4 control-label">
					Necessita de carteira para canhoto?
				</label>
				<div class="col-md-4">
					<label for="ch_cS" class="radio-inline">
						<input type="radio" class="square-green" name="ch_necessidade_canhoto" id="ch_cS" value="S" <?=($r_cadastro['ch_necessidade_canhoto']=='S')?'checked':NULL?> >
						Sim
					</label>
					<label for="ch_cN" class="radio-inline">
						<input type="radio" class="square-green" name="ch_necessidade_canhoto" id="ch_cN" value="N" <?=($r_cadastro['ch_necessidade_canhoto']=='N')?'checked':NULL?> >
						Não
					</label>
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-4 control-label">
					Está prestando vestibular em outra faculdade?
				</label>
				<div class="col-md-4">
					<label for="ch_ofS" class="radio-inline">
						<input type="radio" class="square-green" name="ch_comunicacao_outrafaculdade" id="ch_ofS" value="S" <?=($r_cadastro['ch_comunicacao_outrafaculdade']=='S')?'checked':NULL?> >
						Sim
					</label>
					<label for="ch_ofN" class="radio-inline">
						<input type="radio" class="square-green" name="ch_comunicacao_outrafaculdade" id="ch_ofN" value="N" <?=($r_cadastro['ch_comunicacao_outrafaculdade']=='N')?'checked':NULL?> >
						Não
					</label>
				</div>
			</div>
			<div class="form-group instituicao-details <?=($r_cadastro['ch_comunicacao_outrafaculdade']=='N') ? 'hidden' : '' ?>">
				<label for="str_comunicacao_qualcurso" class="col-md-4 control-label">
					Qual(is) Curso(s)?
				</label>
				<div class="col-md-4">
					<input placeholder="Curso" type="text" class="form-control" name="str_comunicacao_qualcurso" id="str_comunicacao_qualcurso" value="<?=$r_cadastro['str_comunicacao_quaislocais']?>" <?=$str_comunicacao_quaislocais?> required/>
				</div>
			</div>
			<div class="form-group instituicao-details <?=($r_cadastro['ch_comunicacao_outrafaculdade']=='N') ? 'hidden' : '' ?>">
				<label for="str_comunicacao_quaislocais" class="col-md-4 control-label">
					Qual(is) Instituição(ões)?
				</label>
				<div class="col-md-4">
					<input placeholder="Instituição" type="text" class="form-control" name="str_comunicacao_quaislocais" id="str_comunicacao_quaislocais" value="<?=$r_cadastro['str_comunicacao_quaislocais']?>" required <?=$str_comunicacao_quaislocais?> />
				</div>
			</div>

			<div class="form-group">
				<label for="str_comunicacao_ficousabendo" class="col-md-4 control-label">
					Como ficou sabendo do vestibular?
				</label>
				<div class="col-md-4">
					<select class="form-control" name="str_comunicacao_ficousabendo" <?=$str_comunicacao_ficousabendo?>  required>
						<option value="">(Selecione)</option>
						<?php
						foreach ($r_ficousabendo as $key => $value) {

							$selected = ($r_cadastro['str_comunicacao_ficousabendo'] == $value)?'selected':NULL;

							echo "<option value='{$key}' {$selected} >{$value}</option>";
						}
						?>
					</select>
				</div>
			</div>


			<?php if($r_etapa[1]['permissao']){ ?>
				<input type="hidden" name="str_sistema_senha" id="str_sistema_senha" value="<?=$r_cadastro['str_sistema_senha']?>" <?=$str_sistema_senha?> />
				<input type="hidden" name="confir_str_sistema_senha" id="confir_str_sistema_senha" value="<?=$r_cadastro['confir_str_sistema_senha']?>" <?=$confir_str_sistema_senha?> />
			<?php } else {?>

				<div class="panel-heading text-center" style="padding-left: 0 !important;">
					Digite uma senha para posterior acesso à área do inscrito
				</div>
				<br>

				<div class="form-group">
					<label for="str_sistema_senha" class="col-md-4 control-label">
						Senha de Acesso
					</label>
					<div class="col-md-4">
						<input placeholder="Insira sua Senha" type="password" class="form-control" name="str_sistema_senha" id="str_sistema_senha" value="<?=$r_cadastro['str_sistema_senha']?>" <?=$str_sistema_senha?> /> <?php if($msg_senha) echo ' <span class="campo_erro">',$msg_senha,'</span>';?>
					</div>
				</div>
				<div class="form-group">
					<label for="confir_str_sistema_senha" class="col-md-4 control-label">
						Confirmação da Senha
					</label>
					<div class="col-md-4">
						<input placeholder="Confirme sua Senha" type="password" class="form-control" name="confir_str_sistema_senha" id="confir_str_sistema_senha" value="<?=$r_cadastro['confir_str_sistema_senha']?>" <?=$confir_str_sistema_senha?> />
					</div>
				</div>
			<?php } ?>

			<br>

			<div class="form-group">
				<div class="col-md-4 col-md-offset-4">
					<button type="submit" class="btn btn-primary btn-block">Salvar Cadastro</button>
				</div>
			</div>

			<hr>

		</div>
	</div>

</form>