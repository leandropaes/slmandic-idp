<?php
// RECUPERO AS PERGUNTAS E ALTERNATIVAS
$query = "	SELECT 
				a.*, b.str_pergunta 
			
			FROM ".DB_PREFIXO."QuestAlternativas AS a
				
				INNER JOIN ".DB_PREFIXO."QuestPerguntas b ON
				a.id_qsepergunta = b.id_qsepergunta
			
			WHERE 
				b.id_processo = '".$id_processo."'
				and
				a.ch_situacao = 'A' ";
				
$resultado = mysql_query($query, $conectar); 

$vestibular = array();
$validacao = array();

while ($dados = mysql_fetch_object($resultado)) {

	$validacao[$dados->id_qsepergunta] = array ('msg' => NULL, 'msgExibida' => FALSE, 'valor' => NULL );
	
	$vestibular[utf8_encode($dados->str_pergunta)][utf8_encode($dados->str_alternativa)]['tipo']  = $dados->ch_descricao;
	$vestibular[utf8_encode($dados->str_pergunta)][utf8_encode($dados->str_alternativa)]['id']    = $dados->id_qsepergunta;
	$vestibular[utf8_encode($dados->str_pergunta)][utf8_encode($dados->str_alternativa)]['value'] = $dados->id_qsealternativa;

}



?>



<?php
// Considero de inicio que o formulário é válido
$eValido = true;

// SALVAR RESPOSTAS

if(isset($_POST['salvarQuestionario'])){

	// Pego o id da inscrição
	$id_inscricao =  $r_form['id_inscricao'];

	// Pego o arrau das perguntas
	$r_perguntas = $_POST['pergunta'];
	
	// Valido as respostas
	// Buscando os campos que não foram preenchidos
	
	foreach($vestibular as $key => $row){
	
		$temp = 0;
		foreach($row as $key_2 => $row_2){
			
			if ($row_2['id'] != $temp){
				
				if ((!isset($r_perguntas[$row_2['id']])) || ($r_perguntas[$row_2['id']] == NULL)){
								
					if (($row_2['tipo'] == 'R') || ($row_2['tipo'] == 'RS'))
						$txt = 'Selecionado';
					else
						$txt = 'Preenchido';
					
					
					$validacao[$row_2['id']]['msg'] = '<tr><td colspan="2" style="text-align:left; color: red; font-size: 10pt;" >* Campo não '.$txt.'.</td></tr>';
					
					$eValido = false;
				}
				elseif(($row_2['tipo'] == 'D') and (!validaData($r_perguntas[$row_2['id']])) ){
				
					$validacao[$row_2['id']]['msg'] = '<tr><td colspan="2" style="text-align:left; color: red; font-size: 10pt;" >* Formato de data inválida</td></tr>';
					
					
					$eValido = false;
				
				}
				
				else
					$validacao[$row_2['id']]['valor'] = $r_perguntas[$row_2['id']];
				
				$temp = $row_2['id'];
			}
			
		}
		
	}

	// Se o formulário passou na validação então salvo as respostas no banco
	if($eValido){

		// Insiro as informações no banco
		
			// verifico se vou atualizar ou cadastrar
			
			// UPDATE
			if($r_etapa[2]['permissao'] && isset($_SESSION['respostas'])){
			
				
				foreach($_POST['pergunta'] as $key => $row){

					
					if(is_numeric($row)){
					
						$query = "UPDATE ".DB_PREFIXO."QuestRespostas SET id_alternativa = '{$row}', str_resposta = NULL WHERE id_resposta = '".$_SESSION['respostas'][$key]['id_resposta']."';";

						
					}elseif($row){
					
						$query = "UPDATE ".DB_PREFIXO."QuestRespostas SET str_resposta = '".utf8_decode($row)."', id_alternativa = NULL WHERE id_resposta = '".$_SESSION['respostas'][$key]['id_resposta']."';";

					}
					
					mysql_query($query, $conectar);
					
				
				}
				
			
			}
			// CREATE
			else{
				foreach($_POST['pergunta'] as $key => $row){
				
					if(is_numeric($row)){
						$query = "INSERT INTO ".DB_PREFIXO."QuestRespostas
						(id_inscricao, dt_save, id_alternativa, id_pergunta)
						VALUES
						('{$id_inscricao}', now(), '{$row}', '{$key}')";
					}elseif($row){
						
						$query = "INSERT INTO ".DB_PREFIXO."QuestRespostas
						(id_inscricao, str_resposta, dt_save, id_pergunta)
						VALUES
						('{$id_inscricao}', '".utf8_decode($row)."', now(), '{$key}')";
					}
					
					mysql_query($query, $conectar);
				
				}
			}
		
		// Atualizo na inscrição do candidato que ele já preencheu o questionário
		if ($r_form['int_etapa'] < 2){
					
			$query = "UPDATE ".DB_PREFIXO."Inscricoes SET  int_etapa =  '2' WHERE  id_inscricao = '".$r_form["id_inscricao"]."' ;";
			mysql_query($query, $conectar);
					
			// atualizo int_etapa
			$r_form['int_etapa'] = 2;
		}
					
		// Atualizo o nome no formulario
		$r_form = array(		
			'id_curso' 			=> $r_form['id_curso'],
			'id_pessoafisica' 	=> $r_form['id_pessoafisica'],
			'str_pessoa_nome' 	=> $r_form['str_pessoa_nome'],
			'str_pessoa_cpf' 	=> $r_form['str_pessoa_cpf'],
			'num_inscricao' 	=> $r_form['num_inscricao'],
			'id_inscricao' 		=> $r_form['id_inscricao'],
			'int_etapa' 		=> $r_form['int_etapa']
		);
				
		// Atualizo a etapa
					
			// Proxima etapa
			$r_etapa['atual'] = 3;
					
			// Etapa atual fica LIBERADA com UPDATE
			$r_etapa[2]['status']    = 2;
			$r_etapa[2]['permissao'] = 1;
					
			$r_etapa[3]['status'] 	 = 1;

			// Atualizo as sessions
			$_SESSION['form']  = $r_form;
			$_SESSION['etapa'] = $r_etapa;

	}

}
// Verifico se nessa etapa esta com permissao de UPDATE
// Se tiver recupero as informacoes

elseif($r_etapa[2]['permissao']){


	$query = "SELECT * FROM ".DB_PREFIXO."QuestRespostas WHERE id_inscricao = '".$r_form['id_inscricao']."' ORDER BY id_pergunta ASC;";
	
	$resultado = mysql_query($query, $conectar);
	
	while($linha = mysql_fetch_object($resultado)){

		$id_pergunta = $linha->id_pergunta;
		
		if ($linha->id_alternativa){
			$valor = $linha->id_alternativa;
		}
		else{
			$valor = $linha->str_resposta;
		}
		
		$validacao[$id_pergunta]['valor'] = utf8_encode($valor);
		
		$_SESSION['respostas'][$id_pergunta]['id_resposta'] = $linha->id_resposta;
		
	}

}


?>
