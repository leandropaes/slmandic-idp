<?php
	// VERIFICO SE ESTA NA TELA DE (INSCRICAO) OU (AREA DO INSCRITO)
	$r_autenticacao = $_SESSION['autenticacao'];
	
	if ($r_autenticacao['painel'] == 'inscricao'){
	
		// ATUALIZO ONDE ESTOU DA ETAPA, CASO O MUDOU DE ETAPA PELO
		// CLIQUE NO MENU LIBERADO
		
		if(isset($_GET['do'])){
			
			$do = base64_decode($_GET['do']);
			
			// condições para trocar de pagina
			$len   = strlen($do);
			
			// Valido se a informação é igual a 3
			if ($len == 3){

				$line1 = substr($do, 0, 1);
				$line2 = substr($do, 1, 1);
				$line3 = substr($do, 2, 2);
				
				// Valido se a informação esta nos parametros corretos		
				if ((($line1 > 0) and ($line1 < 5)) and ($line2 == ',') and  (($line3 > 0) and ($line3 < 5))){
				
					$temp = explode(',', $do);
			
					$deOndeVim   = $temp[0];
					$paraOndeVou = $temp[1];
					
					// Recupero as etapas
					$r_etapa = $_SESSION['etapa'];
					
					// Valido se o de onde vim e para onde vou não são bloqueados
					if($r_etapa[$paraOndeVou]['status'] and $r_etapa[$deOndeVim]['status']){
					
						// configuro as novas etapas
						$r_etapa['atual'] = $paraOndeVou;
						
						// Status de onde vim fica liberado (2)
						$r_etapa[$deOndeVim]['status'] = 2;
						
						// Status para onde vou fica como atual (1)
						$r_etapa[$paraOndeVou]['status'] = 1;
						
						//atualizo a session
						
						$_SESSION['etapa'] = $r_etapa;
						
						
						// Verifico se estou vindo da etapa 3 para 180px
						$goto_url = '?do='.$_GET['do'];
						$goto = $deOndeVim;
						
					
					}
					
				}
				
			}

		}
		
		// RECUPERO AS INFORMAÇÕES DAS SESSÕES
		$r_form  = $_SESSION['form'];
		$r_etapa = $_SESSION['etapa'];
		
		
		// VALIDAÇÃO DOS FORMULÁRIOS
		if ($r_etapa['atual'] == 1) require_once('do/forms/validarPessoasFisicas.php');
		if ($r_etapa['atual'] == 2) require_once('do/forms/validarQuestionario.php');
		
		
		// Antes de iniciar o processo, verifico o usuario atual já escolhei o grupo
		// do curso em questão.
		// Se ele não escolheu, verifico se para aquele curso do determinado 
		// processo existem grupos.
		// Caso exista incluo o formulario para preenchimento.

		$query = "	SELECT 
						grupos.id_grupo, grupos.str_nome, grupos.str_descricao
					
					FROM 
						
						".DB_PREFIXO."Inscricoes AS inscricoes
						
						INNER JOIN ".DB_PREFIXO."CursosGrupos grupos ON 
						grupos.id_curso = inscricoes.id_curso
						AND grupos.ch_situacao =  'A'
						
					WHERE 
						
						inscricoes.id_inscricao = '".$r_form['id_inscricao']."'
						AND 
						inscricoes.id_grupo =0 ;";
		
		$resultado = mysql_query($query, $conectar);

		if(mysql_num_rows($resultado)){

			require_once('do/forms/cadastroGrupos.php');
		}
		else{

			/* --------------------------------------------------------
			 * ETAPAS
			 * -------------------------------------------------------- */
			
			// CONFIGURO A BARRA DAS ETAPAS
			?>
			<div class="smart-wizard form-horizontal">
				<div id="wizard" class="swMain">
					<ul>
					<?php
					$percent_progress = 0;
					foreach($r_etapa as $key => $valor){

						if ($key != 'atual'){

							switch($r_etapa[$key]['status']){

								// Bloqueada
								case 0:
									echo '
										<li>
											<a href="#step-1" class="'. ($key < $r_etapa['atual'] ? "done" : "") . '">
												<div class="stepNumber">
													'. $key .'
												</div>
												<span class="stepDesc">'. $r_etapa[$key]['nome'] .'
											</a>
										</li>
										';
									break;

								// Atual
								case 1:
									$finalizou = $r_etapa[$r_etapa['atual']]['permissao'];
									echo '
										<li>
											<a href="#step-1" class="'. (($key > 1 || $key == $r_etapa['atual']) && $finalizou == true ? "done" : "selected") . '">
												<div class="stepNumber">
													'. $key .'
												</div>
												<span class="stepDesc">'. $r_etapa[$key]['nome'] .'
											</a>
										</li>
										';
										$percent_progress = $key * 25;
									break;

								// Liberada
								case 2:
									$link = $r_etapa['atual'].','.$key;
									echo '
										<li>
											<a href="index.php?do='. base64_encode($link) . '" class="'. ($key < $r_etapa['atual'] ? "done" : "") . '">
												<div class="stepNumber">
													'. $key .'
												</div>
												<span class="stepDesc">'. $r_etapa[$key]['nome'] .'
											</a>
										</li>
										';
									break;

							}

						}

					}
					?>
					</ul>
					<div class="progress progress-striped active progress-sm">
						<div aria-valuemax="100" aria-valuemin="0" role="progressbar" class="progress-bar progress-bar-success step-bar" style="width: <?=$percent_progress?>%;">
							<span class="sr-only"> 0% Complete (success)</span>
						</div>
					</div>
				</div>
			</div>

			<?php
			
			/* --------------------------------------------------------
			 * CABECALHO
			 * -------------------------------------------------------- */
			 
			?>
			<div class="panel panel-default">
				<div class="panel-heading" style="padding-left: 20px;">
					Curso: <?=$r_cursos[$r_form['id_curso']]['nome']?>
					<div class="panel-tools">
						<a class="btn btn-xs btn-danger" href="sair.php">
							<i class="fa fa-times"></i> SAIR
						</a>
					</div>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-sm-3">
							<label><b>CPF</b></label>
						</div>
						<div class="col-sm-9">
							<label><?=$r_form['str_pessoa_cpf']?></label>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-3">
							<label><b>Valor da Inscricao</b></label>
						</div>
						<div class="col-sm-9 text-info">
							<label><b><?=cifrao($r_cursos[$r_form['id_curso']]['valor'])?></b></label>
						</div>
					</div>
					<?php if($r_form['str_pessoa_nome']){ ?>
					<div class="row">
						<div class="col-sm-3">
							<label><b>Nome</b></label>
						</div>
						<div class="col-sm-9">
							<label><?=utf8_encode($r_form['str_pessoa_nome']);?></label>
						</div>
					</div>
					<?php } ?>
					<?php if($r_form['id_inscricao']){ ?>
					<div class="row">
						<div class="col-sm-3">
							<label><b>Numero da Inscrição</b></label>
						</div>
						<div class="col-sm-9">
							<label><?=$r_form['num_inscricao']?></label>
						</div>
					</div>
					<?php } ?>
				</div>
			</div>

			<?php
	
			
			/* --------------------------------------------------------
			 * FORMULÁRIOS DE CADA ETAPA
			 * -------------------------------------------------------- */
			 
			switch($r_etapa['atual']){
			
				// Cadastro
				case 1: require_once('do/forms/cadastroPessoasFisicas.php'); break;
				
				// Questionario
				case 2: require_once('do/forms/cadastroQuestionario.php'); break;
				
				// Conferencia e Aceites
				case 3: require_once('do/forms/cadastroAceiteConferencia.php'); break;
				
				// Gerar Boletos
				case 4: require_once('do/forms/cadastroBoleto.php'); break;
			}
			
		}// Fim do else -  validação do grupo
	
	}
	else{
	
		// Recupero as informações
		$r_form  = $_SESSION['form'];
		require_once('body/areaInscrito/index.php');

	}

	
?>
