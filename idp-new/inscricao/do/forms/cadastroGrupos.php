<form id="formGrupos" method="POST">

<div class="row">

	<div class="col-md-12">

		<div class="panel panel-default">

			<div class="panel-heading" style="padding-left: 20px;">
				Seja Bem-vindo <?=utf8_encode($r_form['str_pessoa_nome'])?>!
				<div class="panel-tools">
					<a class="btn btn-xs btn-danger" href="sair.php">
						<i class="fa fa-times"></i> SAIR
					</a>
				</div>
			</div>
			<div class="panel-body">


				<div class="alert alert-info">
					O processo de transferência para ingresso no curso de graduação em <?=$r_cursos[$r_form['id_curso']]['nome']?> do IDP/SP
					será permitido exclusivamente para candidatos que se enquadrem em uma das categorias abaixo:
				</div>

				<hr>


				<input type="hidden" id="id_inscricao" name="id_inscricao" value="<?=$r_form['id_inscricao']?>" />

				<div class="form-group" id="box-options-grupo">
				<?php
					while($linha = mysql_fetch_object($resultado)){

						$id_grupo 		= $linha->id_grupo;
						$str_nome 		= utf8_encode($linha->str_nome);
						$str_descricao 	= utf8_encode($linha->str_descricao);

						?>
						<div class="radio" style="padding-bottom: 10px;">
							<label for="<?= $id_grupo; ?>">
								<input required class="square-green" type='radio' id='<?= $id_grupo; ?>' name='id_grupo' value='<?= $id_grupo; ?>'>
								<b><?= $str_nome; ?>:</b> <?= $str_descricao; ?>
							</label>
						</div>

				<?php }	?>
				</div>

			</div>
		</div>

		<div class="form-group text-center">
			<button type="submit" class="btn btn-primary">Li, concordo e aceito os termos acima.</button>
		</div>
	</div>
</div>

</form>

<hr>


					