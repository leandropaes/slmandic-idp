<?php
	
	// RECUPERO O TEXTO DO TERMO DE RESPONSABILIDADE
	
		$query = "SELECT txt_mensagem_inscricao_termos, str_arquivo_manual FROM ".DB_PREFIXO."Processos WHERE id_processo = ".$id_processo.";";

		$resultado = mysql_query($query, $conectar);
	
		$linha = mysql_fetch_object($resultado);
		
		$txt_mensagem_inscricao_termos = $linha->txt_mensagem_inscricao_termos;
		$str_arquivo_manual = $linha->str_arquivo_manual;
	
	
	
	// RECUPERO AS INFORMAÇÕES DE CADASTRO
	
		$query = "SELECT * FROM ".DB_PREFIXO."PessoasFisicas WHERE id_pessoafisica = ".$r_form['id_pessoafisica'].";";
	
		$resultado = mysql_query($query, $conectar);
	
		$linha = mysql_fetch_object($resultado);
		
		$r_cadastro = array(
			'str_pessoa_cpf' 			=> array(0,'CPF', 					$linha->str_pessoa_cpf),
			'str_pessoa_nome' 		  	=> array(0,'Nome', 					utf8_encode($linha->str_pessoa_nome)),
			'str_pessoa_rgrne' 		 	=> array(1,'RG', 					$linha->str_pessoa_rgrne),
			'str_pessoa_ssp' 			=> array(1,'Orgão Expedidor',		strtoupper($linha->str_pessoa_ssp)),
			'dt_pessoa_nascimento' 		=> array(1,'Data de Nascimento', 	formatarData($linha->dt_pessoa_nascimento, '')),
			'ch_pessoa_sexo' 			=> array(1,'Sexo', 					(($linha->ch_pessoa_sexo == 'M')? 'Masculino' : 'Feminino')),
			'str_pessoa_naturalde' 		=> array(1,'Naturalidade', 			utf8_encode($linha->str_pessoa_naturalde)),
			'str_sistema_senha' 		=> array(0,'Senha', 				$linha->str_sistema_senha),
			'dt_pessoa_registro' 		=> array(0,'Registro', 				$linha->dt_pessoa_registro),
		);

	// RECUPERA ENDEREÇO

		$r_endereco = array(
			'str_endereco_cep' 			=> array(1,'CEP', 					$linha->str_endereco_cep),
			'str_endereco_logradouro' 	=> array(1,'Logradouro',			utf8_encode($linha->str_endereco_logradouro)),
			'str_endereco_numero' 		=> array(1,'Numero', 				$linha->str_endereco_numero),
			'str_endereco_bairro' 		=> array(1,'Bairro', 				utf8_encode($linha->str_endereco_bairro)),
			'str_endereco_cidade' 		=> array(1,'Cidade', 				utf8_encode($linha->str_endereco_cidade)),
			'str_endereco_estado' 		=> array(1,'Estado', 				$linha->str_endereco_estado),
			'str_endereco_complemento'	=> array(1,'Complemento', 			utf8_encode($linha->str_endereco_complemento)),
			'str_endereco_pais' 	    => array(1,'Pais', 					utf8_encode($linha->str_endereco_pais)),

		);

	// RECUPERA DADOS PARA CONTATO

		$r_dados_contato = array(
			'str_telefone_residencial' 	=> array(1,'Telefone Residencial', $linha->str_telefone_residencial),
			'str_telefone_comercial'   	=> array(1,'Telefone Comercial', 	$linha->str_telefone_comercial),
			'str_telefone_celular' 	  	=> array(1,'Telefone Celular', 		$linha->str_telefone_celular),
			'str_pessoa_email'			=> array(1,'E-mail', 				$linha->str_pessoa_email),
			'ch_comunicacao_sms' 		=> array(1,'Aceita envio de informações do vestibular por SMS?', 	(($linha->ch_comunicacao_sms == 'S')? 'Sim' : 'Não') )

		);
		
	// 	RECUPERO OS INFORMAÇÕES DO PROCESSO
	
		$query = "SELECT * FROM ".DB_PREFIXO."Inscricoes WHERE id_inscricao = {$_SESSION['form']['id_inscricao']}";
		$resultado = mysql_query($query, $conectar);
		$linha = mysql_fetch_object($resultado);

	// 	ENEM
		$r_opt_enem[] = array(1, 'Deseja utilizar sua nota do ENEM?', (($linha->ch_enem == 'S')? 'Sim':'Não') );
		if($linha->ch_enem == 'S'){
			$r_opt_enem[] = array(1, 'Ano de Exame', utf8_encode($linha->str_enem_ano) );
			$r_opt_enem[] = array(1, 'Nº de Inscrição', utf8_encode($linha->str_enem_inscricao) );
		}

	// INFORMAÇÕES
		$r_incricao[] = array(1, 'Colégio em que concluiu o Ensino Médio', utf8_encode($linha->str_ensinomedio_local) );
		$r_incricao[] = array(1, 'Ano de Conclusão', utf8_encode($linha->str_ensinomedio_conclusao) );


		$r_incricao[] = array(1, 'Você é portador de alguma necessidade especial?', (($linha->ch_necessidade_especial == 'S')? 'Sim':'Não') );
		if($linha->ch_necessidade_especial == 'S'){
			$r_incricao[] = array(1, 'Necessidade', utf8_encode($linha->str_necessidade_especial) );
		}
		$r_incricao[] = array(1, 'Necessita de carteira para canhoto?', (($linha->ch_necessidade_canhoto == 'S')? 'Sim':'Não') );

		$r_incricao[] = array(1, 'Está prestando vestibular em outra faculdade?', (($linha->ch_comunicacao_outrafaculdade == 'S')? 'Sim':'Não') );
		if($linha->ch_comunicacao_outrafaculdade == 'S'){
			$r_incricao[] = array(1, 'Qual(is) Curso(s)?', utf8_encode($linha->str_comunicacao_qualcurso) );
			$r_incricao[] = array(1, 'Qual(is) Instituição(ões)?', utf8_encode($linha->str_comunicacao_quaislocais) );
		}

		$r_incricao[] = array(1, 'Como ficou sabendo do vestibular?', utf8_encode($linha->str_comunicacao_ficousabendo));

	// LINK
		$link = $r_etapa['atual'].',1';
		$link = base64_encode($link);
?>

<div class="page-header">
	<h4>Confira seus Dados</h4>
</div>

<div class="row">
	<div class="col-sm-12 form-horizontal">

		<div class="panel-heading text-center" style="padding-left: 0 !important;">
			Dados Pessoais
			<div class="panel-tools">
				<a class="btn btn-xs btn-primary" href="index.php?do=<?=$link?>#tb-dados-pessoais">
					<i class="fa fa-pencil"></i> EDITAR
				</a>
			</div>
		</div>
		<br>

		<?php foreach($r_cadastro as $chave => $valor) { ?>
			<?php if($valor[0]) { ?>
			<div class="form-group">
				<label class="col-md-4 control-label">
					<?php echo $valor[1]; ?>
				</label>
				<div class="col-md-4">
					<input class="form-control" value="<?php echo $valor[2]; ?>" disabled>
				</div>
			</div>
			<?php } ?>
		<?php } ?>

	</div>
</div>

<div class="row">
	<div class="col-sm-12 form-horizontal">

		<div class="panel-heading text-center" style="padding-left: 0 !important;">
			Endereço Residencial
			<div class="panel-tools">
				<a class="btn btn-xs btn-primary" href="index.php?do=<?=$link?>#tb-endereco-residencial">
					<i class="fa fa-pencil"></i> EDITAR
				</a>
			</div>
		</div>
		<br>

		<?php foreach($r_endereco as $chave => $valor) { ?>
			<?php if($valor[0]) { ?>
				<div class="form-group">
					<label class="col-md-4 control-label">
						<?php echo $valor[1]; ?>
					</label>
					<div class="col-md-4">
						<input class="form-control" value="<?php echo $valor[2]; ?>" disabled>
					</div>
				</div>
			<?php } ?>
		<?php } ?>

	</div>
</div>

<div class="row">
	<div class="col-sm-12 form-horizontal">

		<div class="panel-heading text-center" style="padding-left: 0 !important;">
			Dados para Contato
			<div class="panel-tools">
				<a class="btn btn-xs btn-primary" href="index.php?do=<?=$link?>#tb-dados-contato">
					<i class="fa fa-pencil"></i> EDITAR
				</a>
			</div>
		</div>
		<br>

		<?php foreach($r_dados_contato as $chave => $valor) { ?>
			<?php if($valor[0]) { ?>
				<div class="form-group">
					<label class="col-md-4 control-label">
						<?php echo $valor[1]; ?>
					</label>
					<div class="col-md-4">
						<input class="form-control" value="<?php echo $valor[2]; ?>" disabled>
					</div>
				</div>
			<?php } ?>
		<?php } ?>

	</div>
</div>

<div class="row">
	<div class="col-sm-12 form-horizontal">

		<div class="panel-heading text-center" style="padding-left: 0 !important;">
			Enem
			<div class="panel-tools">
				<a class="btn btn-xs btn-primary" href="index.php?do=<?=$link?>#tb-enem">
					<i class="fa fa-pencil"></i> EDITAR
				</a>
			</div>
		</div>
		<br>

		<?php foreach($r_opt_enem as $chave => $valor) { ?>
			<?php if($valor[0]) { ?>
				<div class="form-group">
					<label class="col-md-4 control-label">
						<?php echo $valor[1]; ?>
					</label>
					<div class="col-md-4">
						<input class="form-control" value="<?php echo $valor[2]; ?>" disabled>
					</div>
				</div>
			<?php } ?>
		<?php } ?>

	</div>
</div>

<div class="row">
	<div class="col-sm-12 form-horizontal">

		<div class="panel-heading text-center" style="padding-left: 0 !important;">
			Informações
			<div class="panel-tools">
				<a class="btn btn-xs btn-primary" href="index.php?do=<?=$link?>#tb-informacoes">
					<i class="fa fa-pencil"></i> EDITAR
				</a>
			</div>
		</div>
		<br>

		<?php foreach($r_incricao as $chave => $valor) { ?>
			<?php if($valor[0]) { ?>
				<div class="form-group">
					<label class="col-md-4 control-label">
						<?php echo $valor[1]; ?>
					</label>
					<div class="col-md-4">
						<input class="form-control" value="<?php echo $valor[2]; ?>" disabled>
					</div>
				</div>
			<?php } ?>
		<?php } ?>

	</div>
</div>

<!-- TERMO -->
<div class="page-header">
	<h4>Termo</h4>
</div>
<?= $txt_mensagem_inscricao_termos?>
<br>

<!-- EDITAL -->
<div class="page-header">
	<h4>Edital</h4>
</div>
<div class="row">
	<div class="col-sm-12">
		<p class="text-center"><a href="<?='../arquivos/'.$str_arquivo_manual?>" target="_blank">Clique aqui para visualizar.</a></p>

		<div id="declaracao">
			<p>Declaro, ainda, que a formalização e conclusão da inscrição implica em concordar e aceitar as normas e condições constantes do Edital 01/2017, as quais li, concordo e aceito. Declaro ainda estar ciente do devido recolhimento da respectiva taxa de inscrição.</p>

			<p style="color: red;">Ao clicar no bot&atilde;o abaixo, voc&ecirc; finalizar&aacute; sua inscri&ccedil;&atilde;o no vestibular. Tenha certeza que revisou todas as informa&ccedil;&otilde;es.</p></p>
			<br/>

			<form id="formAceite" action="javascript:func()" method="POST" class="text-center">

				<button type="submit" class="btn btn-primary">Li, entendi e aceito todos os Termos do Edital</button>

				<div id="carregando_aceite" class="text-center">
					<img src="images/carregando.gif" />
				</div>
				<div id="msg_aceite" class="alert alert-danger">
					<b>Alerta:</b> Não foi possível finalizar o processo nesse momento. Tente mais tarde.
				</div>

			</form>

			<script type="text/javascript" src="do/script/validaFormAceite.js"></script>

		</div>
	</div>
</div>

<hr>
