<div class="row">
	<div class="col-sm-12">
		<form id="frmEtapaQuestionario" action="index.php" method="POST">

			<!-- Mensagem de Erro -->
			<div id="msg_questionario" class="alert alert-danger <?php echo (($eValido) ? 'hidden' : '') ?>"><b>Alerta:</b> É obrigatório o preenchimento de todos os campos.</div>

			<?php
			foreach($vestibular as $key => $row){ ?>

			<div class="form-group">
				<!-- TITULO DA PERGUNTA -->
				<div class="page-header">
					<h4><?= $key; ?></h4>
				</div>

				<?php
				foreach($row as $key_2 => $row_2){

					// PERGUNTAS TEXTAREA
					if($row_2['tipo'] == 'T'){
						if (($validacao[$row_2['id']]['msg']) and (!$validacao[$row_2['id']]['msgExibida'])){ $validacao[$row_2['id']]['msgExibida'] = TRUE; echo '<div class="aux-erro-questionario text-danger">' . $validacao[$row_2['id']]['msg'] . '</div>'; }
						?>
						<div class="form-group">
							<textarea rows="4" cols="50" name="pergunta[<?= $row_2['id'] ?>]" id="nome" class="form-control" required /><?=$validacao[$row_2['id']]['valor']?></textarea><?= $key_2; ?>
						</div>
						<?php

					// PERGUNTAS DATA
					}else if($row_2['tipo'] == 'D'){
						if (($validacao[$row_2['id']]['msg']) and (!$validacao[$row_2['id']]['msgExibida'])){ $validacao[$row_2['id']]['msgExibida'] = TRUE; echo '<div class="aux-erro-questionario text-danger">' .$validacao[$row_2['id']]['msg'] . '</div>'; }
						?>
						<div class="form-group">
							<input type="text" required name="pergunta[<?= $row_2['id'] ?>]" id="nome" class="data" data-mask="00/00/0000" value="<?=$validacao[$row_2['id']]['valor']?>" /><?= $key_2; ?>
						</div>
						<?php

						// PERGUNTAS RADIO
					}else if($row_2['tipo'] == 'R'){
						if (($validacao[$row_2['id']]['msg']) and (!$validacao[$row_2['id']]['msgExibida'])){ $validacao[$row_2['id']]['msgExibida'] = TRUE; echo '<div class="aux-erro-questionario text-danger">' . $validacao[$row_2['id']]['msg'] . '</div>'; }

						($validacao[$row_2['id']]['valor'] == $row_2['value'])? $checked = 'CHECKED' : $checked = NULL;
						?>
						<div class="radio" style="padding-bottom: 10px;">
							<label style="">
								<input required class="r square-green" type="radio" name="pergunta[<?= $row_2['id'] ?>]" id="radio" value="<?= $row_2['value'] ?>" <?=$checked?> />
								<?= $key_2; ?>
							</label>
						</div>
						<?php

						// PERGUNTAS RADIO (OUTRO)
					}else if($row_2['tipo'] == 'RS'){

						if (($validacao[$row_2['id']]['msg']) and (!$validacao[$row_2['id']]['msgExibida'])){ $validacao[$row_2['id']]['msgExibida'] = TRUE; echo '<div class="aux-erro-questionario text-danger">' . $validacao[$row_2['id']]['msg'] . '</div>'; }


						if((is_numeric($validacao[$row_2['id']]['valor'])) || ($validacao[$row_2['id']]['valor'] == NULL)){

							($validacao[$row_2['id']]['valor'] == $row_2['value'])? $checked = 'CHECKED' : $checked = NULL;
							?>
							<div class="radio" style="padding-bottom: 10px;">
								<label>
									<input required class="rs square-green" add="n" tname="pergunta[<?= $row_2['id'] ?>]" type="radio" name="pergunta[<?= $row_2['id'] ?>]" id="radio" value="<?= $row_2['value'] ?>" <?=$checked?> />
									<?= $key_2; ?>
								</label>
							</div>
							<?php
						}
						else{?>

							<div class="radio" style="padding-bottom: 10px;">
								<label>
									<input required class="rs square-green" add="s" tname="pergunta[<?= $row_2['id'] ?>]" type="radio" name="pergunta[<?= $row_2['id'] ?>]" id="radio" value="<?= $row_2['value'] ?>" checked />
									<?= $key_2; ?>
								</label>
							</div>
							<div class="remove">
								<input required class="form-control" name="pergunta[<?= $row_2['id'] ?>]" type='text' value="<?=$validacao[$row_2['id']]['valor']?>" />
							</div>

							<?php
						}

					}
				}
				?>
			</div>
			<?php
			}// fim do foreach
			?>

			<input type="hidden" name="salvarQuestionario" id="salvarQuestionario" value="salvarQuestionario" />
			<br>

			<div class="text-center">
				<button type="submit" class="btn btn-primary">Salvar Questionário</button>
			</div>
		</form>

	</div>
</div>

<hr>
					