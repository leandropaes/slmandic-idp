/* Máscaras ER */
function mascara(o,f){
	v_obj=o
	v_fun=f
	setTimeout("execmascara()",1)
}
function execmascara(){
	v_obj.value=v_fun(v_obj.value)
}
function mtel(v){
	v=v.replace(/\D/g,"");                  //Remove tudo o que não é dígito
	v=v.replace(/^(\d{2})(\d)/g,"($1) $2"); //Coloca parênteses em volta dos dois primeiros dígitos
	v=v.replace(/(\d)(\d{4})$/,"$1-$2");    //Coloca hífen entre o quarto e o quinto dígitos
	return v;
}

$(document).ready(function(){

	//------------------------------------------------------------------------------
	// ADICIONAR E REMOVER CAMPO QUESTIONACIO SOCIO ECONOMICO
	//------------------------------------------------------------------------------
	
	$('body').on('ifClicked', 'input.rs', function(){


		var add = $(this).attr('add');
		
		if (add == 'n'){
			var name = $(this).attr('tname');
			var row = "<div class='form-group remove'><input class='form-control' required name='"+name+"' type='text' /></div>";
			
			$(this).attr("add", "s");
			$(this).parent().parent().parent().after(row);
		}

	});  

	$('body').on('ifClicked', 'input.r', function(){

		$(this).parent().parent().parent().parent().find('.remove').remove();
		$(this).parent().parent().parent().parent().find('input.rs').attr("add", "n");

	});

	//------------------------------------------------------------------------------
	// COMPLETAR CEP
	//------------------------------------------------------------------------------

		$("#str_endereco_cep").blur(function(e){
			if($.trim($("#str_endereco_cep").val()) != ""){
				$.getScript("http://cep.republicavirtual.com.br/web_cep.php?formato=javascript&cep="+$("#str_endereco_cep").val(), function(){
					if(resultadoCEP["resultado"]){
						$("#str_endereco_logradouro").val(unescape(resultadoCEP["tipo_logradouro"])+" "+unescape(resultadoCEP["logradouro"]));
						$("#str_endereco_bairro").val(unescape(resultadoCEP["bairro"]));
						$("#str_endereco_cidade").val(unescape(resultadoCEP["cidade"]));
						$("#str_endereco_estado").val(unescape(resultadoCEP["uf"]));
						
						
						if (unescape(resultadoCEP["cidade"]) != ''){
							$("#str_endereco_numero").focus();
						}
						else{
							$("#str_endereco_estado").val("(Selecione)");
							$("#str_endereco_estado").focus();
						}
						
					}
				});				
			}		
		});
	


	// Mascara Formulario Tela de Login
	//$('#str_cpf').mask('999.999.999-99');
	//$('#inscrito_str_cpf').mask('999.999.999-99');
	//$('#str_telefone_residencial').mask('(99) 9999-9999');
	//$('#str_telefone_comercial').mask('(99) 9999-9999');
	//$('#str_endereco_cep').mask('99.999-999');
	//$('#str_telefone_celular').focusout(function(){var phone, element; element = $(this); element.unmask(); phone = element.val().replace(/\D/g, '');if(phone.length > 10) {element.mask("(99) 99999-999?9");}else {element.mask("(99) 9999-9999?9");}}).trigger('focusout');
	
	// Mascaras Formulario Tela cadastro
	//$('#str_pessoa_cpf').mask('999.999.999-99');
	
	// Mascaras Questionario
	//$(".data").mask("99/99/9999");
	//$("#dt_pessoa_nascimento").mask("99/99/9999");
	
	


});

 function startTime() {
     var today=new Date();
     var h=today.getHours();
     var m=today.getMinutes();
     var s=today.getSeconds();
     // add a zero in front of numbers<10
     m=checkTime(m);
     s=checkTime(s);
     document.getElementById('hora').innerHTML=h+":"+m+":"+s;
     t=setTimeout('startTime()',500);
 }

 function checkTime(i){
 if (i<10) {
     i="0" + i;
 }
     return i;
 }
