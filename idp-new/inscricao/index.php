<?php 
require_once('__lib__.php');

if (isset($_SESSION['autenticacao']) && $_SESSION['autenticacao']['painel'] == 'inscrito') {
	// Recupero as informações
	$r_form  = $_SESSION['form'];

	require_once('body/areaInscrito/includes/header.php');
	require_once('body/areaInscrito/index.php');
	require_once('body/areaInscrito/includes/footer.php');

} else {
	require_once('body/cabecalho.php');

	if(!isset($_GET['recuperar_senha']))
		require_once('body/conteudo.php');
	else
		require_once('body/recuperarSenha/index.php');

	require_once('body/rodape.php');
}
?>