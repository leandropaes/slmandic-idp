<?php
// RECUPERO OS CURSOS DO PROCESSO ATUAL

$query = "SELECT * FROM  ".DB_PREFIXO."Cursos WHERE id_processo	= '".$id_processo."' AND ch_situacao = 'A';";
$resultado = mysql_query($query, $conectar);
$r_cursos = array();
while($linha = mysql_fetch_object($resultado)){
	$r_cursos[$linha->id_curso] = array('nome' => utf8_decode($linha->str_nome), 'valor' => $linha->int_valor);
}

// FIM DA RECUPERAÇÃO
?>

<?php if (!isset($_SESSION['autenticacao'])){ ?>

<div class="row">

	<?php if($existeProcesso){ ?>

	<div class="col-md-6">
		<div class="panel panel-default">
			<div class="panel-heading" style="padding-left: 20px;">
				Nova Inscrição
			</div>
			<div class="panel-body">

				<div class="inscrever">
					<?php
						if($txt_incricoes){
							echo '<h3 class="text-center">',$txt_incricoes,'</h3>';
						}
						else{
							?>
							<div id="bemvindo"></div>

							<div id="divformInscrever">

								<form id="formInscrever" action="javascript:func()" method="POST">

									<input type="hidden" id="id_processo" name="id_processo" value="<?=$id_processo?>" />
									<input type="hidden" id="txt_ano" name="txt_ano" value="<?=$txt_ano?>" />

									<div class="form-group">
										<label for="id_curso">Curso</label>
										<select id="id_curso" name="id_curso" class="form-control" >
											<option value="">(Selecione)</option>
											<?php
											foreach($r_cursos as $key => $value){
												echo '<option value="',$key,'">',cifrao($value['valor']),' - ',$value['nome'],'</option>';
											}
											?>
										</select>
									</div>

									<div class="form-group">
										<label for="str_cpf">CPF</label>
										<input type="text" data-mask="000.000.000-00" id="str_cpf" name="str_cpf" class="form-control">
									</div>

									<div class="form-group">
										<button type="submit" class="btn btn-primary btn-block">Fazer Inscrição</button>
									</div>
									<br>

									<div id="msg-inscrever"></div>

								</form>

								<div id="carregando_begin1">
									<img src="images/carregando.gif" />
								</div>

							</div>

							<!-- Se já existir CPF mostra esse form -->

							<div id="divformInscreverSenha">

								<form id="formInscreverSenha" action="javascript:func()" method="POST">

									<input type="hidden" id="id_processo" name="id_processo" value="<?=$id_processo?>" />
									<input type="hidden" id="txt_ano" name="txt_ano" value="<?=$txt_ano?>" />

									<div class="form-group">
										<label for="id_curso">Curso</label>
										<select id="id_curso" name="id_curso" class="form-control">
											<option value="">(Selecione)</option>
											<?php
											foreach($r_cursos as $key => $value){
												echo '<option value="',$key,'">',cifrao($value['valor']),' - ',$value['nome'],'</option>';
											}
											?>
										</select>
									</div>

									<div class="form-group">
										<label for="str_cpf">CPF</label>
										<input type="text" data-mask="000.000.000-00" id="str_cpf" name="str_cpf" class="form-control">
									</div>

									<div class="form-group">
										<label for="str_senha">Senha</label>
										<input type="password" id="str_senha" name="str_senha" class="form-control">
									</div>

									<div class="form-group">
										<button type="submit" class="btn btn-primary btn-block">Fazer Inscrição</button>
									</div>


									<div id="msg-inscrever" class="alert alert-danger hidden"></div>

									<a href="index.php?recuperar_senha">
										<div class="telaInicial-recuperarSenha text-center">Recuperar Senha</div>
									</a>

								</form>

								<div id="carregando_begin" class="text-center">
									<img src="images/carregando.gif" />
								</div>

							</div>

							<?php
						}
					?>
				</div>

			</div>
		</div>
	</div>

	<?php } ?>

	<div class="<?php echo (($existeProcesso) ? 'col-md-6' : 'col-md-6 col-md-offset-3'); ?>">
		<div class="panel panel-default">
			<div class="panel-heading" style="padding-left: 20px;">
				Área do Inscrito
			</div>
			<div class="panel-body">

				<div class="inscritos">
					<form id="formLogin" action="javascript:func()" method="POST">

						<div class="form-group">
							<label for="str_cpfLogin">CPF</label>
							<input type="text" data-mask="000.000.000-00" id="str_cpfLogin" name="str_cpfLogin" class="form-control">
						</div>

						<div class="form-group">
							<label for="str_senhaLogin">Senha</label>
							<input type="password" id="str_senhaLogin" name="str_senhaLogin" class="form-control">
						</div>

						<div class="form-group">
							<button class="btn btn-primary btn-block">Acessar</button>
						</div>

					</form>

					<div id="msg-inscrito" class="alert alert-danger hidden"></div>

					<a href="index.php?recuperar_senha"><div class="telaInicial-recuperarSenha text-center">Recuperar Senha</div></a>

					<div id="carregando_begin">
						<img src="images/carregando.gif" />
					</div>

				</div>

			</div>
		</div>
	</div>

</div>


<div id="conteudo">

	<br>
	
	<?php if($existeProcesso && $arquivo_edital){?>


	<div class="well well-sm text-center">
		<p class="no-margin-bottom">Edital - N° <?php echo $nome_edital; ?></p>
		<a href="<?=URL?>/vestibular/arquivos/<?=$arquivo_edital?>" class="btn btn-link no-margin-bottom" target="_blank">
			Clique aqui
		</a>
	</div>

	<?php }
	
}
else{
	
	require_once('do/forms/controlarEtapas.php');
	
}?>
	
</div>