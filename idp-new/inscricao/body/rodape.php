				<div class="row">
					<div class="col-xs-12 text-center">
						© 2015 IDP / SP. Todos os direitos reservados.
					</div>
				</div>
				<br>

				</div>
			</div>
		</div>
	</div>

	<script src="theme/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
	<script src="theme/plugins/bootstrap/js/bootstrap.min.js"></script>
	<script src="theme/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
	<script src="theme/plugins/blockUI/jquery.blockUI.js"></script>
	<script src="theme/plugins/iCheck/jquery.icheck.min.js"></script>
	<script src="theme/plugins/perfect-scrollbar/src/jquery.mousewheel.js"></script>
	<script src="theme/plugins/perfect-scrollbar/src/perfect-scrollbar.js"></script>
	<script src="theme/plugins/less/less-1.5.0.min.js"></script>
	<script src="theme/plugins/jquery-cookie/jquery.cookie.js"></script>
	<script src="theme/js/main.js"></script>
	<script type="text/javascript" src="js/jquery.mask.js"></script>
	<script src="theme/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
	<script src="theme/plugins/jquery-validation/localization/messages_pt_BR.js"></script>
	<script src="js/jquery.validate.cpf.js"></script>
	<script src="js/jquery.validate.dateBR.js"></script>
	<script src="theme/plugins/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>
	<script src="theme/js/form-wizard.js"></script>
	<script src="do/script/validaFormGeralInscricao.js"></script>
</body>
<!-- end: BODY -->
</html>