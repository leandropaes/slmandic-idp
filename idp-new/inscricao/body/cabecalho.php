	<?php
	// Monto a query
	// Verifico se existe processo em aberto
	$query ="SELECT 
				id_processo, 
				num_processo_ano,
				str_processo_apelido,
				num_vencimentopadrao,
				num_processo_ano,
				str_processo_semestre,
				dt_vencimento_maximo,
				dt_inscricao_inicio,
				dt_inscricao_fim,
				NOW() as datetime,
				DATE_FORMAT( `dt_inscricao_inicio`, '%H:%i de %d/%m/%Y' ) as dateInicio,
				str_arquivo_portaria,
				str_arquivo_edital,
				str_nome_portaria,
				str_nome_edital
			FROM 
				".DB_PREFIXO."Processos 
			WHERE
				ch_situacao = 'A'
			LIMIT 1
			";
				
	$resultado = mysql_query($query, $conectar); 
	
	if (mysql_num_rows($resultado)){
	
		$linha = mysql_fetch_object($resultado);
		
		// recupero as informações
		$txt_ano     		  = $linha->num_processo_ano;
		$str_processo_apelido = $linha->str_processo_apelido;
		$num_processo_ano 	  = $linha->num_processo_ano;
		$str_processo_semestre= $linha->str_processo_semestre;
		$num_vencimentopadrao = $linha->num_vencimentopadrao;
		$dt_vencimento_maximo = $linha->dt_vencimento_maximo;
		$id_processo 		  = $linha->id_processo;
		$dt_inscricao_inicio  = $linha->dt_inscricao_inicio;
		$dt_inscricao_fim     = $linha->dt_inscricao_fim;
		$datetimeAtual        = $linha->datetime;
		$dateInicio           = $linha->dateInicio;
		
		$arquivo_portaria	  = $linha->str_arquivo_portaria;
		$arquivo_edital		  = $linha->str_arquivo_edital;
		
		$nome_portaria	  = $linha->str_nome_portaria;
		$nome_edital      = $linha->str_nome_edital;
		
		
		// Controle se já iniciou ou encerrou as incrições
		if ($datetimeAtual < $dt_inscricao_inicio)
			$txt_incricoes = 'As Inscrições iniciam <br/>a partir das<br/><br/>'.$dateInicio;
			
		elseif($datetimeAtual > $dt_inscricao_fim)
			$txt_incricoes = 'Inscrições encerradas.';
			
		else
			$txt_incricoes = FALSE;
		
		// defino que existe um processo vigente
		$existeProcesso = TRUE;
	}
	else{
		$existeProcesso = FALSE;
	}
	?>
	<!DOCTYPE html>
	<!-- Template Name: Clip-One - Responsive Admin Template build with Twitter Bootstrap 3.x Version: 1.3 Author: ClipTheme -->
	<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
	<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
	<!--[if !IE]><!-->
	<html lang="en" class="no-js">
	<!--<![endif]-->
	<!-- start: HEAD -->
	<head>
		<title>IDP - Vestibular</title>
		<!-- start: META -->
		<meta charset="utf-8" />
		<!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta content="" name="description" />
		<meta content="" name="author" />
		<!-- end: META -->
		<!-- start: MAIN CSS -->
		<link rel="stylesheet" href="theme/plugins/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="theme/plugins/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="theme/fonts/style.css">
		<link rel="stylesheet" href="theme/css/main.css">
		<link rel="stylesheet" href="theme/css/main-responsive.css">
		<link rel="stylesheet" href="theme/plugins/iCheck/skins/all.css">
		<link rel="stylesheet" href="theme/plugins/bootstrap-colorpalette/css/bootstrap-colorpalette.css">
		<link rel="stylesheet" href="theme/plugins/perfect-scrollbar/src/perfect-scrollbar.css">
		<link rel="stylesheet" href="theme/css/theme_light.css" type="text/css" id="skin_color">
		<link rel="stylesheet" href="theme/css/print.css" type="text/css" media="print"/>
		<!--[if IE 7]>
		<link rel="stylesheet" href="theme/plugins/font-awesome/css/font-awesome-ie7.min.css">
		<![endif]-->
		<!-- end: MAIN CSS -->

		<!-- STYLE / CSS -->
		<link rel="stylesheet" type="text/css" href="css/style.css">

		<!-- start: MAIN JAVASCRIPTS -->
		<!--[if lt IE 9]>
		<script src="theme/plugins/respond.min.js"></script>
		<script src="theme/plugins/excanvas.min.js"></script>
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<![endif]-->
		<!--[if gte IE 9]><!-->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
		<!--<![endif]-->

		<script type="text/javascript" src="js/jquery.util.js"></script>

	</head>
	<!-- end: HEAD -->
	<!-- start: BODY -->
	<body class="page-full-width">

	<div class="main-content">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
				<br>

				<div class="row">
					<div class="col-xs-12">
						<div class="page-header no-border-top">
							<img src="images/logo.png" class="pull-left" width="90" />
							<h1 id="titulo-vestibular">
								<?=($existeProcesso)? ('Vestibular '.$num_processo_ano.'<br/>'.utf8_encode($str_processo_semestre)): 'VESTIBULAR';?>
							</h1>
							<div style="clear: both;"></div>
						</div>
					</div>
				</div>