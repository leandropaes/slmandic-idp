<?php
// Verifico se existe processo em aberto
$query ="SELECT	num_processo_ano, str_processo_semestre FROM ".DB_PREFIXO."Processos WHERE ch_situacao = 'A' LIMIT 1";

$resultado = mysql_query($query, $conectar);
if (mysql_num_rows($resultado)){

    $linha = mysql_fetch_object($resultado);

    // recupero as informações
    $num_processo_ano 	  = $linha->num_processo_ano;
    $str_processo_semestre= $linha->str_processo_semestre;

    // defino que existe um processo vigente
    $existeProcesso = TRUE;
}
else{
    $existeProcesso = FALSE;
}

// Menu Vestibular
$query = "	SELECT
                  processos.num_processo_ano
                , processos.id_processo
                , inscricoes.id_inscricao
                , inscricoes.num_inscricao
                , cursos.str_nome

            FROM
                ".DB_PREFIXO."Processos AS processos

                INNER JOIN ".DB_PREFIXO."Inscricoes inscricoes ON
                processos.id_processo = inscricoes.id_processo
                AND
                inscricoes.id_pessoafisica = '".$r_form['id_pessoafisica']."'

                INNER JOIN ".DB_PREFIXO."Cursos cursos ON
                inscricoes.id_curso = cursos.id_curso

            ORDER
                BY num_processo_ano DESC;";

$resultado = mysql_query($query, $conectar);

// GERO O MENU
$processo_ano = array();
$menu         = array();

while($linha = mysql_fetch_object($resultado)){

    ((isset($_GET['inscricao']))&&(base64_decode($_GET['inscricao']) == $linha->id_inscricao))? $atual = '_atual': $atual = NULL;

    if (!in_array($linha->num_processo_ano, $processo_ano)){
        $menu['ano'][]  = $linha->num_processo_ano;
        $processo_ano[] = $linha->num_processo_ano;
    }

    $menu[$linha->num_processo_ano][] = $atual;

    $menu['sub_menu'][] = array(
        "ano"				=> $linha->num_processo_ano
    , "link"			=>'?inscricao='.base64_encode($linha->id_inscricao)
    , "num_inscricao"	=>'['.$linha->num_inscricao.']'
    , "curso"			=>$linha->str_nome
    , "atual"			=>$atual
    );

}
?>

<!DOCTYPE html>
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<head>
    <title>IDP - Vestibular</title>
    <meta charset="utf-8" />
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="" name="description" />
    <meta content="" name="author" />

    <link rel="stylesheet" href="theme/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="theme/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="theme/fonts/style.css">
    <link rel="stylesheet" href="theme/css/main.css">
    <link rel="stylesheet" href="theme/css/main-responsive.css">
    <link rel="stylesheet" href="theme/plugins/iCheck/skins/all.css">
    <link rel="stylesheet" href="theme/plugins/bootstrap-colorpalette/css/bootstrap-colorpalette.css">
    <link rel="stylesheet" href="theme/plugins/perfect-scrollbar/src/perfect-scrollbar.css">
    <link rel="stylesheet" href="theme/css/theme_light.css" type="text/css" id="skin_color">
    <link rel="stylesheet" href="theme/css/print.css" type="text/css" media="print"/>
    <!--[if IE 7]>
    <link rel="stylesheet" href="theme/plugins/font-awesome/css/font-awesome-ie7.min.css">
    <![endif]-->

    <!-- start: MAIN JAVASCRIPTS -->
    <!--[if lt IE 9]>
    <script src="theme/plugins/respond.min.js"></script>
    <script src="theme/plugins/excanvas.min.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <![endif]-->
    <!--[if gte IE 9]><!-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <!--<![endif]-->

    <script type="text/javascript" src="js/jquery.util.js"></script>
</head>
<body>

<div class="navbar navbar-inverse navbar-fixed-top">
    <!-- start: TOP NAVIGATION CONTAINER -->
    <div class="container">
        <div class="navbar-header" style="height: 47px;">
            <!-- start: RESPONSIVE MENU TOGGLER -->
            <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                <span class="clip-list-2"></span>
            </button>
            <!-- end: RESPONSIVE MENU TOGGLER -->
            <a class="navbar-brand" href="javascript:void(0);" style="padding-top: 6px;">
                <img src="images/logo.png" height="35" />
            </a>
        </div>
        <div class="navbar-tools">
            <!-- start: TOP NAVIGATION MENU -->
            <ul class="nav navbar-right">

                <!-- start: USER DROPDOWN -->
                <li class="dropdown" style="font-size: 16px; padding: 14px 10px 11px; color: #666666;">
                    <?=($existeProcesso)? ('Vestibular '.$num_processo_ano.' - '.utf8_encode($str_processo_semestre)): 'VESTIBULAR';?>
                </li>
                <li class="dropdown current-user">
                    <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" data-close-others="true" href="#" style="padding: 14px 4px 11px 9px;">
                        <i class="clip-user-2"></i>
                        <span class="username"><?=utf8_encode($r_form['str_pessoa_nome'])?></span>
                        <i class="clip-chevron-down"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="index.php?cadastro">
                                <i class="clip-pencil"></i>
                                &nbsp;Dados Cadastrais
                            </a>
                        </li>
                        <li>
                            <a href="sair.php">
                                <i class="clip-exit"></i>
                                &nbsp;Sair
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- end: HEADER -->
<!-- start: MAIN CONTAINER -->
<div class="main-container">
    <div class="navbar-content">
        <!-- start: SIDEBAR -->
        <div class="main-navigation navbar-collapse collapse">
            <!-- start: MAIN MENU TOGGLER BUTTON -->
            <div class="navigation-toggler">
                <i class="clip-chevron-left"></i>
                <i class="clip-chevron-right"></i>
            </div>
            <!-- end: MAIN MENU TOGGLER BUTTON -->
            <!-- start: MAIN NAVIGATION MENU -->
            <ul class="main-navigation-menu">
                <li class="<?php echo (isset($_GET['inscricao']) ? '' : 'active open'); ?>">
                    <a href="index.php?cadastro"><i class="clip-user-2"></i>
                        <span class="title"> Dados Cadastrais </span><span class="selected"></span>
                    </a>
                </li>

                <?php
                foreach($menu['ano'] as $key => $txt_ano) {
                    (in_array('_atual', $menu[$txt_ano]))? $atual = '_atual':  $atual = null;
                    ?>
                    <li class="<?php echo (($_GET['a'] == $txt_ano) ? 'active open' : ''); ?>">
                        <a href="javascript:void(0)"><i class="clip-pencil"></i>
                            <span class="title"> Vestibular <?= $txt_ano; ?> </span><i class="icon-arrow"></i>
                            <span class="selected"></span>
                        </a>
                        <ul class="sub-menu">
                            <?php
                            foreach($menu['sub_menu'] as $key2 => $subMenu) {
                                if($subMenu['ano'] == $txt_ano) {
                                ?>

                                    <li>
                                        <a href="index.php<?= $subMenu['link']; ?>&a=<?= $txt_ano; ?>">
                                            <span class="title">
                                                <small><?= $subMenu['num_inscricao']; ?></small>
                                                <?= $subMenu['curso']; ?>
                                            </span>
                                        </a>
                                    </li>
                                <?php
                                }
                            }
                            ?>
                        </ul>
                    </li>
                <?php } ?>
            </ul>
            <!-- end: MAIN NAVIGATION MENU -->
        </div>
        <!-- end: SIDEBAR -->
    </div>

