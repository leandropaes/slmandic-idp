</div>
<!-- end: MAIN CONTAINER -->
<!-- start: FOOTER -->
<div class="footer clearfix">
    <div class="footer-inner">
        &copy; 2015 IDP / SP. Todos os direitos reservados.
    </div>
    <div class="footer-items">
        <span class="go-top"><i class="clip-chevron-up"></i></span>
    </div>
</div>
<!-- end: FOOTER -->

<!-- start: MAIN JAVASCRIPTS -->
<script src="theme/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
<script src="theme/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="theme/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
<script src="theme/plugins/blockUI/jquery.blockUI.js"></script>
<script src="theme/plugins/iCheck/jquery.icheck.min.js"></script>
<script src="theme/plugins/perfect-scrollbar/src/jquery.mousewheel.js"></script>
<script src="theme/plugins/perfect-scrollbar/src/perfect-scrollbar.js"></script>
<script src="theme/plugins/less/less-1.5.0.min.js"></script>
<script src="theme/plugins/jquery-cookie/jquery.cookie.js"></script>
<script src="theme/js/main.js"></script>
<script type="text/javascript" src="js/jquery.mask.js"></script>
<script src="theme/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="theme/plugins/jquery-validation/localization/messages_pt_BR.js"></script>
<script src="js/jquery.validate.cpf.js"></script>
<script src="js/jquery.validate.dateBR.js"></script>
<!-- end: MAIN JAVASCRIPTS -->
<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script>
    jQuery(document).ready(function() {
        Main.init();

        $('#frmDadosCadastro').validate({
            errorElement: "span", // contain the error msg in a span tag
            errorClass: 'help-block',
            errorPlacement: function (error, element) { // render error placement for each input type
                if (element.attr("type") == "radio" || element.attr("type") == "checkbox") { // for chosen elements, need to insert the error after the chosen container
                    error.insertAfter($(element).closest('.form-group').children('div').children().last());
                } else {
                    error.insertAfter(element);
                    // for other inputs, just perform default behavior
                }
            },
            ignore: ':hidden',
            rules: {
                str_pessoa_cpf: {
                    cpf: true,
                    required: true
                },
                dt_pessoa_nascimento : {
                    required: true,
                    dateBR: true
                },
                atual_str_sistema_senha: {
                    required: {
                        depends:function() {
                            if ($('#str_sistema_senha').val() != "" || $('#confir_str_sistema_senha').val() != "") {
                                return true;
                            }
                            else {
                                return false;
                            }
                        }
                    }
                },
                str_sistema_senha: {
                    minlength: 6,
                    required: {
                        depends:function() {
                            if ($('#atual_str_sistema_senha').val() != "" || $('#confir_str_sistema_senha').val() != "") {
                                return true;
                            }
                            else {
                                return false;
                            }
                        }
                    }
                },
                confir_str_sistema_senha: {
                    minlength: 6,
                    equalTo: "#str_sistema_senha",
                    required: {
                        depends:function() {
                            if ($('#atual_str_sistema_senha').val() != "" || $('#str_sistema_senha').val() != "") {
                                return true;
                            }
                            else {
                                return false;
                            }
                        }
                    }
                }
            },
            messages: {
                str_pessoa_cpf: {
                    cpf: 'CPF Inválido'
                }
            },
            highlight: function (element) {
                $(element).closest('.help-block').removeClass('valid');
                // display OK icon
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                // add the Bootstrap error class to the control group
                $(element).closest('form').find('.help-block').addClass('no-margin-bottom');
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error');
                // set error class to the control group
            },
            success: function (label, element) {
                label.addClass('help-block valid');
                label.remove();
                // mark the current input as valid and display OK icon
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
            }
        });
    });
</script>
</body>
<!-- end: BODY -->
</html>