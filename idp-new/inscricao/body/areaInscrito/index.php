<div class="main-content">

	<div class="container">

		<?php

		if(isset($_GET['inscricao'])){

			$id_inscricao = base64_decode($_GET['inscricao']);

			// recupero os dados dessa inscrição

			$query = "	SELECT

							processos.dt_inscricao_fim,
							processos.dt_vencimento_maximo,
							processos.dt_divulgacaoResultado,
							DATE_FORMAT(processos.dt_divulgacaoResultado, '%H:%i do dia %d/%m/%Y' ) as dateDivulgacao,
							now() as dataAtual,
							inscricoes.*

						FROM ".DB_PREFIXO."Inscricoes as inscricoes

							INNER JOIN ".DB_PREFIXO."Processos processos ON
							processos.id_processo = inscricoes.id_processo

						WHERE

							inscricoes.id_inscricao = {$id_inscricao}

					;";

			$resultado = mysql_query($query, $conectar);

			if(mysql_num_rows($resultado)){

				$linha = mysql_fetch_object($resultado);

				// Verifico se o ID dessa inscricao é o mesmo da pessoa logada
				if ($r_form['id_pessoafisica'] != $linha->id_pessoafisica){
					require_once("body/areaInscrito/includes/messages.php");
					?>
					<script>
						$("#msg_inscricao").html("<b>Alerta:</b> Você não tem permissão para acessar essa inscrição.");
					</script>
					<?php

				}

				// Verifico se o usuário finalizou o processo
				elseif($linha->int_etapa < 4){

					if ($linha->dataAtual > $linha->dt_inscricao_fim){
						require_once("body/areaInscrito/includes/messages.php");
						?>
						<script>
							$("#msg_inscricao").html("<b>Alerta:</b> Você ainda não finalizou essa inscrição. Período de Inscrição encerrada para esse Processo.");
						</script><?php
					}
					else{
						require_once("body/areaInscrito/includes/messages.php");
						?>
						<script>
							$("#msg_inscricao").html("<b>Alerta:</b> Você ainda não finalizou esse processo, <a href='sair.php' id_inscricao='<?=$id_inscricao?>' id='redirecionar'>clique aqui para finalizar</a>. <span id='carregando_red' style='display: none;'><b><i>Redirecionando&nbsp;&nbsp;<img src='images/carregando.gif'/></i></p></span>");
						</script><?php
					}
					?>
					<script type="text/javascript" src="do/script/validaRedirecionar.js"></script>
					<?php
				}
				else{

					require_once("body/areaInscrito/formInscricao.php");

				}


			}
			else{
				require_once("body/areaInscrito/includes/messages.php");
				?>
				<script>
					$("#msg_inscricao").html("<b>Alerta:</b> Inscrição não encontrada.");
				</script>
				<?php

			}

		}

		else {

			require_once("body/areaInscrito/formCadastro.php");
		}

		?>



		<!-- end: PAGE CONTENT-->
	</div>
</div>
