<?php
	// Busco os estados

		$r_estado = array();
		$query = "SELECT * FROM  ".DB_PREFIXO."Estados;";
		$resultado = mysql_query($query, $conectar);
		while($linha = mysql_fetch_object($resultado)){
			$r_estado[$linha->str_sigla] = $linha->str_nome;
		}
	
	// Busco Sexo
		$r_sexo = array('M'=>'Masculino', 'F'=>'Feminino');
	
	// Busco SMS
		$r_sms  = array('S'=>'SIM', 'N'=>'NÃO');
		

	// Identificação de Campo Obrigatorio
		$atual_str_sistema_senha  	= BORDA_PADRAO;
		$str_sistema_senha 		  	= BORDA_PADRAO;
		$confir_str_sistema_senha 	= BORDA_PADRAO;
		$str_telefone_residencial 	= BORDA_PADRAO;
		$str_telefone_comercial   	= BORDA_PADRAO;
		$str_telefone_celular 	  	= BORDA_PADRAO;
		$str_pessoa_nome 		  	= BORDA_PADRAO;
		$str_pessoa_rgrne 		 	= BORDA_PADRAO;
		$str_pessoa_ssp 			= BORDA_PADRAO;
		$str_pessoa_cpf 			= BORDA_PADRAO;
		$str_pessoa_email 			= BORDA_PADRAO;
		$str_pessoa_naturalde 		= BORDA_PADRAO;
		$str_endereco_cep 			= BORDA_PADRAO;
		$str_endereco_logradouro 	= BORDA_PADRAO;
		$str_endereco_numero 		= BORDA_PADRAO;
		$str_endereco_complemento	= BORDA_PADRAO;
		$str_endereco_bairro 		= BORDA_PADRAO;
		$str_endereco_cidade 		= BORDA_PADRAO;
		$str_endereco_estado 		= BORDA_PADRAO;
		$str_endereco_pais 			= BORDA_PADRAO;
		//$dt_pessoa_registro 		= BORDA_PADRAO;
		$dt_pessoa_nascimento 		= BORDA_PADRAO;
		$ch_comunicacao_sms 		= BORDA_PADRAO;
		$ch_pessoa_sexo 			= BORDA_PADRAO;

	// Mensagens
		$msg_senha = NULL;
		$msg_senhaAtual = NULL;
		$msg_email = NULL;
		$msg_data = NULL;

	$validacao = TRUE;

if (isset($_POST['salvarPessoaFisica'])){
	
	
	// Validação do formulário
	
		// valido Senha
		$r_cadastro['senha_atual'] = $_POST['senha_atual']; 	
		
		
		$r_cadastro['atual_str_sistema_senha'] 	= $_POST['atual_str_sistema_senha']; 			
		$r_cadastro['str_sistema_senha'] 		= $_POST['str_sistema_senha']; 			
		$r_cadastro['confir_str_sistema_senha'] = $_POST['confir_str_sistema_senha']; 	
	
	
		// se algum campo foi preenchido
		if (($r_cadastro['atual_str_sistema_senha'] != NULL) || ($r_cadastro['str_sistema_senha'] != NULL) || ($r_cadastro['confir_str_sistema_senha'] != NULL)){
			
			// se todos campos foram preenchidos
			if (($r_cadastro['atual_str_sistema_senha'] != NULL) && ($r_cadastro['str_sistema_senha'] != NULL) && ($r_cadastro['confir_str_sistema_senha'] != NULL)){
			
				// se o atual é valido
				if(sha1($r_cadastro['atual_str_sistema_senha']) != $r_cadastro['senha_atual']){
					$r_cadastro['atual_str_sistema_senha'] = NULL;
					$atual_str_sistema_senha = BORDA_RED;
					$msg_senhaAtual = '* Senha inválida.';
					$validacao = FALSE; 
				
				}
				
				// se as senhas coincidem
				// Verifico se a senha esta correta
				if ($r_cadastro['str_sistema_senha'] != $r_cadastro['confir_str_sistema_senha']){
				
					$r_cadastro['str_sistema_senha']        = NULL;
					$r_cadastro['confir_str_sistema_senha'] = NULL;
					
					$str_sistema_senha 		  = BORDA_RED;
					$confir_str_sistema_senha = BORDA_RED;
					
					$msg_senha = '* As senhas não coincidem.';
					
					$validacao = FALSE;
				
				}

			
			}
			else{
			
				// nova senha preenchiada
				// se todos campos foram preenchidos
				if ($r_cadastro['atual_str_sistema_senha'] != NULL){
				
					// se o atual é valido
					if(sha1($r_cadastro['atual_str_sistema_senha']) != $r_cadastro['senha_atual']){
						$r_cadastro['atual_str_sistema_senha'] = NULL;
						$atual_str_sistema_senha = BORDA_RED;
						$msg_senhaAtual = '* Senha inválida.';
						$validacao = FALSE; 
					
					}

				}
	
				
				// se as senhas coincidem
				// Verifico se a senha esta correta
				if ($r_cadastro['str_sistema_senha'] != $r_cadastro['confir_str_sistema_senha']){
				
					$r_cadastro['str_sistema_senha']        = NULL;
					$r_cadastro['confir_str_sistema_senha'] = NULL;
					
					$str_sistema_senha 		  = BORDA_RED;
					$confir_str_sistema_senha = BORDA_RED;
					
					$msg_senha = '* As senhas não coincidem.';
					
					$validacao = FALSE;
				
				}
				elseif($r_cadastro['atual_str_sistema_senha'] == NULL){
					$atual_str_sistema_senha = BORDA_RED;
					$msg_senhaAtual = '* Informe sua senha atual.';
					$validacao = FALSE; 
				
				}
			}
		
		}
	
	
	
	$r_cadastro['str_telefone_residencial'] = $_POST['str_telefone_residencial'];	if($r_cadastro['str_telefone_residencial'] 	== NULL){ $validacao = FALSE; $str_telefone_residencial = BORDA_RED;}
	$r_cadastro['str_telefone_comercial'] 	= $_POST['str_telefone_comercial']; 	//if($r_cadastro['str_telefone_comercial'] 	== NULL){ $validacao = FALSE; $str_telefone_comercial 	= BORDA_RED;}
	$r_cadastro['str_telefone_celular']		= $_POST['str_telefone_celular']; 		if($r_cadastro['str_telefone_celular'] 		== NULL){ $validacao = FALSE; $str_telefone_celular		= BORDA_RED;}
	$r_cadastro['str_pessoa_nome'] 			= $_POST['str_pessoa_nome']; 			if($r_cadastro['str_pessoa_nome'] 			== NULL){ $validacao = FALSE; $str_pessoa_nome 			= BORDA_RED;}
	$r_cadastro['str_pessoa_rgrne'] 		= $_POST['str_pessoa_rgrne']; 			if($r_cadastro['str_pessoa_rgrne'] 			== NULL){ $validacao = FALSE; $str_pessoa_rgrne 		= BORDA_RED;}
	$r_cadastro['str_pessoa_ssp'] 			= $_POST['str_pessoa_ssp']; 			if($r_cadastro['str_pessoa_ssp']			== NULL){ $validacao = FALSE; $str_pessoa_ssp 			= BORDA_RED;}
	$r_cadastro['str_pessoa_email'] 		= $_POST['str_pessoa_email']; 			if($r_cadastro['str_pessoa_email'] 			== NULL){ $validacao = FALSE; $str_pessoa_email 		= BORDA_RED;}
	$r_cadastro['str_pessoa_naturalde'] 	= $_POST['str_pessoa_naturalde']; 		if($r_cadastro['str_pessoa_naturalde']		== NULL){ $validacao = FALSE; $str_pessoa_naturalde 	= BORDA_RED;}
	$r_cadastro['str_endereco_cep'] 		= $_POST['str_endereco_cep']; 			if($r_cadastro['str_endereco_cep'] 			== NULL){ $validacao = FALSE; $str_endereco_cep 		= BORDA_RED;}
	$r_cadastro['str_endereco_logradouro'] 	= $_POST['str_endereco_logradouro']; 	if($r_cadastro['str_endereco_logradouro'] 	== NULL){ $validacao = FALSE; $str_endereco_logradouro 	= BORDA_RED;}
	$r_cadastro['str_endereco_numero'] 		= $_POST['str_endereco_numero']; 		if($r_cadastro['str_endereco_numero']		== NULL){ $validacao = FALSE; $str_endereco_numero 		= BORDA_RED;}
	$r_cadastro['str_endereco_complemento'] = $_POST['str_endereco_complemento']; 	//if($r_cadastro['str_endereco_complemento'] 	== NULL){ $validacao = FALSE; $str_endereco_complemento = BORDA_RED;}
	$r_cadastro['str_endereco_bairro'] 		= $_POST['str_endereco_bairro']; 		if($r_cadastro['str_endereco_bairro'] 		== NULL){ $validacao = FALSE; $str_endereco_bairro 		= BORDA_RED;}
	$r_cadastro['str_endereco_cidade'] 		= $_POST['str_endereco_cidade']; 		if($r_cadastro['str_endereco_cidade'] 		== NULL){ $validacao = FALSE; $str_endereco_cidade 		= BORDA_RED;}
	$r_cadastro['str_endereco_estado'] 		= $_POST['str_endereco_estado']; 		if(($r_cadastro['str_endereco_estado'] 		== '(Selecione)')||($r_cadastro['str_endereco_estado'] 		== NULL)){ $validacao = FALSE; $str_endereco_estado 		= BORDA_RED;}
	$r_cadastro['str_endereco_pais'] 		= $_POST['str_endereco_pais']; 			if($r_cadastro['str_endereco_pais'] 		== NULL){ $validacao = FALSE; $str_endereco_pais		= BORDA_RED;}
	//$r_cadastro['dt_pessoa_registro'] 		= $_POST['dt_pessoa_registro']; 		if($r_cadastro['dt_pessoa_registro'] 		== NULL){ $validacao = FALSE; $dt_pessoa_registro 		= BORDA_RED;}
	$r_cadastro['ch_comunicacao_sms'] 		= $_POST['ch_comunicacao_sms']; 		if($r_cadastro['ch_comunicacao_sms'] 		== NULL){ $validacao = FALSE; $ch_comunicacao_sms 		= BORDA_RED;}
	$r_cadastro['ch_pessoa_sexo'] 			= $_POST['ch_pessoa_sexo'];				if($r_cadastro['ch_pessoa_sexo']			== '(Selecione)'){ $validacao = FALSE; $ch_pessoa_sexo 			= BORDA_RED;}	
	$r_cadastro['dt_pessoa_nascimento'] 	= $_POST['dt_pessoa_nascimento']; 
	
	// valido o Data
	if (!validaData($r_cadastro['dt_pessoa_nascimento'])){
		
		$dt_pessoa_nascimento 	= BORDA_RED; 
		$msg_data = '* Informe a data Corretamente dd/mm/aaaa.';
		$validacao = FALSE; 
	
	}
	
	
	// valido o Email
	if(!validaEmail($r_cadastro['str_pessoa_email'] )){
	
		$str_pessoa_email = BORDA_RED;
		$msg_email = '* O e-mail deve ser válido.';
		$validacao = FALSE;
	
	}

	
	// A Validação esta ok, insiro no banco as informações
	if ($validacao){
			
		// Se eu estiver alterando não atualizo a senha
		if($r_cadastro['str_sistema_senha'] != NULL)
			$r_cadastro['str_sistema_senha'] = sha1($r_cadastro['str_sistema_senha']);		
		else
			$r_cadastro['str_sistema_senha'] = $r_cadastro['senha_atual'];		
		
		//formato a data de nascimento para aaaa-mm-dd
		$r_cadastro['dt_pessoa_nascimento'] = formatarData($r_cadastro['dt_pessoa_nascimento'], 'DB');
			
	
		//Salvo as Informações no Banco
		
		$query = "	UPDATE 
		
						".DB_PREFIXO."PessoasFisicas
						
					SET
						 str_sistema_senha 			= '".$r_cadastro['str_sistema_senha']."'
						,str_telefone_residencial 	= '".$r_cadastro['str_telefone_residencial']."'
						,str_telefone_comercial 	= '".$r_cadastro['str_telefone_comercial']."'
						,str_telefone_celular 		= '".$r_cadastro['str_telefone_celular']."'
						,str_pessoa_nome 			= '".utf8_decode($r_cadastro['str_pessoa_nome'])."'
						,str_pessoa_rgrne 			= '".$r_cadastro['str_pessoa_rgrne']."'
						,str_pessoa_ssp 			= '".$r_cadastro['str_pessoa_ssp']."'
						,str_pessoa_email 			= '".$r_cadastro['str_pessoa_email']."'
						,str_pessoa_naturalde 		= '".utf8_decode($r_cadastro['str_pessoa_naturalde'])."'
						,str_endereco_cep 			= '".$r_cadastro['str_endereco_cep']."'
						,str_endereco_logradouro 	= '".utf8_decode($r_cadastro['str_endereco_logradouro'])."'
						,str_endereco_numero 		= '".$r_cadastro['str_endereco_numero']."'
						,str_endereco_complemento 	= '".utf8_decode($r_cadastro['str_endereco_complemento'])."'
						,str_endereco_bairro 		= '".utf8_decode($r_cadastro['str_endereco_bairro'])."'
						,str_endereco_cidade 		= '".utf8_decode($r_cadastro['str_endereco_cidade'])."'
						,str_endereco_estado 		= '".$r_cadastro['str_endereco_estado']."'
						,str_endereco_pais 			= '".utf8_decode($r_cadastro['str_endereco_pais'])."'
						,dt_pessoa_nascimento 		= '".$r_cadastro['dt_pessoa_nascimento']."'
						,ch_comunicacao_sms 		= '".$r_cadastro['ch_comunicacao_sms']."'
						,ch_pessoa_sexo 			= '".$r_cadastro['ch_pessoa_sexo']."'
						
					WHERE
					
						id_pessoafisica = '".$r_form['id_pessoafisica']."'
				";
				
				//echo "<pre>{$query}</pre>"; exit();
		
					
		$resultado = mysql_query($query, $conectar); 
		
		if ($resultado){
		
			$formValido = TRUE;
			$r_cadastro['senha_atual'] 	= $r_cadastro['str_sistema_senha'] 	;
			$r_cadastro['atual_str_sistema_senha']  = NULL;
			$r_cadastro['str_sistema_senha'] 		= NULL;
			$r_cadastro['confir_str_sistema_senha'] = NULL;
			
			$r_cadastro['dt_pessoa_nascimento'] = formatarData($r_cadastro['dt_pessoa_nascimento'], '');

		}
		else{
		
			$formValido = FALSE;
		
		}
	}
}
else{

	

	// Verifico se nessa etapa esta com permissao de UPDATE
	// Se tiver recupero as informacoes

	
	
		$query = "SELECT * FROM ".DB_PREFIXO."PessoasFisicas WHERE id_pessoafisica = '".$r_form["id_pessoafisica"]."' ;";
		
		$resultado = mysql_query($query, $conectar);
		
		if ($resultado){
		
			$linha = mysql_fetch_object($resultado);
			
			$r_cadastro['senha_atual'] 		        = $linha->str_sistema_senha ;
			$r_cadastro['atual_str_sistema_senha']  = NULL ;
			$r_cadastro['str_sistema_senha'] 		= NULL ;
			$r_cadastro['confir_str_sistema_senha'] = NULL ;
			$r_cadastro['str_telefone_residencial'] = $linha->str_telefone_residencial ;
			$r_cadastro['str_telefone_comercial'] 	= $linha->str_telefone_comercial ;
			$r_cadastro['str_telefone_celular'] 	= $linha->str_telefone_celular ;
			$r_cadastro['str_pessoa_nome'] 			= utf8_encode($linha->str_pessoa_nome) ;
			$r_cadastro['str_pessoa_rgrne'] 		= $linha->str_pessoa_rgrne ;
			$r_cadastro['str_pessoa_ssp'] 			= $linha->str_pessoa_ssp ;
			$r_cadastro['str_pessoa_email'] 		= $linha->str_pessoa_email ;
			$r_cadastro['str_pessoa_naturalde'] 	= utf8_encode($linha->str_pessoa_naturalde) ;
			$r_cadastro['str_endereco_cep'] 		= $linha->str_endereco_cep ;
			$r_cadastro['str_endereco_logradouro'] 	= utf8_encode($linha->str_endereco_logradouro) ;
			$r_cadastro['str_endereco_numero'] 		= $linha->str_endereco_numero ;
			$r_cadastro['str_endereco_complemento'] = utf8_encode($linha->str_endereco_complemento) ;
			$r_cadastro['str_endereco_bairro'] 		= utf8_encode($linha->str_endereco_bairro) ;
			$r_cadastro['str_endereco_cidade'] 		= utf8_encode($linha->str_endereco_cidade) ;
			$r_cadastro['str_endereco_estado'] 		= $linha->str_endereco_estado ;
			$r_cadastro['str_endereco_pais'] 		= utf8_encode($linha->str_endereco_pais) ;
			$r_cadastro['dt_pessoa_registro'] 		= $linha->dt_pessoa_registro ;
			$r_cadastro['dt_pessoa_nascimento'] 	= formatarData($linha->dt_pessoa_nascimento, '');
			$r_cadastro['ch_comunicacao_sms']		= $linha->ch_comunicacao_sms ;
			$r_cadastro['ch_pessoa_sexo'] 			= $linha->ch_pessoa_sexo ;
		}
	
	else{
		
		// Informações do formulário
		$r_cadastro['senha_atual']              = NULL;
		$r_cadastro['atual_str_sistema_senha']  = NULL;
		$r_cadastro['str_sistema_senha'] 		= NULL;
		$r_cadastro['confir_str_sistema_senha'] = NULL;
		$r_cadastro['str_telefone_residencial'] = NULL;
		$r_cadastro['str_telefone_comercial'] 	= NULL;
		$r_cadastro['str_telefone_celular'] 	= NULL;
		$r_cadastro['str_pessoa_nome'] 			= NULL;
		$r_cadastro['str_pessoa_rgrne'] 		= NULL;
		$r_cadastro['str_pessoa_ssp'] 			= NULL;
		$r_cadastro['str_pessoa_email'] 		= NULL;
		$r_cadastro['str_pessoa_naturalde'] 	= NULL;
		$r_cadastro['str_endereco_cep'] 		= NULL;
		$r_cadastro['str_endereco_logradouro'] 	= NULL;
		$r_cadastro['str_endereco_numero'] 		= NULL;
		$r_cadastro['str_endereco_complemento'] = NULL;
		$r_cadastro['str_endereco_bairro'] 		= NULL;
		$r_cadastro['str_endereco_cidade'] 		= NULL;
		$r_cadastro['str_endereco_estado'] 		= NULL;
		$r_cadastro['str_endereco_pais'] 		= 'Brasil';
		//$r_cadastro['dt_pessoa_registro'] 		= NULL;
		$r_cadastro['dt_pessoa_nascimento'] 	= NULL;
		$r_cadastro['ch_comunicacao_sms']		= 'N';
		$r_cadastro['ch_pessoa_sexo'] 			= NULL;
	
	}


}

?>
<!-- start: PAGE HEADER -->
<div class="row">
	<div class="col-sm-12">

		<!-- start: PAGE TITLE & BREADCRUMB -->
		<ol class="breadcrumb">
			<li>
				<i class="clip-file"></i>
				<a href="#">
					Home
				</a>
			</li>
			<li class="active">
				Dados Cadastrais
			</li>
		</ol>
		<div class="page-header">
			<h1>Dados Cadastrais</h1>
		</div>
		<!-- end: PAGE TITLE & BREADCRUMB -->
	</div>
</div>
<!-- end: PAGE HEADER -->
<!-- start: PAGE CONTENT -->

<div class="row">
	<div class="col-md-12">


		<!-- ALERTAS -->
		<div id="msg_inscricao"></div>
		<div id="msg_cadastroSUCESSO" class="alert alert-success <?php echo (isset($formValido) && ($formValido)? '' : 'hidden' ) ?>"><b>Alerta:</b> Cadastro atualizado com sucesso !</div>
		<div id="msg_cadastroERRO" class="alert alert-danger hidden"><b>Alerta:</b> Erro ao atualizar cadastro, tente mais tarde.</div>


		<form id="frmDadosCadastro" action="index.php?cadastro" class="form-horizontal" method="POST">


			<input type="hidden" name="salvarPessoaFisica" value="salvarPessoaFisica">

			<div class="panel-heading text-center" style="padding-left: 0 !important;">
				Para atualizar sua senha, preencha o formulário abaixo
				<input type="hidden" name="senha_atual" id="senha_atual" value="<?=$r_cadastro['senha_atual']?>" />
			</div>
			<br>

			<div class="form-group">
				<label for="atual_str_sistema_senha" class="col-md-4 control-label">
					Senha de Atual
				</label>
				<div class="col-md-4">
					<input class="form-control" type="password" name="atual_str_sistema_senha" id="atual_str_sistema_senha" value="<?=$r_cadastro['atual_str_sistema_senha']?>" <?=$atual_str_sistema_senha?> /> <?php if($msg_senhaAtual) echo ' <span class="campo_erro">',$msg_senhaAtual,'</span>';?>		</div>
			</div>
			<div class="form-group">
				<label for="str_sistema_senha" class="col-md-4 control-label">
					Nova Senha
				</label>
				<div class="col-md-4">
					<input class="form-control" type="password" name="str_sistema_senha" id="str_sistema_senha" value="<?=$r_cadastro['str_sistema_senha']?>" <?=$str_sistema_senha?> /> <?php if($msg_senha) echo ' <span class="campo_erro">',$msg_senha,'</span>';?>
				</div>
			</div>
			<div class="form-group">
				<label for="str_sistema_senha" class="col-md-4 control-label">
					Confirmação da Senha
				</label>
				<div class="col-md-4">
					<input class="form-control" type="password" name="confir_str_sistema_senha" id="confir_str_sistema_senha" value="<?=$r_cadastro['confir_str_sistema_senha']?>" <?=$confir_str_sistema_senha?> />
				</div>
			</div>

			<!-- DADOS PESSOAIS -->
			<div class="panel-heading text-center" style="padding-left: 0 !important;">
				Dados Pessoais
			</div>
			<br>

			<div class="form-group">
				<label for="str_pessoa_cpf" class="col-md-4 control-label">
					CPF
				</label>
				<div class="col-md-4">
					<input class="form-control" type="text" name="str_pessoa_cpf" id="str_pessoa_cpf" value="<?=$r_form['str_pessoa_cpf']?>"  <?=$str_pessoa_cpf?> disabled required />
				</div>
			</div>
			<div class="form-group">
				<label for="str_pessoa_nome" class="col-md-4 control-label">
					Nome
				</label>
				<div class="col-md-4">
					<input class="form-control" required type="text" name="str_pessoa_nome" id="str_pessoa_nome" value="<?=$r_cadastro['str_pessoa_nome']?>" <?=$str_pessoa_nome?>/>
				</div>
			</div>
			<div class="form-group">
				<label for="str_pessoa_rgrne" class="col-md-4 control-label">
					RG
				</label>
				<div class="col-md-4">
					<input class="form-control" type="text" name="str_pessoa_rgrne" id="str_pessoa_rgrne" value="<?=$r_cadastro['str_pessoa_rgrne']?>" <?=$str_pessoa_rgrne?> required/>
				</div>
			</div>
			<div class="form-group">
				<label for="str_pessoa_ssp" class="col-md-4 control-label">
					Órgão Expedidor
				</label>
				<div class="col-md-4">
					<input class="form-control" required type="text" name="str_pessoa_ssp" id="str_pessoa_ssp" value="<?=$r_cadastro['str_pessoa_ssp']?>" <?=$str_pessoa_ssp?>/>
				</div>
			</div>
			<div class="form-group">
				<label for="dt_pessoa_nascimento" class="col-md-4 control-label">
					Data de Nascimento
				</label>
				<div class="col-md-4">
					<input class="form-control" required data-mask="00/00/0000" type="text" name="dt_pessoa_nascimento" id="dt_pessoa_nascimento" value="<?=$r_cadastro['dt_pessoa_nascimento']?>" <?=$dt_pessoa_nascimento?>/><?php if($msg_data) echo ' <span class="campo_erro">',$msg_data,'</span>';?>
				</div>
			</div>
			<div class="form-group">
				<label for="ch_pessoa_sexo" class="col-md-4 control-label">
					Sexo
				</label>
				<div class="col-md-4">
					<select class="form-control" required name="ch_pessoa_sexo" id="ch_pessoa_sexo" <?=$ch_pessoa_sexo?>>
						<option>(Selecione)</option>
						<?php
						foreach($r_sexo as $sigla => $sexo){
							($r_cadastro['ch_pessoa_sexo'] == $sigla)? $selected = 'selected' : $selected = NULL;
							echo "<option value='{$sigla}' {$selected}>{$sexo}</option>";
						}
						?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label for="str_pessoa_naturalde" class="col-md-4 control-label">
					Naturalidade
				</label>
				<div class="col-md-4">
					<input class="form-control" required type="text" name="str_pessoa_naturalde" id="str_pessoa_naturalde" value="<?=$r_cadastro['str_pessoa_naturalde']?>" <?=$str_pessoa_naturalde?>/>
				</div>
			</div>


			<!-- ENDERECO -->
			<div class="panel-heading text-center" style="padding-left: 0 !important;">
				Endereço Residencial
			</div>
			<br>

			<div class="form-group">
				<label for="str_endereco_cep" class="col-md-4 control-label">
					CEP
				</label>
				<div class="col-md-4">
					<input class="form-control" required type="text" data-mask="00.000-000" name="str_endereco_cep" id="str_endereco_cep" value="<?=$r_cadastro['str_endereco_cep']?>" <?=$str_endereco_cep?>/>
				</div>
			</div>
			<div class="form-group">
				<label for="str_endereco_logradouro" class="col-md-4 control-label">
					Logradouro
				</label>
				<div class="col-md-4">
					<input class="form-control" required type="text" name="str_endereco_logradouro" id="str_endereco_logradouro" value="<?=$r_cadastro['str_endereco_logradouro']?>" <?=$str_endereco_logradouro?>/>
				</div>
			</div>
			<div class="form-group">
				<label for="str_endereco_logradouro" class="col-md-4 control-label">
					Número
				</label>
				<div class="col-md-4">
					<input class="form-control" required type="text" name="str_endereco_numero" id="str_endereco_numero" value="<?=$r_cadastro['str_endereco_numero']?>" <?=$str_endereco_numero?>/>
				</div>
			</div>
			<div class="form-group">
				<label for="str_endereco_bairro" class="col-md-4 control-label">
					Bairro
				</label>
				<div class="col-md-4">
					<input class="form-control" required type="text" name="str_endereco_bairro" id="str_endereco_bairro" value="<?=$r_cadastro['str_endereco_bairro']?>" <?=$str_endereco_bairro?>/>
				</div>
			</div>
			<div class="form-group">
				<label for="str_endereco_cidade" class="col-md-4 control-label">
					Cidade
				</label>
				<div class="col-md-4">
					<input class="form-control" required type="text" name="str_endereco_cidade" id="str_endereco_cidade" value="<?=$r_cadastro['str_endereco_cidade']?>" <?=$str_endereco_cidade?>/>
				</div>
			</div>
			<div class="form-group">
				<label for="str_endereco_estado" class="col-md-4 control-label">
					Estado
				</label>
				<div class="col-md-4">
					<select class="form-control" required name="str_endereco_estado" id="str_endereco_estado" <?=$str_endereco_estado?>>
						<option>(Selecione)</option>
						<?php
						foreach($r_estado as $sigla => $estado){
							($r_cadastro['str_endereco_estado'] == $sigla)? $selected = 'selected' : $selected = NULL;
							echo "<option value='{$sigla}' {$selected}>{$sigla}</option>";
						}
						?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label for="str_endereco_pais" class="col-md-4 control-label">
					Complemento
				</label>
				<div class="col-md-4">
					<input class="form-control" type="text" name="str_endereco_complemento" id="str_endereco_complemento" value="<?=$r_cadastro['str_endereco_complemento']?>" <?=$str_endereco_complemento?>/>
				</div>
			</div>
			<div class="form-group">
				<label for="str_endereco_pais" class="col-md-4 control-label">
					País
				</label>
				<div class="col-md-4">
					<input class="form-control" required type="text" name="str_endereco_pais" id="str_endereco_pais" value="<?=$r_cadastro['str_endereco_pais']?>" <?=$str_endereco_pais?>/>
				</div>
			</div>

			<!-- DADOS PARA CONTATO -->
			<div class="panel-heading text-center" style="padding-left: 0 !important;">
				Dados para Contato
			</div>
			<br>

			<div class="form-group">
				<label for="str_telefone_residencial" class="col-md-4 control-label">
					Telefone Residencial
				</label>
				<div class="col-md-4">
					<input class="form-control" required data-mask="(00) 0000-0000" type="text" name="str_telefone_residencial" id="str_telefone_residencial" value="<?=$r_cadastro['str_telefone_residencial']?>" <?=$str_telefone_residencial?>/>
				</div>
			</div>
			<div class="form-group">
				<label for="str_telefone_comercial" class="col-md-4 control-label">
					Telefone Comercial
				</label>
				<div class="col-md-4">
					<input class="form-control" data-mask="(00) 0000-0000" type="text" name="str_telefone_comercial" id="str_telefone_comercial" value="<?=$r_cadastro['str_telefone_comercial']?>" <?=$str_telefone_comercial?>/>
				</div>
			</div>
			<div class="form-group">
				<label for="str_telefone_celular" class="col-md-4 control-label">
					Celular
				</label>
				<div class="col-md-4">
					<input class="form-control" required maxlength="15" onkeyup="mascara( this, mtel );" type="text" name="str_telefone_celular" id="str_telefone_celular" value="<?=$r_cadastro['str_telefone_celular']?>" <?=$str_telefone_celular?>/>
				</div>
			</div>
			<div class="form-group">
				<label for="str_pessoa_email" class="col-md-4 control-label">
					E-mail
				</label>
				<div class="col-md-4">
					<input class="form-control" required type="email" name="str_pessoa_email" id="str_pessoa_email" value="<?=$r_cadastro['str_pessoa_email']?>" <?=$str_pessoa_email?>/><?php if($msg_email) echo '  <span class="campo_erro">',$msg_email,'</span>';?>
				</div>
			</div>
			<div class="form-group">
				<label for="str_pessoa_email" class="col-md-4 control-label">
					Aceita envio de informações do vestibular por SMS?
				</label>
				<div class="col-md-4">
					<select class="form-control" required name="ch_comunicacao_sms" id="ch_comunicacao_sms" <?=$ch_comunicacao_sms?>>
						<?php
						foreach($r_sms as $silga => $sms){
							($r_cadastro['ch_comunicacao_sms'] == $silga)? $selected = 'selected' : $selected = NULL;
							echo "<option value='{$silga}' {$selected}>{$sms}</option>";
						}
						?>
					</select>
				</div>
			</div>

			<hr>

			<div class="form-group text-center">
				<button class="btn btn-primary">Salvar Cadastro</button>
			</div>
		</form>

	</div>
</div>