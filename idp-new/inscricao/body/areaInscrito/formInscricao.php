<?php

	// incluo o update do boleto caso precise
	require_once('body/areaInscrito/updateBoleto.php');
		
	// Recupero os informações da transfInscricoes
	$num_inscricao 		= $linha->num_inscricao;
	$dt_inscricao  		= formatarDataHoraDB($linha->dt_inscricao);
	$id_processo   		= $linha->id_processo;
	$id_curso      		= $linha->id_curso;
	$dt_processo_fim 	= $linha->dt_inscricao_fim;
	$dt_vencimento	 	= $linha->dt_vencimento_maximo;

	// Recupero as informações do Curso
	$query     = "SELECT * FROM ".DB_PREFIXO."Cursos WHERE id_curso = '".$id_curso ."';";
	$resultado = mysql_query($query, $conectar);
	$cursos    = mysql_fetch_object($resultado);
	
	$str_curso = $cursos->str_nome.' - Taxa '.cifrao($cursos->int_valor);
	
	// Recupero as informações do Boleto
	
	$query     = "SELECT *, now() as dataAtual FROM ".DB_PREFIXO."Boletos WHERE id_processo = '".$id_processo."' and id_inscricao = '".$num_inscricao."';";
	$resultado = mysql_query($query, $conectar);
	$boletos   = mysql_fetch_object($resultado);
	
		// ---------------------------------------------------------------------------
		// ANALISES DO BOLETO
		// ---------------------------------------------------------------------------

		$str_boletoPx    = NULL; /*Altura da linha do boleto*/ 
		$str_boletoAlign = NULL; /*Altura da linha do boleto*/ 
		
		// Texto no final da caixa para informar que a inscrição desse respectivo processo encerrou
		if($boletos->dataAtual > $dt_processo_fim)
			$txt_inscricao_encerrada = "<span style='font-size: 12px;'><i><br/>Período de Inscrição encerrada para esse Processo.</i></span>";
		else
			$txt_inscricao_encerrada = NULL;
			
		// -- --------------------------------------
		// -- PAGO
		// -- --------------------------------------
		
		// Verifico se o boleto já foi pago
		$query     = "SELECT dt_pagamento FROM ".DB_PREFIXO."BoletosPagamentos WHERE id_boleto = '".$boletos->id_boleto."';";
		$resultado = mysql_query($query, $conectar);
	
		if (mysql_num_rows($resultado)){
		
			$pagamentos = mysql_fetch_object($resultado);
			
			$temp = explode(' ', $pagamentos->dt_pagamento);

			$str_boleto = 'Pago em '.formatarData($temp[0], '');
					
		}
		
		// -- --------------------------------------
		// -- NOVA VIA
		// -- --------------------------------------
		
		// Se não foi pago, verifico se vou somente imprimir ou gerar um
		// novo boleto
		elseif ($boletos->dataAtual > $boletos->dt_vencimento){
			
			// Se não foi pago, verifico se o processo já encerrou, caso tenha encerrado então
			// não exibo o botão para gerar um novo boleto
			if($boletos->dataAtual > $dt_vencimento){
		
				$str_boleto = "Boleto NÃO pago.";
			}
			else{

				// Se as inscrições não encerraram então monto o Link para gerar novo boleto
				$num_via = $boletos->num_via;
				
				$str_boletoAlign = 'center';
				$str_boletoPx    = '40px';
	
				$str_boleto  = '<form  action="index.php?inscricao='.base64_encode($id_inscricao).'" method="POST">';
				$str_boleto .= '<INPUT TYPE="hidden" NAME="id_boleto" VALUE="'.$boletos->id_boleto.'" />';
				$str_boleto .= '<INPUT TYPE="submit" class="btn btn-sm btn-primary" value="Gerar '.($num_via+1).'&#170; via do Boleto" />';
				$str_boleto .= '</form>';
			
			}
	
		}
		
		// -- --------------------------------------
		// -- IMPRIMIR 
		// -- --------------------------------------
		
		// Se ainda não venceu e Não foi pago Gero o link do Boleto e mostro a data de vencimento
		else{
			
			$temp = explode(' ',$boletos->dt_gerado_em);
			
			$boleto_gerado_em = $temp[0];
			$boleto_gerado_as = $temp[1];
			
			$temp = explode(' ',$boletos->dt_vencimento);
			$vencimento = formatarData($temp[0], '');
			
			// ESTRUTURO OS BLOCOS DO BOLETO
			$bloco_1 = substr(md5($boletos->dt_vencimento), 6, 17);
			$bloco_2 = substr(sha1($boletos->id_inscricao), 9, 18);
			$bloco_3 = substr(md5($boletos->val_valor), 13, 6);
			$bloco_4 = substr(sha1($boleto_gerado_em), 27, 2);
			$bloco_5 = substr(md5($boleto_gerado_as), 9, 7);

			// ESTRUTURO O CODIGO DO BOLETO
			$boleto_codigo = $bloco_2 . $bloco_4 . $bloco_1 . $bloco_5 . $bloco_3;
			
			// GERO O LINK DO BOLETO
		
			$boleto_link = URL.'/boleto/vestibular-boleto.php?inscricao=';
			
			$boleto_link .= implode(',', array(substr($bloco_2, 3, 7), $id_processo, $id_inscricao, substr($bloco_3 . $bloco_5, 3, 7), $boletos->id_boleto));
			$boleto_link = '<a href="' . $boleto_link . '" target="_blank">Imprimir o Boleto</a>';
			
			$boleto_link .= '<br/> Vencimento: '.$vencimento;
		
			$str_boleto = $boleto_link;
			$str_boletoPx = '50px';
		}
		
	// Pego o titulo do processo em questão
	$query     = "SELECT num_processo_ano, str_processo_nome, str_arquivo_manual FROM ".DB_PREFIXO."Processos WHERE id_processo = '".$boletos->id_processo."';";
	$resultado = mysql_query($query, $conectar);
	$processos = mysql_fetch_object($resultado);
	
	

?>

<!-- start: PAGE HEADER -->
<div class="row">
	<div class="col-sm-12">

		<!-- start: PAGE TITLE & BREADCRUMB -->
		<ol class="breadcrumb">
			<li>
				<i class="clip-file"></i>
				<a href="#">
					Home
				</a>
			</li>
			<li class="active">
				Vestibular <?= $processos->num_processo_ano; ?>
			</li>
			<li class="active">
				<?= $num_inscricao; ?> <?= $cursos->str_nome; ?>
			</li>
		</ol>
		<div class="page-header">
			<h1><?=utf8_encode($processos->str_processo_nome)?></h1>
		</div>
		<!-- end: PAGE TITLE & BREADCRUMB -->
	</div>
</div>
<!-- end: PAGE HEADER -->
<!-- start: PAGE CONTENT -->

<div class="row">
	<div class="col-md-12">

		<div class="table-responsive">

			<table class="table table-hover table-bordered">
			<tr>
				<td class="descricao" width="200">Nº da Inscrição:</td>
				<td class="informacao"><?=$num_inscricao?></td>
			</tr>
			<tr>
				<td class="descricao">Curso:</td>
				<td class="informacao"><?=$str_curso?></td>
			</tr>
			<tr>
				<td class="descricao">Inscrito em:</td>
				<td class="informacao"><?=$dt_inscricao?></td>
			</tr>
			<tr>
				<td class="descricao" height="<?=$str_boletoPx?>">Pagamento:</td>
				<td class="informacao text-left"><?=$str_boleto?></td>
			</tr>
			<tr>
				<td class="descricao">ENEM:</td>
				<td class="informacao">
					<?php
						if($linha->ch_enem == 'N')
							echo "Optou por não utilizar nota.";
						else
							echo $linha->str_enem_ano.' - '.$linha->str_enem_inscricao;
					?>
				</td>
			</tr>

			<tr>
				<td class="descricao">Carteira para Canhoto?</td>
				<td class="informacao"><?=($linha->ch_necessidade_canhoto == 'S')?'Sim':'Não';?></td>
			</tr>

			<tr>
				<td class="descricao" title="Portador de Necessidades Especiais">PNE?</td>
				<td class="informacao"><?=($linha->ch_necessidade_especial == 'S')?'Sim - '.$linha->str_necessidade_especial:'Não';?></td>
			</tr>

			<tr>
				<td class="descricao">Edital:</td>
				<td class="informacao"><a href="<?='../arquivos/'.$processos->str_arquivo_manual?>" target="_blank">Download</a></td>
			</tr>

			<!--
				--	INFORMAÇÕES DO RESULTADO
				-- ------------------------------------------------------------------------------>
			<?php
			// SE O DATA DO RESULTADO AINDA NÃO CHEGOU INFORMO PARA O CANDIDATO
			// A DATA E HORA QUE SERÃO DIVULGADOS OS RESULTADOS


			if($linha->dt_divulgacaoResultado != '0000-00-00 00:00:00'){

				if($linha->dataAtual < $linha->dt_divulgacaoResultado)

					echo "<tr><td class='resultado' colspan='2'>Os Resultados serão divulgados às $linha->dateDivulgacao</td></tr>";

				// EXIBO AS INFORMAÇÕES DO RESULTADO
				else{

					// -- -------------------------------------------------------
					// -- Lucas Cunha : 24/07/15 a pedido do Gustavo Daltoé
					// -- A condição abaixo para o processo ser maior ou igual ao
					// -- 3, é pelo fato de somente aparacer se a pessoa foi
					// -- classificada ou não, nos processos anteriores aparaceria
					// -- informações da (Nota / Classificação / Período)
					// -- -------------------------------------------------------

					if($linha->id_processo >= 3){


						switch($linha->int_aprovado){

							case 1: $classificacao = "<strong class='text-success'>APROVADO</strong>"; break;

							case 2: $classificacao = "<strong class='text-warning'>LISTA DE ESPERA</strong>"; break;

							default : $classificacao = "<strong class='text-danger'>DESCLASSIFICADO</strong>";

						}

						?>
						<tr>
							<th class="titulo" colspan="2" >RESULTADO</th>
						</tr>
						<tr>
							<td colspan='2' class="informacao" align="center" ><?=$classificacao?></td>
						</tr>
						<?php


					}
					else{


						$classificacao = ($linha->num_classificacao)? $linha->num_classificacao.'°' : "<strong class='text-danger'>DESCLASSIFICADO</strong>";


						?>
						<tr>
							<th class="titulo" colspan="2" >RESULTADO</th>
						</tr>
						<tr>
							<td class="descricao">Classificação:</td>
							<td class="informacao"><?=$classificacao?></td>
						</tr>


						<tr>
							<td class="descricao">Nota:</td>
							<td class="informacao"><?=$linha->num_nota_nf?></td>
						</tr>

						<?php if($linha->num_classificacao){?>
						<tr>
							<td class="descricao">Período:</td>
							<td class="informacao"><?=utf8_encode($linha->str_periodoClassificado)?></td>
						</tr>
						<?php
						}

					}
				}

			}
			?>
		</table>

		</div>
	</div>
</div>
