<div id="msg_recuperarSUCESSO" class="alert alert-success hidden">
	<b>Atenção:</b> O link para a recuperação de sua senha, foi enviado ao seu e-mail.
</div>
<div id="msg_cadastroSUCESSO" class="alert alert-success hidden">
	<b>Atenção:</b> Senha alterada com SUCESSO! clique em <a href="index.php">voltar</a> e acesse novamente.
</div>
<div id="msg_cadastroERRO" class="alert alert-danger hidden">
	<b>Alerta:</b> Ocorreu um erro ao alterar a Senha, tente mais tarde. Obrigado!
</div>

<div class="row">

	<div class="col-md-6 col-md-offset-3">


				<?php

					if (isset($_GET['codigo_recuperar'])){

						if(!$_GET['codigo_recuperar']){
							header('location: index.php');
							exit;
						}

						// verifico se existe a Hash
						$query = "SELECT id_pessoafisica FROM  ".DB_PREFIXO."PessoasFisicas WHERE str_sistema_hash = '".$_GET['codigo_recuperar']."' ;";
						$resultado = mysql_query($query, $conectar);

						if(mysql_num_rows($resultado)){

							$linha = mysql_fetch_object($resultado);
							$id_pessoafisica = $linha->id_pessoafisica;

							?>


							<div class="panel panel-default">

								<div class="panel-heading" style="padding-left: 20px;">
									Recuperar Senha
								</div>
								<div class="panel-body">

									<form id="formRecuperar" method="POST">

										<INPUT TYPE="hidden"id="id_pessoafisica" NAME="id_pessoafisica" VALUE="<?=$id_pessoafisica?>">

										<div class="form-group">
											<label for="str_senha">Nova Senha</label>
											<input type="password"  id="str_senha" name="str_senha" class="form-control">
										</div>

										<div class="form-group">
											<label for="str_senha_c">Confirme sua Senha</label>
											<input type="password" id="str_senha_c" name="str_senha_c" class="form-control">
										</div>

										<div class="form-group text-center">
											<button type="submit" class="btn btn-block btn-primary">Salvar Nova Senha</button>
										</div>

										<div class="text-center">
											<div id="msg-inscrito" class="alert alert-danger hidden"></div>
											<div id="carregando_login"><img src="images/carregando.gif" /></div>
										</div>

									</form>
								</div>
							</div>

							<p class="text-center">
								<a href="index.php">Voltar para tela Inicial</a>
							</p>

						<?php

						}
						else{
							header('location: index.php');
							exit;
						}

					}
					else{


						?>


						<div class="panel panel-default">

							<div class="panel-heading" style="padding-left: 20px;">
								Recuperar Senha
							</div>
							<div class="panel-body">
								<form id="formRecuperarSenha" method="POST">

									<div class="form-group">
										<label for="str_cpf">Informe seu CPF</label>
										<input type="text" data-mask="000.000.000-00" id="str_cpf" name="str_cpf" class="form-control">
									</div>

									<div class="form-group text-center">
										<button type="submit" class="btn btn-block btn-primary">Recuperar</button>
									</div>

									<div class="text-center">
										<div id="msg-inscrito" class="alert alert-danger hidden"></div>
										<div id="carregando_login"><img src="images/carregando.gif" /></div>
									</div>

								</form>

							</div>
						</div>

						<p class="text-center">
							<a href="index.php">Voltar para tela Inicial</a>
						</p>
						<?php


					}

				?>


	</div>
</div>

<hr>