<!DOCTYPE HTML>
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="estilo.css">
	<title>IDPSP - Relatórios</title>
</head>
<body>
	<?php
	require_once('../inscricao/__lib__.php');
	
	if (!isset($_SESSION['sessao_logada_ret'])) {
		header('location: login.php');
		exit();
	}
	
	// RECUPERO OS PROCESSOS SELETIVOS
	$query = mysql_query("SELECT id_processo, str_processo_nome FROM {$DB_PREFIXO}Processos ORDER BY id_processo DESC;");
			
	while($row = mysql_fetch_assoc($query)) $r_processos[$row['id_processo']] = utf8_encode($row['str_processo_nome']);
			
	?>

	<form action="?" method="post">
		Selecione o processo:
		<select name="id_processo">
			<?php 
			foreach($r_processos as $key => $value) echo "<option value='{$key}'>{$value}</option>";
			?>
		</select>
		<input type="submit" value="Selecionar" /> 
		<a href="?">Limpar Filtro</a> | <a href="logout.php">SAIR</a>
	</form>
	
	<?php
	if(isset($_POST['id_processo'])){
	
		$id_processo = $_POST['id_processo'];
		
		// -------------------------------------------------------------
		// ARRAYS DOS FILTROS
		// -------------------------------------------------------------
			// Ordenação
			$r_ordem_nome = array(
				 'ins.id_inscricao' 	 => 'Inscrição'
				,'pf.str_pessoa_nome' 	 => 'Nome'
				,'ins.num_classificacao' => 'Classificação'
			);
			$r_ordem_tipo = array(
				 'ASC'  => 'Ascendente'
				,'DESC' => 'Descendente'
			);
			
			// Cursos
			$query = mysql_query("SELECT id_curso, str_nome FROM {$DB_PREFIXO}Cursos WHERE ch_situacao = 'A' AND id_processo = {$id_processo} ORDER BY str_nome ASC;");
			
			$r_cursos[''] = 'Todos';
			while($row = mysql_fetch_assoc($query)) $r_cursos[$row['id_curso']] = utf8_encode($row['str_nome']);
			
			// Pagamento
			$r_pagamentos = array(
				 ''  => 'Todos'
				,'1' => 'Pagos'
				,'2' => 'Não Pagos'
			);
			
			// Classificação
			$r_classificacao = array(
				 ''  => 'Todas'
				,'1' => 'Classificados'
				,'2' => 'Desclassificados'
			);
			
			// Outros
			$r_outros = array(
				 'ins.ch_necessidade_especial' 	=> 'Portador de Necessidades Especiais'
				,'ins.ch_necessidade_canhoto' 	=> 'Nessita de mesa para Canhoto'
				,'ins.ch_enem' 					=> 'Optou pelo ENEM'
			);
			
		// -------------------------------------------------------------
		// VALORES
		// -------------------------------------------------------------
		
		$ordem_nome 	= (isset($_POST['ordem_nome'])) 	? $_POST['ordem_nome'] 		: NULL;
		$ordem_tipo 	= (isset($_POST['ordem_tipo'])) 	? $_POST['ordem_tipo'] 		: NULL;
		$curso 			= (isset($_POST['curso'])) 			? $_POST['curso'] 			: NULL;
		$pagamento 		= (isset($_POST['pagamento'])) 		? $_POST['pagamento'] 		: NULL;
		$classificacao 	= (isset($_POST['classificacao'])) 	? $_POST['classificacao'] 	: NULL;
		$outros 		= (isset($_POST['outros'])) 		? $_POST['outros'] 			: NULL;

		?>
		<div class="tudo">
			<div class="menu">
			
				<form class="moldura" action="?" method="post">
				
					<input type="hidden" name="id_processo" value="<?=$id_processo?>">
					<input type="hidden" name="filtrar" 	value="true">
				
					<input type="submit" value="Filtrar" /> 
				
					<br><br><span>Campo / Ordem</span><br><br>
					<?php
					echo "<select style='width: 50%' name='ordem_nome'>";
					foreach($r_ordem_nome as $key => $value){ 
						$selected = ($key == $ordem_nome) ? 'selected' : null; 
						echo "<option value='{$key}' {$selected}>{$value}</option>"; 
					}
					echo "<select>";
					echo "&nbsp;&nbsp;";
					echo "<select style='width: 45%' name='ordem_tipo'>";
					foreach($r_ordem_tipo as $key => $value){ 
						$selected = ($key == $ordem_tipo) ? 'selected' : null;
						echo "<option value='{$key}' {$selected}>{$value}</option>"; 
					}
					echo "<select>";
					?>
						
					<br><br><span>Curso</span><br><br>
					<?php
					echo "<select class='width' name='curso'>";
					foreach($r_cursos as $key => $value){ 
						$selected = ($key == $curso) ? 'selected' : null; 
						echo "<option value='{$key}' {$selected}>{$value}</option>"; 
					}
					echo "<select>";
					?>
						
					<br><br><span>Pagamento</span><br><br>
					<?php
					echo "<select class='width' name='pagamento'>";
					foreach($r_pagamentos as $key => $value){ 
						$selected = ($key == $pagamento) ? 'selected' : null;
						echo "<option value='{$key}' {$selected}>{$value}</option>"; 
					}
					echo "<select>";
					?>
					
					<br><br><span>Classificação</span><br><br>
					<?php
					echo "<select class='width' name='classificacao'>";
					foreach($r_classificacao as $key => $value){ 
						$selected = ($key == $classificacao) ? 'selected' : null; 
						echo "<option value='{$key}' {$selected}>{$value}</option>"; 
					}
					echo "<select>";
					?>
					
					<br><br><span>Outros</span><br><br>
					<?php 
					echo "<table>";
					foreach($r_outros as $key => $value){
					
						$checked = ($outros && in_array($key, $outros))? 'checked' : NULL;
					
						echo "<tr><td><label for='{$key}'>{$value}</label>:</td><td><input {$checked} type='checkbox' name='outros[]' value='{$key}' id='{$key}'></td></tr>";
					
					}
					echo "</table>";
					?>
				
				</form> 
				
				<br>
				<form action="?" method="post">
					<input type="hidden" name="id_processo" value="<?=$id_processo?>">
					<input type="hidden" name="estado">
					<input type="submit" value="Inscritos por estado" /> 
				</form>
				
				<br><br>
				<form action="?" method="post">
					<input type="hidden" name="id_processo" value="<?=$id_processo?>">
					<input type="hidden" name="ficousabendo">
					<input type="submit" value="Como ficou sabendo" /> 
				</form>
				
			</div>
			
			<div class="conteudo">
				<span><?=$r_processos[$id_processo];?></span>
				<br>
				
				<?php
				if(isset($_POST['filtrar'])){
				
		
					$where = " ins.int_etapa = 4 AND ins.id_processo = {$id_processo} ";
					
					$order_by = "ORDER BY {$_POST['ordem_nome']} {$_POST['ordem_tipo']}";
					
					// Condição do curso
					if($_POST['curso'])
					
						$where .= " AND ins.id_curso = {$_POST['curso']} ";
						
					// Condição Pagamento
					if($_POST['pagamento'])
						
						$where .= ($_POST['pagamento'] == 1) ? " AND pgto.id_pagamento IS NOT NULL " :  " AND pgto.id_pagamento IS NULL ";
					
					// Classificação
					if($_POST['classificacao'])
					
						$where .= ($_POST['classificacao'] == 1) ? " AND ins.num_classificacao >= 1 " :  " AND ins.num_classificacao < 1 ";
						
					// Outros condições
					if(isset($_POST['outros'])){
					
						foreach($_POST['outros'] as $value) $where .= " AND {$value} = 'S' ";
					
					}
					
					$query = "
						SELECT
						
							:colunas:
							
						FROM {$DB_PREFIXO}Inscricoes as ins
						
							INNER JOIN {$DB_PREFIXO}PessoasFisicas pf ON
							pf.id_pessoafisica = ins.id_pessoafisica
							
							INNER JOIN {$DB_PREFIXO}Cursos cur ON
							cur.id_curso = ins.id_curso
							
							LEFT JOIN {$DB_PREFIXO}BoletosPagamentos pgto ON 
							pgto.num_inscricao = ins.num_inscricao

						WHERE
							
							{$where}
							
						{$order_by} 
							
					";
					
					$_SESSION['exportar_query'] = base64_encode($query);
					$_SESSION['exportar_id_processo'] = base64_encode($id_processo);
	
					$query = str_replace(':colunas:', 'ins.*, pf.*', $query);
	
					//echo "<pre>{$query}</pre>";
				
					$query = mysql_query($query);
					
					echo "<p>Total de Inscrições: ".mysql_num_rows($query)."</p>";
					
					
					
					if(mysql_num_rows($query)){
					
						?>
						
						<a href="exportar.php" target="_blank"><button>Exportar Excel</button></a>
						<br><br>
						
						<table id="listagem">
							<thead>
								<th>Incrição</th>
								<th>Curso</th>
								<th>Nome</th>
								<th>Celular</th>
								<th>E-Mail</th>
								<th>Cidade</th>
							</thead>
							
							<tbody>
								<?php 
								while($row = mysql_fetch_assoc($query)){
								
									foreach($row as $key => $value) $row[$key] = utf8_encode($value); 
																
									echo "<tr>";
									echo "<td>{$row['num_inscricao']}</td>";
									echo "<td>{$r_cursos[$row['id_curso']]}</td>";
									echo "<td>{$row['str_pessoa_nome']}</td>";
									echo "<td>{$row['str_telefone_celular']}</td>";
									echo "<td>{$row['str_pessoa_email']}</td>";
									echo "<td>{$row['str_endereco_estado']} - {$row['str_endereco_cidade']}</td>";
									echo "</tr>";
								}
								?>
							</tbody>
						</table>
						<?php
					
					}
				
				}
				
				if(isset($_POST['estado'])){
					
					$query = "
						SELECT 
							
							 pf.str_endereco_estado
							,count(pf.str_endereco_estado) as quantidade

						FROM proInscricoes as ins 

						INNER JOIN proPessoasFisicas pf ON
						pf.id_pessoafisica = ins.id_pessoafisica

						WHERE

						ins.int_etapa = 4 AND ins.id_processo = {$id_processo}
						
						GROUP BY pf.str_endereco_estado
						
						ORDER BY pf.str_endereco_estado
						";
						
					$query = mysql_query($query);
					
					?>
					<br>
					<table id="listagem" style="width: 150px">
						<tbody>
							<?php 
							while($row = mysql_fetch_assoc($query)){
	
								echo "<tr>";
								echo "<th>{$row['str_endereco_estado']}</th>";
								echo "<td>{$row['quantidade']}</td>";
								echo "</tr>";
							}
							?>
						</tbody>
					</table>
					<?php
	
					
				}
				
				if(isset($_POST['ficousabendo'])){
				
					$query = "
						SELECT 
							
							 ins.str_comunicacao_ficousabendo
							,count(ins.str_comunicacao_ficousabendo) as quantidade

						FROM proInscricoes as ins 

						WHERE

						ins.int_etapa = 4 AND ins.id_processo = {$id_processo}
						
						GROUP BY ins.str_comunicacao_ficousabendo
						
						ORDER BY ins.str_comunicacao_ficousabendo
						";
						
					$query = mysql_query($query);
					
					?>
					<br>
					<table id="listagem" style="width: 250px">
						<tbody>
							<?php 
							while($row = mysql_fetch_assoc($query)){
	
								foreach($row as $key => $value) $row[$key] = utf8_encode($value); 
	
								echo "<tr>";
								echo "<th style='text-align: left'>{$row['str_comunicacao_ficousabendo']}</th>";
								echo "<td style='text-align: center'>{$row['quantidade']}</td>";
								echo "</tr>";
							}
							?>
						</tbody>
					</table>
					<?php
				
				}
				?>
				
			</div>
			
			<div class="clear"></div>
		</div>
		<?php


	
	}
	?>
	
	
	
</body>
</html>