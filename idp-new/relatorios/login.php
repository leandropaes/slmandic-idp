<!DOCTYPE HTML>
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="estilo.css">
	<title>IDPSP - Relatórios</title>
</head>
<body>
	<div id="login">
	
		<?php
		require_once('../inscricao/__lib__.php');

		if(isset($_POST['usuario_rel']) && isset($_POST['senha_rel'])){

			$usuario = $_POST['usuario_rel'];
			$senha = $_POST['senha_rel'];

			if (($usuario === 'relatorios') && ($senha === 'Vest!bul@r')) {
				
				$_SESSION['sessao_logada_ret'] = 'relatorios';
				
				header('location: ./');
				exit();
			}
			else
				echo "<div class='erro'>ATENÇÃO: Autenticação inválida.</div>";
		
		}
		?>
	
		<form action="?" method="post">
			<fieldset>
				<legend>Identificação</legend>

				<table>
					<tr>
						<td>Usuario:</td><td> <input type="text" name="usuario_rel" /></td>
					</tr>
					<tr>
						<td>Senha:</td><td> <input type="password" name="senha_rel" /></td>
					</tr>
					<tr>
						<td colspan="2" align="right"><input type="submit" value="Entrar" /></td>
					</tr>
				</table>
			</fieldset>
		</form>
	</div>
	
</body>
</html>