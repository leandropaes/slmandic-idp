<!DOCTYPE HTML>
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
</head>
<body>
<?php
	// defino o Nome do arquivo
	$nomeArquivo = "VEST_IDPSP_".date('Y-m-d_H-i-s');
	
	// Configurções do exel
	header("Content-type: application/vnd.ms-excel");
	header("Content-type: application/force-download");
	header("Content-Disposition: attachment; filename={$nomeArquivo}.xls");
	header("Pragma: no-cache");

	require_once('../inscricao/__lib__.php');
	
	if (!isset($_SESSION['sessao_logada_ret']) || !isset($_SESSION['exportar_query'])) {
		header('location: login.php');
		exit();
	}
	
	$query = base64_decode($_SESSION['exportar_query']);
	$id_processo = base64_decode($_SESSION['exportar_id_processo']);
	
	$colunas = " 
	
		 ins.id_inscricao
		,ins.num_inscricao
		,cur.str_nome as str_curso
		
		,pf.str_pessoa_nome
		,pf.str_telefone_celular
		,pf.str_telefone_residencial
		,pf.str_telefone_comercial
		,pf.str_pessoa_rgrne as str_rg
		,pf.str_pessoa_ssp as str_ssp
		,pf.str_pessoa_cpf
		,pf.str_pessoa_email
		,pf.str_pessoa_naturalde as str_naturalidade
		,pf.str_endereco_cep
		,pf.str_endereco_logradouro
		,pf.str_endereco_numero
		,pf.str_endereco_complemento
		,pf.str_endereco_bairro
		,pf.str_endereco_cidade
		,pf.str_endereco_estado
		,pf.str_endereco_pais
		,DATE_FORMAT(pf.dt_pessoa_nascimento,'%d/%m/%Y') as dt_nascimento
		,IF(pf.ch_pessoa_sexo = 'M','Masculino','Feminino') as str_sexo
		
		,ins.str_ensinomedio_local
		,ins.str_ensinomedio_conclusao
		,ins.ch_enem
		,ins.str_enem_ano
		,ins.str_enem_inscricao
		,ins.ch_necessidade_especial
		,ins.str_necessidade_especial
		,IF(ins.ch_necessidade_canhoto = 'S', 'Sim', 'Nao') as str_canhoto
		
		,ins.str_comunicacao_quaislocais
		,ins.str_comunicacao_qualcurso
		,ins.str_comunicacao_ficousabendo
		
		,FORMAT((pgto.val_valor_pago / 100),2,'de_DE') as valor_pgto
		,DATE_FORMAT(pgto.dt_pagamento,'%d/%m/%Y') as dt_pgto ";
	
	$query = str_replace(':colunas:', $colunas, $query);
		
	$query = mysql_query($query);
		
	if(mysql_num_rows($query)){
			
		// Recupero as perguntas do questionário
		$query_per = mysql_query("SELECT str_pergunta FROM proQuestPerguntas WHERE id_processo = {$id_processo} and ch_situacao = 'A' ORDER BY num_posicao	ASC");
		
		while($row_per = mysql_fetch_assoc($query_per)) $perguntas[] = utf8_encode($row_per['str_pergunta']);
			
		?>
		<table border="1">
		
			<thead>
				<th colspan="2"></th>
				<th colspan="18">Pessoa Física</th>
				<th colspan="5">Inscrição</th>
				<th colspan="3">Comunicação</th>
				<th colspan="2">Pagamento</th>
				<th colspan="<?=count($perguntas)?>">Questionário</th>
			</thead>

			<tbody>
			
				<tr>
					<th>N° Inscrição</th>
					<th>Curso</th>
					
					<th>Nome</th>
					<th>Data de Nascimento</th>
					<th>Sexo</th>
					<th>Tel. Celular</th>
					<th>Tel. Residencial</th>
					<th>Tel. Comercial</th>
					<th>E-mail</th>
					<th>RG</th>
					<th>CPF</th>
					<th>Naturalidade</th>
					<th>CEP</th>
					<th>Logradouro</th>
					<th>Número</th>
					<th>Complemento</th>
					<th>Bairro</th>
					<th>Cidade</th>
					<th>Estado</th>
					<th>País</th>
					
					<th>Ano de conclusão Ensino Médio</th>
					<th>Local de conclusão Ensino Médio</th>
					<th>Enem</th>
					<th>PNE</th>
					<th>Canhoto</th>
					
					<th>Está prestando vestibular em outra IES, qual(is)?</th>
					<th>Qual(is) outro(s) curso(s) está prestando?</th>
					<th>Como ficou sabendo do vestibular do IDPSP?</th>
					
					<th>Valor Pago</th>
					<th>Data do Pagamento</th>
					
					<?php foreach($perguntas as $pergunta) echo "<th>{$pergunta}</th>"; ?>
					
				</tr>
			
				<?php 
				while($row = mysql_fetch_assoc($query)){
				
					foreach($row as $key => $value) $row[$key] = utf8_encode($value); 
	
					
	
					echo "<tr>";
					
					echo "<td>{$row['num_inscricao']}</td>";
					echo "<td>{$row['str_curso']}</td>";
					
					echo "<td>{$row['str_pessoa_nome']}</td>";
					echo "<td>{$row['dt_nascimento']}</td>";
					echo "<td>{$row['str_sexo']}</td>";
					echo "<td>{$row['str_telefone_celular']}</td>";
					echo "<td>{$row['str_telefone_residencial']}</td>";
					echo "<td>{$row['str_telefone_comercial']}</td>";
					echo "<td>{$row['str_pessoa_email']}</td>";
					echo "<td>{$row['str_rg']} - {$row['str_ssp']}</td>";
					echo "<td>{$row['str_pessoa_cpf']}</td>";
					echo "<td>{$row['str_naturalidade']}</td>";
					echo "<td>{$row['str_endereco_cep']}</td>";
					echo "<td>{$row['str_endereco_logradouro']}</td>";
					echo "<td>{$row['str_endereco_numero']}</td>";
					echo "<td>{$row['str_endereco_complemento']}</td>";
					echo "<td>{$row['str_endereco_bairro']}</td>";
					echo "<td>{$row['str_endereco_cidade']}</td>";
					echo "<td>{$row['str_endereco_estado']}</td>";
					echo "<td>{$row['str_endereco_pais']}</td>";
					
					echo "<td>{$row['str_ensinomedio_conclusao']}</td>";
					echo "<td>{$row['str_ensinomedio_local']}</td>";
					echo "<td>{$row['str_enem_ano']} - {$row['str_enem_inscricao']}</td>";
					echo "<td>{$row['ch_necessidade_especial']}</td>";
					echo "<td>{$row['str_canhoto']}</td>";
					
					echo "<td>{$row['str_comunicacao_quaislocais']}</td>";
					echo "<td>{$row['str_comunicacao_qualcurso']}</td>";
					echo "<td>{$row['str_comunicacao_ficousabendo']}</td>";
					
					echo "<td>{$row['valor_pgto']}</td>";
					echo "<td>{$row['dt_pgto']}</td>";
					
					$query_qst = "
					SELECT 
					
						IF(res.str_resposta IS NOT NULL, res.str_resposta, alt.str_alternativa) as str_alternativa

					FROM proQuestRespostas as res

						INNER JOIN proQuestPerguntas per ON
						per.id_qsepergunta = res.id_pergunta

						LEFT JOIN proQuestAlternativas alt ON
						alt.id_qsealternativa = res.id_alternativa

					WHERE

						res.id_inscricao = {$row['id_inscricao']}

					ORDER BY res.id_pergunta ASC";
					
					$query_qst = mysql_query($query_qst);
					
					while($row_qst = mysql_fetch_assoc($query_qst)){
					
						echo "<td>".utf8_encode($row_qst['str_alternativa'])."</td>";
						
					}
					
					echo "</tr>";
				}
				?>
			</tbody>
		</table>
		<?php
	}
	else
	
		echo "Nenhuma inscrição encontrada.";
?>
</body>
</html>