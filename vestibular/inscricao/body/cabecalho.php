<div id="cabecalho">
	<?php 
	// Monto a query
	// Verifico se existe processo em aberto
	$query ="SELECT 
				id_processo, 
				num_processo_ano,
				str_processo_apelido,
				num_vencimentopadrao,
				num_processo_ano,
				str_processo_semestre,
				dt_vencimento_maximo,
				dt_inscricao_inicio,
				dt_inscricao_fim,
				NOW() as datetime,
				DATE_FORMAT( `dt_inscricao_inicio`, '%H:%i de %d/%m/%Y' ) as dateInicio,
				str_arquivo_portaria,
				str_arquivo_edital,
				str_nome_portaria,
				str_nome_edital
			FROM 
				".DB_PREFIXO."Processos 
			WHERE
				ch_situacao = 'A'
			LIMIT 1
			";
				
	$resultado = mysql_query($query, $conectar); 
	
	if (mysql_num_rows($resultado)){
	
		$linha = mysql_fetch_object($resultado);
		
		// recupero as informações
		$txt_ano     		  = $linha->num_processo_ano;
		$str_processo_apelido = $linha->str_processo_apelido;
		$num_processo_ano 	  = $linha->num_processo_ano;
		$str_processo_semestre= $linha->str_processo_semestre;
		$num_vencimentopadrao = $linha->num_vencimentopadrao;
		$dt_vencimento_maximo = $linha->dt_vencimento_maximo;
		$id_processo 		  = $linha->id_processo;
		$dt_inscricao_inicio  = $linha->dt_inscricao_inicio;
		$dt_inscricao_fim     = $linha->dt_inscricao_fim;
		$datetimeAtual        = $linha->datetime;
		$dateInicio           = $linha->dateInicio;
		
		$arquivo_portaria	  = $linha->str_arquivo_portaria;
		$arquivo_edital		  = $linha->str_arquivo_edital;
		
		$nome_portaria	  = $linha->str_nome_portaria;
		$nome_edital      = $linha->str_nome_edital;
		
		
		// Controle se já iniciou ou encerrou as incrições
		if ($datetimeAtual < $dt_inscricao_inicio)
			$txt_incricoes = 'As Inscrições iniciam <br/>a partir das<br/><br/>'.$dateInicio;
			
		elseif($datetimeAtual > $dt_inscricao_fim)
			$txt_incricoes = 'Inscrições encerradas.';
			
		else
			$txt_incricoes = FALSE;
		
		// defino que existe um processo vigente
		$existeProcesso = TRUE;
	}
	else{
		$existeProcesso = FALSE;
	}
	?>
	<table border="0">
		<tr>
			<td class="imagem"><img src="images/logo.png" width="150px"/></td>
			<td class="titulo"><?=($existeProcesso)? ('Vestibular '.$num_processo_ano.'<br/>'.utf8_encode($str_processo_semestre)): 'VESTIBULAR';?></td>
		</tr>
	</table>
</div>