<div id="area_barra"><table><tr><td>Seja Bem-vindo <?=utf8_encode($r_form['str_pessoa_nome'])?> !</td><td style="text-align: right">| <a href="sair.php" style="color: white; text-decoration: none">sair</a> |</td></tr></table></div>

<div id="area_principal">
	
	<!-- ------------------------------------------------------------
	  -- MENU
	  -- ------------------------------------------------------------>
	
	<div id="area_menu">
		<a href="index.php?cadastro"><div id="area_sub<?= (isset($_GET['cadastro']))? '_atual': NULL;?>">Dados cadastrais</div></a>

		<?php
		$query = "	SELECT 
						  processos.num_processo_ano
						, processos.id_processo
						, inscricoes.id_inscricao
						, inscricoes.num_inscricao
						, cursos.str_nome
						
					FROM 
						".DB_PREFIXO."Processos AS processos
						
						INNER JOIN ".DB_PREFIXO."Inscricoes inscricoes ON 
						processos.id_processo = inscricoes.id_processo
						AND 
						inscricoes.id_pessoafisica = '".$r_form['id_pessoafisica']."'
						
						INNER JOIN ".DB_PREFIXO."Cursos cursos ON 
						inscricoes.id_curso = cursos.id_curso
						
					ORDER 
						BY num_processo_ano DESC;";
						
		$resultado = mysql_query($query, $conectar);
		
		// GERO O MENU
		
		$processo_ano = array();		
		$menu         = array();
		
		while($linha = mysql_fetch_object($resultado)){

			((isset($_GET['inscricao']))&&(base64_decode($_GET['inscricao']) == $linha->id_inscricao))? $atual = '_atual': $atual = NULL;

			if (!in_array($linha->num_processo_ano, $processo_ano)){
				$menu['ano'][]  = $linha->num_processo_ano;								
				$processo_ano[] = $linha->num_processo_ano;
			}
			
			$menu[$linha->num_processo_ano][] = $atual;
			
			$menu['sub_menu'][] = array(
				  "ano"				=> $linha->num_processo_ano
				, "link"			=>'?inscricao='.base64_encode($linha->id_inscricao)
				, "num_inscricao"	=>'['.$linha->num_inscricao.']'
				, "curso"			=>$linha->str_nome
				, "atual"			=>$atual
			);
			
		}
		
		
		foreach($menu['ano'] as $key => $txt_ano){
			
			(in_array('_atual', $menu[$txt_ano]))? $atual = '_atual':  $atual = NULL;
			
			echo '<div id="area_transf',$atual,'"  >Vestibular ',$txt_ano,'</div>';
			
			foreach($menu['sub_menu'] as $key2 => $subMenu){
			
				if($subMenu['ano'] == $txt_ano) 
				
					echo '<a href="index.php',$subMenu['link'],'"><div id="area_transf_sub',$subMenu['atual'],'" ><span>',$subMenu['num_inscricao'],'</span> ',$subMenu['curso'],'</div></a>'; 			
			}

		}
?>

	</div>

	<!-- ------------------------------------------------------------
	  -- CONTEUDO
	  -- ------------------------------------------------------------>
	
	<div id="area_conteudo">

		
		<!-- ALERTAS -->
		<div id="msg_inscricao"></div>
		
		
		<?php 
			
		if(isset($_GET['inscricao'])){
			
			$id_inscricao = base64_decode($_GET['inscricao']);

			// recupero os dados dessa inscrição
			
			$query = "	SELECT 
			
							processos.dt_inscricao_fim, 
							processos.dt_vencimento_maximo,
							processos.dt_divulgacaoResultado,
							DATE_FORMAT(processos.dt_divulgacaoResultado, '%H:%i do dia %d/%m/%Y' ) as dateDivulgacao,
							now() as dataAtual,
							inscricoes.* 
						
						FROM ".DB_PREFIXO."Inscricoes as inscricoes

							INNER JOIN ".DB_PREFIXO."Processos processos ON
							processos.id_processo = inscricoes.id_processo
						
						WHERE 
						
							inscricoes.id_inscricao = {$id_inscricao} 
							
					;";
			
			$resultado = mysql_query($query, $conectar);
			
			if(mysql_num_rows($resultado)){
			
				$linha = mysql_fetch_object($resultado);

				// Verifico se o ID dessa inscricao é o mesmo da pessoa logada
				if ($r_form['id_pessoafisica'] != $linha->id_pessoafisica){
				
					?>
					<script>
						$("#msg_inscricao").show("slow");
						$("#msg_inscricao").html("<b>Alerta:</b> Você não tem permissão para acessar essa inscrição.");
					</script>
					<?php
				
				}
				
				// Verifico se o usuário finalizou o processo
				elseif($linha->int_etapa < 4){
				
					if ($linha->dataAtual > $linha->dt_inscricao_fim){
						?><script>
							$("#msg_inscricao").show("slow");
							$("#msg_inscricao").html("<b>Alerta:</b> Você ainda não finalizou essa inscrição. Período de Inscrição encerrada para esse Processo.");
						</script><?php
					}
					else{
						?><script>
							$("#msg_inscricao").show("slow");
							$("#msg_inscricao").html("<b>Alerta:</b> Você ainda não finalizou esse processo, <a href='sair.php' id_inscricao='<?=$id_inscricao?>' id='redirecionar' style='color: white'>clique aqui para finalizar</a>. <span id='carregando_red' style='display: none;'><b><i>Redirecionando&nbsp;&nbsp;<img src='images/carregando.gif'/></i></p></span>");
						</script><?php	
					}
					?>
					<script type="text/javascript" src="do/script/validaRedirecionar.js"></script>
					<?php
					
		
				}
				else{
				
					require_once("body/areaInscrito/formInscricao.php");
				
				}

			
			}
			else{
				
				?>
				<script>
					$("#msg_inscricao").show("slow");
					$("#msg_inscricao").html("<b>Alerta:</b> Inscrição não encontrada.");
				</script>
				<?php

			}

		}
		elseif(isset($_GET['cadastro'])){
		
			require_once("body/areaInscrito/formCadastro.php");
		}
		else{
		
			?><center><img src="images/transferencia.png"></center><?php
		
		}

		?>
		

	</div>

</div>




