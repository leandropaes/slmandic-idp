<?php
// ATUALIZAÇÃO DO BOLETO PARA UMA NOVA VIA

if(isset($_POST['id_boleto'])){

	// RECUPERO AS INFORMAÇÕES DAS SESSÕES
	$id_boleto  = $_POST['id_boleto'];

	// RECUPERO AS INFORMAÇÕES DO BOLETO
	$query = "	SELECT 
					  boletos.* 
					, proc.num_vencimentopadrao
					, proc.dt_vencimento_maximo
					
				FROM 
					".DB_PREFIXO."Boletos AS boletos
					
					INNER JOIN ".DB_PREFIXO."Processos proc ON 
					proc.id_processo = boletos.id_processo
				
				WHERE 
					boletos.id_boleto =  '".$id_boleto."' 
				;";	
				
				
	$resultado = mysql_query($query, $conectar);
	
	$boletos = mysql_fetch_object($resultado);
	
	
	// NUMERO DA VIA

	$num_via = $boletos->num_via;
	$num_via++;

	

	$temp = explode(' ', $boletos->dt_gerado_em);
	
	// DATA EM QUE ESTA SENDO GERADO
	$boleto_gerado_em = $temp[0];
	
	// HORA EM QUE ESTA SENDO GERADO
    $boleto_gerado_as = $temp[1];
	
	// NUMERO DA INSCRICAO
    $boleto_inscricao = $boletos->id_inscricao;
	
	// DIA QUE IRA VENCER
	$num_vencimentopadrao = $boletos->num_vencimentopadrao;
	$dt_vencimento_maximo = $boletos->dt_vencimento_maximo;

    $boleto_vencimento = date('Y-m-d', strtotime("+" . $num_vencimentopadrao . " day"));
	
	if((strtotime((date('Y-m-d H:i:s', strtotime("+" . $num_vencimentopadrao . " day"))))) >= (strtotime($dt_vencimento_maximo))){
		$boleto_vencimento = $dt_vencimento_maximo;
	} else {
		$boleto_vencimento = date("Y-m-d H:i:s", strtotime("+".$num_vencimentopadrao."day"));
	}

	
	// VALOR DO BOLETO
	$boleto_valor = $boletos->val_valor;
	
	// ESTRUTURO OS BLOCOS DO BOLETO
	$bloco_1 = substr(md5($boleto_vencimento), 6, 17);
    $bloco_2 = substr(sha1($boleto_inscricao), 9, 18);
    $bloco_3 = substr(md5($boleto_valor), 13, 6);
    $bloco_4 = substr(sha1($boleto_gerado_em), 27, 2);
    $bloco_5 = substr(md5($boleto_gerado_as), 9, 7);

	// ESTRUTURO O CODIGO DO BOLETO
    $boleto_codigo = $bloco_2 . $bloco_4 . $bloco_1 . $bloco_5 . $bloco_3;
	
	
	$query = "	UPDATE 
					".DB_PREFIXO."Boletos 
				SET 
					
					 num_via 			= '".$num_via."'
					,str_codigodoboleto = '".$boleto_codigo."'
					,dt_vencimento 		= '".$boleto_vencimento."'
					,dt_ultimavia 		= now()
				
				WHERE
				
					id_boleto = '".$id_boleto."'
				
			;";
	
	mysql_query($query, $conectar);

}

?>