<?php 
require_once('__lib__.php');?>

<!DOCTYPE HTML>
<html>

<head>
    <title>IDP - Vestibular</title>
	<!--<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />-->
	<meta charset="utf-8">
	
	<!-- STYLE / CSS -->
    <link rel="stylesheet" type="text/css" href="css/style.css">
	
	<!-- JAVA e JQUERY -->
	<script type="text/javascript" src="js/jquery-1.10.2.js"></script>
	<script type="text/javascript" src="js/jquery.util.js"></script>
	
	<script type="text/javascript">
	/* Máscaras ER */
	function mascara(o,f){
		v_obj=o
		v_fun=f
		setTimeout("execmascara()",1)
	}
	function execmascara(){
		v_obj.value=v_fun(v_obj.value)
	}
	function mtel(v){
		v=v.replace(/\D/g,"");                  //Remove tudo o que não é dígito
		v=v.replace(/^(\d{2})(\d)/g,"($1) $2"); //Coloca parênteses em volta dos dois primeiros dígitos
		v=v.replace(/(\d)(\d{4})$/,"$1-$2");    //Coloca hífen entre o quarto e o quinto dígitos
		return v;
	}
	</script>

</head>

<body>
	<?php
	require_once('body/cabecalho.php');
	
	if(!isset($_GET['recuperar_senha']))
		require_once('body/conteudo.php');
	else	
		require_once('body/recuperarSenha/index.php');
		
	require_once('body/rodape.php');
	?>
	
	<script type="text/javascript" src="js/jquery.mask.js"></script>
	
</body>

</html>


