<?php

/* --------------------------------------------------- */
/* VALIDAR DATA
/* --------------------------------------------------- */

function validaData($dat){

	// verifico se a data tem o tamanho correto e
	// se ela n�o � nula
	if(($dat == NULL )|| (strlen($dat) < 10) || ($dat == '11/11/1111') ){ 
		return FALSE;
	}
	else{
	
		$data = explode("/","$dat"); // fatia a string $dat em pedados, usando / como refer�ncia
		$d = $data[0];
		$m = $data[1];
		$y = $data[2];
	 
		// verifica se a data � v�lida!
		// 1 = true (v�lida)
		// 0 = false (inv�lida)
		$res = checkdate($m,$d,$y);
		if ($res == 1){
			return TRUE;
		} else {
		   return FALSE;
		}
	
	}

}

/* --------------------------------------------------- */
/* VALIDAR EMAIL
/* --------------------------------------------------- */

function validaEmail($mail){
	if(preg_match("/^([[:alnum:]_.-]){3,}@([[:lower:][:digit:]_.-]{3,})(.[[:lower:]]{2,3})(.[[:lower:]]{2})?$/", $mail)) {
		return true;
	}else{
		return false;
	}
}

/* --------------------------------------------------- */
/* VALIDAR CPF
/* --------------------------------------------------- */

// Fun��o que valida o CPF
function validaCPF($cpf){	

	// Verifiva se o n�mero digitado cont�m todos os digitos
    $cpf = str_pad(ereg_replace('[^0-9]', '', $cpf), 11, '0', STR_PAD_LEFT);
	
	// Verifica se nenhuma das sequ�ncias abaixo foi digitada, caso seja, retorna falso
    if (strlen($cpf) != 11 || $cpf == '00000000000' || $cpf == '11111111111' || $cpf == '22222222222' || $cpf == '33333333333' || $cpf == '44444444444' || $cpf == '55555555555' || $cpf == '66666666666' || $cpf == '77777777777' || $cpf == '88888888888' || $cpf == '99999999999')
	{
	return false;
    }
	else
	{   // Calcula os n�meros para verificar se o CPF � verdadeiro
        for ($t = 9; $t < 11; $t++) {
            for ($d = 0, $c = 0; $c < $t; $c++) {
                $d += $cpf{$c} * (($t + 1) - $c);
            }
 
            $d = ((10 * $d) % 11) % 10;
 
            if ($cpf{$c} != $d) {
                return false;
            }
        }
 
        return true;
    }
}

/* --------------------------------------------------- */
/* GERAR DIA DA SEMANA COM UMA DATA (dd/mm/AAAA)
/* --------------------------------------------------- */

function diasemana($data) {
	$ano =  substr("$data", 0, 4);
	$mes =  substr("$data", 5, -3);
	$dia =  substr("$data", 8, 9);

	$diasemana = date("w", mktime(0,0,0,$mes,$dia,$ano) );

	switch($diasemana) {
		case"0": $diasemana = "Domingo";       break;
		case"1": $diasemana = "Segunda-Feira"; break;
		case"2": $diasemana = "Ter�a-Feira";   break;
		case"3": $diasemana = "Quarta-Feira";  break;
		case"4": $diasemana = "Quinta-Feira";  break;
		case"5": $diasemana = "Sexta-Feira";   break;
		case"6": $diasemana = "S�bado";        break;
	}
	
	switch($mes) {
		case"01": $mes = "Janeiro";       break;
		case"02": $mes = "Fevereiro";   break;
		case"03": $mes = "Mar�o";  		break;
		case"04": $mes = "Abril";  break;
		case"05": $mes = "Maio";   break;
		case"06": $mes = "Junho";        break;
		case"06": $mes = "Julho";        break;
		case"06": $mes = "Agosto";        break;
		case"06": $mes = "Setembro";        break;
		case"06": $mes = "Outubro";        break;
		case"06": $mes = "Novembro";        break;
		case"06": $mes = "Dezembro";        break;
	}

	echo "{$diasemana} {$dia} de {$mes}";
}

/* --------------------------------------------------- */
/* GERO O LINK DA URL ATUAL ATE SUA RAIZ
/* --------------------------------------------------- */

function gerarURL(){

	$server = $_SERVER['SERVER_NAME']; 
	$endereco = $_SERVER ['REQUEST_URI'];

	$endereco = $server.$endereco;
	$temp = explode('?', $endereco);
	$endereco = "http://".$temp[0];

	return $endereco;

}

/* --------------------------------------------------- */
/* COLOCO CIFR�O NO VALOR
/* --------------------------------------------------- */

function cifrao($valor){

	return 'R$ '.$valor.',00';

}


/* --------------------------------------------------- */
/* GERAR NUMERO DA INSCRICAO
/* --------------------------------------------------- */

function gerarNumInscricao($id_inscricao){
 
	 // Conex�o com o Banco de Dados
			
	$user     = "root";      # usuário
	$password = "root";        # senha
	$server   = "localhost"; # Servidor
	$database = "edbcursosdedir2";  	# nome do banco de dados
			
		
	$conectar = mysql_connect($server, $user, $password); 
		
	mysql_select_db($database, $conectar);
	
	// Pego o ID do processo
	$query = "SELECT id_processo FROM ".DB_PREFIXO."Inscricoes WHERE id_inscricao = '".$id_inscricao."';";	
	
	$resultado = mysql_query($query, $conectar);	
	$inscricao = mysql_fetch_object($resultado);
	
	$id_processo   = $inscricao->id_processo;
	
	// 
	$query = "SELECT num_inscricao FROM ".DB_PREFIXO."Inscricoes WHERE id_processo = '".$id_processo."' ORDER BY num_inscricao DESC LIMIT 1;";	
	
	$resultado  = mysql_query($query, $conectar);	
	$inscricao2 = mysql_fetch_object($resultado);
	
	$num_inscricao = $inscricao2->num_inscricao;
	
	// se o numero de inscricao for nulo ent�o significa que ela � a primeira, 
	// busco na tabela do processo qual sera o padr�o
	if(!$num_inscricao){
	
		$query = "SELECT num_primeirainscricao FROM ".DB_PREFIXO."Processos WHERE id_processo = '".$id_processo."';";	
		$resultado = mysql_query($query, $conectar);	
		$processo = mysql_fetch_object($resultado);
		
		$num_primeirainscricao = $processo->num_primeirainscricao;
	
		return $num_primeirainscricao;
	
	}
	// se n�o for nulo ent�o o numero de inscricao sera o proximo
	else{
	
		return $num_inscricao+1;
	
	}

}


/* -------------------------------------------------------------------- */
/* FUN��O PARA CONTRALAR MULTIPLOS SELECTS NO MESMO FORMULARIO
/* -------------------------------------------------------------------- */

function get_post_action($name){
	$params = func_get_args();

	foreach ($params as $name){
		if (isset($_POST[$name])){
			return $name;
		}
	}
}

/* -------------------------------------------------------------------- */
/* FORMATAR DATA
/* -------------------------------------------------------------------- */

function formatarData($data, $tipo){

		
	if (($data == NULL) or ($data == '0000-00-00')){
	
		return false;
	}	
	else{
	
		if ($tipo == 'DB'){
			// data = 06/03/1993 -> 1993-03-06
			$data = explode('/', $data);
			return $data[2].'-'.$data[1].'-'.$data[0];
		}
		// TIPO == ''
		else{
			// data = 1993-03-06 -> 06/03/1993 
			$data = explode('-', $data);
			return $data[2].'/'.$data[1].'/'.$data[0];
		}
	
	}


}

/* 	Formata a data e hora vinda do banco
	Entrada -> 2014-06-03 12:15:01
	Saida   <- 03/06/2014 �s 12:15
*/

function formatarDataHoraDB($datahora){

	$temp = explode(' ', $datahora);
	$data = $temp[0];
	$hora = $temp[1];
	
	$data = formatarData($data, '');

	$hora = substr($hora, 0, 5);
	
	return $data .' &agrave;s '.$hora;

} 

/* Data OLD */

function data($data){

	$data = explode('-', $data);
	
	return $data[2].'/'.$data[1].'/'.$data[0];

} 


/* -------------------------------------------------------------------- 
   FUN��O ONDE TEM COMO ENTRATA UM NOME AM CAIXA ALTA
   E SAIDA COM AS STRINGS MINUSCULAS E AS PRIMEIRAS
   LETRAS MAIUSCULAS.
  -------------------------------------------------------------------- */

function str_minusculo($nome){
    
    // crio um array
    $array = array('do', 'Do', 'DO', 'da', 'Da', 'DA', 'de', 'De', 'DE', 'dos', 'Dos', 'DOS', 'das', 'Das', 'DAS');
    
    // Deixo o texto todo em minusculo
    $nome = strtolower($nome);
    
    // Divido as partes do texto pelo Espa�o
    $r_nome = explode(" ", $nome);
    
    // Quantidade de palavras no mome
    $n_nome = count($r_nome);
    
    $nome = ucwords($r_nome[0]);
    
    for ($i = 1; $i < $n_nome; $i++){
        
        if (!in_array($r_nome[$i], $array)){
            
            // Deixo a primeira letra maiuscula
            $nome .= " ".ucwords($r_nome[$i]);
        }
        else{
            
            $nome .= " ".$r_nome[$i];
            
        }
        
    }
	
	// Se o tamanho do texto for maior doque 32 caracteres ent�o pego o primeiro e ultimo nome
	if(strlen($nome) > 32)
		return abrevia($nome);
    else
		return $nome;
    
}


?>