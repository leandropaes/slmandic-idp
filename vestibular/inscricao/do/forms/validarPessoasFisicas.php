<?php
	// Busco os estados

		$r_estado = array();
		$query = "SELECT * FROM  ".DB_PREFIXO."Estados;";
		$resultado = mysql_query($query, $conectar);
		while($linha = mysql_fetch_object($resultado)){
			$r_estado[$linha->str_sigla] = $linha->str_nome;
		}

	// Busco os anos do Enem

		$r_enem = null;
		$query = "SELECT str_ano FROM  ".DB_PREFIXO."Enem WHERE id_processo = {$id_processo} ORDER BY str_ano ASC;";
		$resultado = mysql_query($query, $conectar);
		while($linha = mysql_fetch_object($resultado)){
			$r_enem[$linha->str_ano] = $linha->str_ano;
		}
		
	// Busco as opções de como ficou sabendo

		$r_ficousabendo = array();
		$query = "SELECT str_nome FROM ".DB_PREFIXO."QuestFicouSabendo";
		$resultado = mysql_query($query, $conectar);
		while($linha = mysql_fetch_object($resultado)){
			$r_ficousabendo[utf8_encode($linha->str_nome)] = utf8_encode($linha->str_nome);
		}
	
	// Busco Sexo
		$r_sexo = array('M'=>'Masculino', 'F'=>'Feminino');
	
	// Busco SMS
		$r_sms  = array('S'=>'SIM', 'N'=>'NÃO');
		

	// Identificação de Campo Obrigatorio Pessoa física
		$str_sistema_senha 		  	= BORDA_PADRAO;
		$confir_str_sistema_senha 	= BORDA_PADRAO;
		$str_telefone_residencial 	= BORDA_PADRAO;
		$str_telefone_comercial   	= BORDA_PADRAO;
		$str_telefone_celular 	  	= BORDA_PADRAO;
		$str_pessoa_nome 		  	= BORDA_PADRAO;
		$str_pessoa_rgrne 		 	= BORDA_PADRAO;
		$str_pessoa_ssp 			= BORDA_PADRAO;
		$str_pessoa_cpf 			= BORDA_PADRAO;
		$str_pessoa_email 			= BORDA_PADRAO;
		$str_pessoa_naturalde 		= BORDA_PADRAO;
		$str_endereco_cep 			= BORDA_PADRAO;
		$str_endereco_logradouro 	= BORDA_PADRAO;
		$str_endereco_numero 		= BORDA_PADRAO;
		$str_endereco_complemento	= BORDA_PADRAO;
		$str_endereco_bairro 		= BORDA_PADRAO;
		$str_endereco_cidade 		= BORDA_PADRAO;
		$str_endereco_estado 		= BORDA_PADRAO;
		$str_endereco_pais 			= BORDA_PADRAO;
		//$dt_pessoa_registro 		= BORDA_PADRAO;
		$dt_pessoa_nascimento 		= BORDA_PADRAO;
		$ch_comunicacao_sms 		= BORDA_PADRAO;
		$ch_pessoa_sexo 			= BORDA_PADRAO;

	// Identificação dos Campos Obrigatórios Inscrição

		$str_enem_ano					= NULL;
		$str_enem_inscricao				= BORDA_PADRAO;
		$str_ensinomedio_local      	= BORDA_PADRAO;
		$str_ensinomedio_conclusao  	= BORDA_PADRAO;
		$str_comunicacao_ficousabendo 	= BORDA_PADRAO;
		$str_necessidade_especial		= BORDA_PADRAO;
		$str_comunicacao_qualcurso		= BORDA_PADRAO;	
		$str_comunicacao_quaislocais    = BORDA_PADRAO;
		
	// Mensagens
		$msg_senha = NULL;
		$msg_email = NULL;
		$msg_data = NULL;

	$validacao = TRUE;

if (isset($_POST['salvarPessoaFisica'])){
	
	// considero que o formulario esta validado
	
	// Validação do formulário
	
	// Informações da pessoa fisica
	$r_cadastro['str_sistema_senha'] 		= $_POST['str_sistema_senha']; 			if($r_cadastro['str_sistema_senha'] 		== NULL){ $validacao = FALSE; $str_sistema_senha 		= BORDA_RED;}
	$r_cadastro['confir_str_sistema_senha'] = $_POST['confir_str_sistema_senha']; 	if($r_cadastro['confir_str_sistema_senha'] 	== NULL){ $validacao = FALSE; $confir_str_sistema_senha = BORDA_RED;}
	$r_cadastro['str_telefone_residencial'] = $_POST['str_telefone_residencial'];	if($r_cadastro['str_telefone_residencial'] 	== NULL){ $validacao = FALSE; $str_telefone_residencial = BORDA_RED;}
	$r_cadastro['str_telefone_comercial'] 	= $_POST['str_telefone_comercial']; 	//if($r_cadastro['str_telefone_comercial'] 	== NULL){ $validacao = FALSE; $str_telefone_comercial 	= BORDA_RED;}
	$r_cadastro['str_telefone_celular']		= $_POST['str_telefone_celular']; 		if($r_cadastro['str_telefone_celular'] 		== NULL){ $validacao = FALSE; $str_telefone_celular		= BORDA_RED;}
	$r_cadastro['str_pessoa_nome'] 			= $_POST['str_pessoa_nome']; 			if($r_cadastro['str_pessoa_nome'] 			== NULL){ $validacao = FALSE; $str_pessoa_nome 			= BORDA_RED;}
	$r_cadastro['str_pessoa_rgrne'] 		= $_POST['str_pessoa_rgrne']; 			if($r_cadastro['str_pessoa_rgrne'] 			== NULL){ $validacao = FALSE; $str_pessoa_rgrne 		= BORDA_RED;}
	$r_cadastro['str_pessoa_ssp'] 			= $_POST['str_pessoa_ssp']; 			if($r_cadastro['str_pessoa_ssp']			== NULL){ $validacao = FALSE; $str_pessoa_ssp 			= BORDA_RED;}
	$r_cadastro['str_pessoa_email'] 		= $_POST['str_pessoa_email']; 			if($r_cadastro['str_pessoa_email'] 			== NULL){ $validacao = FALSE; $str_pessoa_email 		= BORDA_RED;}
	$r_cadastro['str_pessoa_naturalde'] 	= $_POST['str_pessoa_naturalde']; 		if($r_cadastro['str_pessoa_naturalde']		== NULL){ $validacao = FALSE; $str_pessoa_naturalde 	= BORDA_RED;}
	$r_cadastro['str_endereco_cep'] 		= $_POST['str_endereco_cep']; 			if($r_cadastro['str_endereco_cep'] 			== NULL){ $validacao = FALSE; $str_endereco_cep 		= BORDA_RED;}
	$r_cadastro['str_endereco_logradouro'] 	= $_POST['str_endereco_logradouro']; 	if($r_cadastro['str_endereco_logradouro'] 	== NULL){ $validacao = FALSE; $str_endereco_logradouro 	= BORDA_RED;}
	$r_cadastro['str_endereco_numero'] 		= $_POST['str_endereco_numero']; 		if($r_cadastro['str_endereco_numero']		== NULL){ $validacao = FALSE; $str_endereco_numero 		= BORDA_RED;}
	$r_cadastro['str_endereco_complemento'] = $_POST['str_endereco_complemento']; 	//if($r_cadastro['str_endereco_complemento'] 	== NULL){ $validacao = FALSE; $str_endereco_complemento = BORDA_RED;}
	$r_cadastro['str_endereco_bairro'] 		= $_POST['str_endereco_bairro']; 		if($r_cadastro['str_endereco_bairro'] 		== NULL){ $validacao = FALSE; $str_endereco_bairro 		= BORDA_RED;}
	$r_cadastro['str_endereco_cidade'] 		= $_POST['str_endereco_cidade']; 		if($r_cadastro['str_endereco_cidade'] 		== NULL){ $validacao = FALSE; $str_endereco_cidade 		= BORDA_RED;}
	$r_cadastro['str_endereco_estado'] 		= $_POST['str_endereco_estado']; 		if(($r_cadastro['str_endereco_estado'] 		== '(Selecione)')||($r_cadastro['str_endereco_estado'] 		== NULL)){ $validacao = FALSE; $str_endereco_estado 		= BORDA_RED;}
	$r_cadastro['str_endereco_pais'] 		= $_POST['str_endereco_pais']; 			if($r_cadastro['str_endereco_pais'] 		== NULL){ $validacao = FALSE; $str_endereco_pais		= BORDA_RED;}
	//$r_cadastro['dt_pessoa_registro'] 		= $_POST['dt_pessoa_registro']; 		if($r_cadastro['dt_pessoa_registro'] 		== NULL){ $validacao = FALSE; $dt_pessoa_registro 		= BORDA_RED;}
	$r_cadastro['ch_comunicacao_sms'] 		= $_POST['ch_comunicacao_sms']; 		if($r_cadastro['ch_comunicacao_sms'] 		== NULL){ $validacao = FALSE; $ch_comunicacao_sms 		= BORDA_RED;}
	$r_cadastro['ch_pessoa_sexo'] 			= $_POST['ch_pessoa_sexo'];				if($r_cadastro['ch_pessoa_sexo']			== '(Selecione)'){ $validacao = FALSE; $ch_pessoa_sexo 			= BORDA_RED;}
	$r_cadastro['dt_pessoa_nascimento'] 	= $_POST['dt_pessoa_nascimento']; 
	
	// Valido Enem
	$r_cadastro['ch_enem'] 	= $_POST['ch_enem'];
	
	if($r_cadastro['ch_enem'] == 'N'){
		$r_cadastro['str_enem_ano'] 		= NULL;
		$r_cadastro['str_enem_inscricao'] 	= NULL;
	}
	else{
		$r_cadastro['str_enem_ano'] = (isset($_POST['str_enem_ano']))?$_POST['str_enem_ano']:NULL; if($r_cadastro['str_enem_ano'] == NULL){ $validacao = FALSE; $str_enem_ano = BORDA_RED;}
		$r_cadastro['str_enem_inscricao'] = $_POST['str_enem_inscricao']; if($r_cadastro['str_enem_inscricao'] == NULL){ $validacao = FALSE; $str_enem_inscricao= BORDA_RED;}
	}
	
	// Outra faculadde	
	$r_cadastro['ch_comunicacao_outrafaculdade'] 	= $_POST['ch_comunicacao_outrafaculdade'];
	
	if($r_cadastro['ch_comunicacao_outrafaculdade'] == 'N'){
		$r_cadastro['str_comunicacao_qualcurso'] 	= NULL;
		$r_cadastro['str_comunicacao_quaislocais'] 	= NULL;
	}
	else{
		$r_cadastro['str_comunicacao_qualcurso'] = $_POST['str_comunicacao_qualcurso']; if($r_cadastro['str_comunicacao_qualcurso'] == NULL){ $validacao = FALSE; $str_comunicacao_qualcurso = BORDA_RED;}
		$r_cadastro['str_comunicacao_quaislocais'] = $_POST['str_comunicacao_quaislocais']; if($r_cadastro['str_comunicacao_quaislocais'] == NULL){ $validacao = FALSE; $str_comunicacao_quaislocais = BORDA_RED;}
	}
	
	
	// Necessidades especiais
	$r_cadastro['ch_necessidade_especial']  = $_POST['ch_necessidade_especial'];
	if($r_cadastro['ch_necessidade_especial'] == 'N')
		$r_cadastro['str_necessidade_especial'] = null;
	else{
		$r_cadastro['str_necessidade_especial'] = $_POST['str_necessidade_especial']; if($r_cadastro['str_necessidade_especial'] == NULL){ $validacao = FALSE; $str_necessidade_especial= BORDA_RED;}
	}
		
	
	
	// Valido instituição Ensino Médio
	$r_cadastro['str_ensinomedio_local']     = $_POST['str_ensinomedio_local']; if($r_cadastro['str_ensinomedio_local'] == NULL){ $validacao = FALSE; $str_ensinomedio_local = BORDA_RED;}
	$r_cadastro['str_ensinomedio_conclusao'] = $_POST['str_ensinomedio_conclusao']; if($r_cadastro['str_ensinomedio_conclusao'] == NULL){ $validacao = FALSE; $str_ensinomedio_conclusao = BORDA_RED;}
	
	// Canhoto
	$r_cadastro['ch_necessidade_canhoto'] = $_POST['ch_necessidade_canhoto'];
	
	// Como ficou sabendo
	$r_cadastro['str_comunicacao_ficousabendo'] = $_POST['str_comunicacao_ficousabendo']; if($r_cadastro['str_comunicacao_ficousabendo'] == '(Selecione)'){ $validacao = FALSE; $str_comunicacao_ficousabendo = BORDA_RED;}
	
	// Valido Senha
	if ($r_cadastro['str_sistema_senha'] != $r_cadastro['confir_str_sistema_senha']){
	
		$r_cadastro['str_sistema_senha']        = NULL;
		$r_cadastro['confir_str_sistema_senha'] = NULL;
		
		$str_sistema_senha 		  = BORDA_RED;
		$confir_str_sistema_senha = BORDA_RED;
		
		$msg_senha = '* As senhas não coincidem.';
		
		$validacao = FALSE;
	
	}
	
	// valido o Data
	if (!validaData($r_cadastro['dt_pessoa_nascimento'])){
		
		$dt_pessoa_nascimento 	= BORDA_RED; 
		$msg_data = '* Informe a data Corretamente dd/mm/aaaa.';
		$validacao = FALSE; 
	
	}
	
	// valido o email
	if(!validaEmail($r_cadastro['str_pessoa_email'] )){
	
		$str_pessoa_email = BORDA_RED;
		$msg_email = '* O e-mail deve ser válido.';
		
		$validacao = FALSE;
	
	}

	
	// A Validação esta ok, insiro no banco as informações
	if ($validacao){

	
		// Se eu estiver alterando não atualizo a senha
		if($r_etapa[1]['permissao'])
			$r_cadastro['str_sistema_senha'] = $_POST['str_sistema_senha']; 			
		else
			$r_cadastro['str_sistema_senha'] = sha1($_POST['str_sistema_senha']); 			

		//formato a data de nascimento para aaaa-mm-dd
		$r_cadastro['dt_pessoa_nascimento'] = formatarData($r_cadastro['dt_pessoa_nascimento'], 'DB');
			
	
		//Salvo as Informações no Banco da pessoa fisica
		
		$query = "	UPDATE 
		
						".DB_PREFIXO."PessoasFisicas
						
					SET
						 str_sistema_senha 			= '".$r_cadastro['str_sistema_senha']."'
						,str_telefone_residencial 	= '".$r_cadastro['str_telefone_residencial']."'
						,str_telefone_comercial 	= '".$r_cadastro['str_telefone_comercial']."'
						,str_telefone_celular 		= '".$r_cadastro['str_telefone_celular']."'
						,str_pessoa_nome 			= '".utf8_decode($r_cadastro['str_pessoa_nome'])."'
						,str_pessoa_rgrne 			= '".$r_cadastro['str_pessoa_rgrne']."'
						,str_pessoa_ssp 			= '".$r_cadastro['str_pessoa_ssp']."'
						,str_pessoa_email 			= '".$r_cadastro['str_pessoa_email']."'
						,str_pessoa_naturalde 		= '".utf8_decode($r_cadastro['str_pessoa_naturalde'])."'
						,str_endereco_cep 			= '".$r_cadastro['str_endereco_cep']."'
						,str_endereco_logradouro 	= '".utf8_decode($r_cadastro['str_endereco_logradouro'])."'
						,str_endereco_numero 		= '".$r_cadastro['str_endereco_numero']."'
						,str_endereco_complemento 	= '".utf8_decode($r_cadastro['str_endereco_complemento'])."'
						,str_endereco_bairro 		= '".utf8_decode($r_cadastro['str_endereco_bairro'])."'
						,str_endereco_cidade 		= '".utf8_decode($r_cadastro['str_endereco_cidade'])."'
						,str_endereco_estado 		= '".$r_cadastro['str_endereco_estado']."'
						,str_endereco_pais 			= '".utf8_decode($r_cadastro['str_endereco_pais'])."'
						,dt_pessoa_nascimento 		= '".$r_cadastro['dt_pessoa_nascimento']."'
						,ch_comunicacao_sms 		= '".$r_cadastro['ch_comunicacao_sms']."'
						,ch_pessoa_sexo 			= '".$r_cadastro['ch_pessoa_sexo']."'
						
					WHERE
					
						id_pessoafisica = '".$r_form['id_pessoafisica']."'
				";
		
					
		$resultado = mysql_query($query, $conectar); 
		
		// salvo as informações no banco da inscrição	
		$query = "	UPDATE 
		
						".DB_PREFIXO."Inscricoes
						
					SET
						 ch_enem						= '{$r_cadastro['ch_enem']}'
						,str_enem_ano 					= '{$r_cadastro['str_enem_ano']}'
						,str_enem_inscricao 			= '{$r_cadastro['str_enem_inscricao']}'
						
						,str_ensinomedio_local 			= '".utf8_decode($r_cadastro['str_ensinomedio_local'])."'
						,str_ensinomedio_conclusao 		= '".utf8_decode($r_cadastro['str_ensinomedio_conclusao'])."'
						
						,ch_necessidade_especial 		= '{$r_cadastro['ch_necessidade_especial']}'
						,str_necessidade_especial 		= '".utf8_decode($r_cadastro['str_necessidade_especial'])."'
						
						,ch_necessidade_canhoto 		= '{$r_cadastro['ch_necessidade_canhoto']}'
						
						,ch_comunicacao_outrafaculdade 	= '{$r_cadastro['ch_comunicacao_outrafaculdade']}'
						,str_comunicacao_qualcurso 		= '".utf8_decode($r_cadastro['str_comunicacao_qualcurso'])."'
						,str_comunicacao_quaislocais 	= '".utf8_decode($r_cadastro['str_comunicacao_quaislocais'])."'
						
						,str_comunicacao_ficousabendo 	= '".utf8_decode($r_cadastro['str_comunicacao_ficousabendo'])."'
					
						
					WHERE
					
						id_inscricao = '".$r_form['id_inscricao']."'
				";
		
		$resultado = mysql_query($query, $conectar); 

		
		if ($resultado){
		
			// Se a etapa 1 ainda não foi concluída, então atualizo para que a 1 já
			// tenha sido concluída.
			if ($r_form['int_etapa'] < 1){
				
				$query = "UPDATE ".DB_PREFIXO."Inscricoes SET  int_etapa =  '1' WHERE  id_inscricao = '".$r_form["id_inscricao"]."' ;";
				mysql_query($query, $conectar);
				
				// atualizo int_etapa
				$r_form['int_etapa'] = 1;
			}
			
				
			// Atualizo o nome no formulario
			$r_form = array(		
				'id_curso' 			=> $r_form['id_curso'],
				'id_pessoafisica' 	=> $r_form['id_pessoafisica'],
				'str_pessoa_nome' 	=> utf8_decode($r_cadastro['str_pessoa_nome']),
				'str_pessoa_cpf' 	=> $r_form['str_pessoa_cpf'],
				'num_inscricao' 	=> $r_form['num_inscricao'],
				'id_inscricao' 		=> $r_form['id_inscricao'],
				'int_etapa' 		=> $r_form['int_etapa']
			);
			
			// Atualizo a etapa
			
				// Etapa atual fica LIBERADA com UPDATE
				$r_etapa[1]['status']    = 2;
				$r_etapa[1]['permissao'] = 1;
				
				// Verifico para onde vou direcionar, pois pode ser para a próxima
				// etapa, ou se o usuário esta vindo de outra etapa, eu direcionado
				// para voltar de onde ele veio
				if(isset($_POST['goto'])){
				
					// Proxima etapa
					$r_etapa['atual'] = $_POST['goto'];

					$r_etapa[$_POST['goto']]['status'] = 1;
				
				}
				else{
				
					// Proxima etapa
					$r_etapa['atual'] = 2;

					$r_etapa[2]['status'] 	 = 1;
				
				}

			// Atualizo as sessions
			$_SESSION['form']  = $r_form;
			$_SESSION['etapa'] = $r_etapa;


		}
		
	
	}

	
}
else{

	

	// Verifico se nessa etapa esta com permissao de UPDATE
	// Se tiver recupero as informacoes
	if ($r_etapa[1]['permissao']){
	
	
		$query = "SELECT * FROM ".DB_PREFIXO."PessoasFisicas WHERE id_pessoafisica = '".$r_form["id_pessoafisica"]."' ;";
		
		$resultado = mysql_query($query, $conectar);
		
		$query = "SELECT * FROM ".DB_PREFIXO."Inscricoes WHERE id_inscricao = '".$r_form["id_inscricao"]."' ;";
		
		$resultado2 = mysql_query($query, $conectar);
		
		if ($resultado && $resultado2){
		
			$linha  = mysql_fetch_object($resultado);
			
			$linha2 = mysql_fetch_object($resultado2);
			
			$r_cadastro['str_sistema_senha'] 		= $linha->str_sistema_senha ;
			$r_cadastro['confir_str_sistema_senha'] = $linha->str_sistema_senha ;
			$r_cadastro['str_telefone_residencial'] = $linha->str_telefone_residencial ;
			$r_cadastro['str_telefone_comercial'] 	= $linha->str_telefone_comercial ;
			$r_cadastro['str_telefone_celular'] 	= $linha->str_telefone_celular ;
			$r_cadastro['str_pessoa_nome'] 			= utf8_encode($linha->str_pessoa_nome) ;
			$r_cadastro['str_pessoa_rgrne'] 		= $linha->str_pessoa_rgrne ;
			$r_cadastro['str_pessoa_ssp'] 			= $linha->str_pessoa_ssp ;
			$r_cadastro['str_pessoa_email'] 		= $linha->str_pessoa_email ;
			$r_cadastro['str_pessoa_naturalde'] 	= utf8_encode($linha->str_pessoa_naturalde) ;
			$r_cadastro['str_endereco_cep'] 		= $linha->str_endereco_cep ;
			$r_cadastro['str_endereco_logradouro'] 	= utf8_encode($linha->str_endereco_logradouro) ;
			$r_cadastro['str_endereco_numero'] 		= $linha->str_endereco_numero ;
			$r_cadastro['str_endereco_complemento'] = utf8_encode($linha->str_endereco_complemento) ;
			$r_cadastro['str_endereco_bairro'] 		= utf8_encode($linha->str_endereco_bairro) ;
			$r_cadastro['str_endereco_cidade'] 		= utf8_encode($linha->str_endereco_cidade) ;
			$r_cadastro['str_endereco_estado'] 		= $linha->str_endereco_estado ;
			$r_cadastro['str_endereco_pais'] 		= utf8_encode($linha->str_endereco_pais) ;
			$r_cadastro['dt_pessoa_registro'] 		= $linha->dt_pessoa_registro ;
			$r_cadastro['dt_pessoa_nascimento'] 	= formatarData($linha->dt_pessoa_nascimento, '');
			$r_cadastro['ch_comunicacao_sms']		= $linha->ch_comunicacao_sms ;
			$r_cadastro['ch_pessoa_sexo'] 			= $linha->ch_pessoa_sexo ;
			
			// Informações da inscrição
			$r_cadastro['ch_enem'] 					= (!$linha2->ch_enem)?'N':$linha2->ch_enem;
			$r_cadastro['str_enem_ano'] 			= $linha2->str_enem_ano ;
			$r_cadastro['str_enem_inscricao'] 	    = $linha2->str_enem_inscricao;
			
			$r_cadastro['str_ensinomedio_local'] 	 = utf8_encode($linha2->str_ensinomedio_local) ;
			$r_cadastro['str_ensinomedio_conclusao'] = utf8_encode($linha2->str_ensinomedio_conclusao) ;
			
			$r_cadastro['ch_necessidade_canhoto']    = (!$linha2->ch_necessidade_canhoto)?'N':$linha2->ch_necessidade_canhoto;
			
			$r_cadastro['str_comunicacao_ficousabendo'] = utf8_encode($linha2->str_comunicacao_ficousabendo) ;
			
			$r_cadastro['ch_necessidade_especial']  = (!$linha2->ch_necessidade_especial)?'N':$linha2->ch_necessidade_especial;
			$r_cadastro['str_necessidade_especial'] = utf8_encode($linha2->str_necessidade_especial);
			
			$r_cadastro['ch_comunicacao_outrafaculdade']  = (!$linha2->ch_comunicacao_outrafaculdade)?'N':$linha2->ch_comunicacao_outrafaculdade;
			$r_cadastro['str_comunicacao_qualcurso'] = utf8_encode($linha2->str_comunicacao_qualcurso);
			$r_cadastro['str_comunicacao_quaislocais'] = utf8_encode($linha2->str_comunicacao_quaislocais);
			
		}
	}
	else{
		
		// Informações do formulário
		$r_cadastro['str_sistema_senha'] 		= NULL;
		$r_cadastro['confir_str_sistema_senha'] = NULL;
		$r_cadastro['str_telefone_residencial'] = NULL;
		$r_cadastro['str_telefone_comercial'] 	= NULL;
		$r_cadastro['str_telefone_celular'] 	= NULL;
		$r_cadastro['str_pessoa_nome'] 			= NULL;
		$r_cadastro['str_pessoa_rgrne'] 		= NULL;
		$r_cadastro['str_pessoa_ssp'] 			= NULL;
		$r_cadastro['str_pessoa_email'] 		= NULL;
		$r_cadastro['str_pessoa_naturalde'] 	= NULL;
		$r_cadastro['str_endereco_cep'] 		= NULL;
		$r_cadastro['str_endereco_logradouro'] 	= NULL;
		$r_cadastro['str_endereco_numero'] 		= NULL;
		$r_cadastro['str_endereco_complemento'] = NULL;
		$r_cadastro['str_endereco_bairro'] 		= NULL;
		$r_cadastro['str_endereco_cidade'] 		= NULL;
		$r_cadastro['str_endereco_estado'] 		= NULL;
		$r_cadastro['str_endereco_pais'] 		= 'Brasil';
		//$r_cadastro['dt_pessoa_registro'] 		= NULL;
		$r_cadastro['dt_pessoa_nascimento'] 	= NULL;
		$r_cadastro['ch_comunicacao_sms']		= 'N';
		$r_cadastro['ch_pessoa_sexo'] 			= NULL;
		
		// Informações da inscrição
		$r_cadastro['ch_enem'] 					= 'N';
		$r_cadastro['str_enem_ano'] 			= NULL;
		$r_cadastro['str_enem_inscricao'] 	    = NULL;
		
		$r_cadastro['str_ensinomedio_local'] 	 = NULL;
		$r_cadastro['str_ensinomedio_conclusao'] = NULL;
		
		$r_cadastro['ch_necessidade_canhoto']    = 'N';
		
		$r_cadastro['str_comunicacao_ficousabendo'] = NULL;
		
		$r_cadastro['ch_necessidade_especial']  = 'N';
		$r_cadastro['str_necessidade_especial'] = NULL;
		
		$r_cadastro['ch_comunicacao_outrafaculdade']  = 'N';
		$r_cadastro['str_comunicacao_qualcurso'] = NULL;
		$r_cadastro['str_comunicacao_quaislocais'] = NULL;
		
		
		
	
	}


}

?>
