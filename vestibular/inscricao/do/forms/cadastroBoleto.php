<br/>
<?php
// Incluo a biblioteca para envio de email
require_once('../../lib/PHPMailer/class.phpmailer.php');
	
	
	
$permissao = $r_etapa[$r_etapa['atual']]['permissao'];

// Se a permissao par a etapa atual for negada, significa
// que ianda não salvei os dados do boleto no banco para
// essa inscricao. 	

if(!$permissao){

	
	// VALOR DO BOLETO
	$boleto_valor = $r_cursos[$r_form['id_curso']]['valor'];

	// DATA EM QUE ESTA SENDO GERADO
	$boleto_gerado_em = date('Y-m-d');
	
	// HORA EM QUE ESTA SENDO GERADO
    $boleto_gerado_as = date('H:i:s');
	
	// NUMERO DA INSCRICAO
    $boleto_inscricao = $r_form['num_inscricao'];
	
	// DIA QUE IRA VENCER
    $boleto_vencimento = date('Y-m-d', strtotime("+" . $num_vencimentopadrao . " day"));
	
	if((strtotime((date('Y-m-d H:i:s', strtotime("+" . $num_vencimentopadrao . " day"))))) >= (strtotime($dt_vencimento_maximo))){
		$boleto_vencimento = $dt_vencimento_maximo;
	} else {
		$boleto_vencimento = date("Y-m-d H:i:s", strtotime("+".$num_vencimentopadrao."day"));
	}
	
	
	// ESTRUTURO OS BLOCOS DO BOLETO
	$bloco_1 = substr(md5($boleto_vencimento), 6, 17);
    $bloco_2 = substr(sha1($boleto_inscricao), 9, 18);
    $bloco_3 = substr(md5($boleto_valor), 13, 6);
    $bloco_4 = substr(sha1($boleto_gerado_em), 27, 2);
    $bloco_5 = substr(md5($boleto_gerado_as), 9, 7);

	// ESTRUTURO O CODIGO DO BOLETO
    $boleto_codigo = $bloco_2 . $bloco_4 . $bloco_1 . $bloco_5 . $bloco_3;
	
	
	// SALVO NO BANCO AS INFORMAÇÕES
	
		// Monto a query
		$query = "INSERT INTO ".DB_PREFIXO."Boletos (
							  id_processo
							, id_inscricao
							, num_via
							, val_valor
							, str_codigodoboleto
							, dt_vencimento
							, dt_gerado_em
							, dt_ultimavia
							
						) VALUES (
						
							  '" . $id_processo . "'
							, '" . $boleto_inscricao . "'
							, 1
							, '" . $boleto_valor . "'
							, '" . $boleto_codigo . "'
							, '" . $boleto_vencimento . "'
							, now()
							, now()
						);";
						
		// Executo a query
		mysql_query($query, $conectar);
		
		// Recupero o ID do Boleto Gerado
		$id_boleto = mysql_insert_id();
		
		
		
	// ATUALIZO O PROCESSO DE TRANFERENCIA PARA STATUS 4.
	// Significa que a pessoa já concluiu todo processo
	
		// Monto e executo a query
		$query = "UPDATE ".DB_PREFIXO."Inscricoes SET  int_etapa =  '4' WHERE  id_inscricao = '".$r_form["id_inscricao"]."' ;";
		mysql_query($query, $conectar);
					
		// Atualizo int_etapa
		$r_form['int_etapa'] = 4;
		
		// Atualizo a permissão da etapa atual
		$r_etapa[4]['permissao'] = 1;
		
		// Atualizo as sessions
		$_SESSION['form']  = $r_form;
		$_SESSION['etapa'] = $r_etapa;
		
		
		
	// A PARTIR DESSE PONTO IREI EXIBIR A MENSAGEM PARA
	// O INSCRITO.
		
		// GERO O LINK DO BOLETO
		
		$boleto_link = URL.'/boleto/vestibular-boleto.php?inscricao=';
		
		$boleto_link .= implode(',', array(substr($bloco_2, 3, 7), $id_processo, $r_form['id_inscricao'], substr($bloco_3 . $bloco_5, 3, 7), $id_boleto));
		$boleto_link = '<a href="' . $boleto_link . '" target="_blank">' . $boleto_link . '</a>';
	
	
		// RECUPERO AS INFORMAÇÕES DO PROCESSO ATUAL
		
		$query ="SELECT txt_mensagem_email_corpo, txt_mensagem_inscricao_agradecimento, str_responsavel_nome, str_responsavel_email, txt_mensagem_email_copias, str_mensagem_email_assunto FROM ".DB_PREFIXO."Processos WHERE ch_situacao = 'A' LIMIT 1 ";			
		$resultado = mysql_query($query, $conectar); 
		$processo = mysql_fetch_object($resultado);
		
		$msg_email         		= $processo->txt_mensagem_email_corpo;
		$msg_agradecimento 		= $processo->txt_mensagem_inscricao_agradecimento;
		$str_responsavel_nome 	= $processo->str_responsavel_nome;
		$str_responsavel_email 	= $processo->str_responsavel_email;
		$email_assunto 			= $processo->str_mensagem_email_assunto;
		
		// RECUPERO AS INFORMAÇÕES DO USUÁRIO
		
		$query ="SELECT str_pessoa_email FROM ".DB_PREFIXO."PessoasFisicas WHERE id_pessoafisica = '".$r_form['id_pessoafisica']."' LIMIT 1 ";			
		$resultado = mysql_query($query, $conectar); 
		$linha = mysql_fetch_object($resultado);
		
		$str_pessoa_email = $linha->str_pessoa_email;
		
		
		// PRE DEFINO O QUE SERA SUBSTITUIDO NOS TEXTOS PADROES
				
		$substituicoes = array(
			'{nome-do-inscrito}'     => utf8_encode($r_form['str_pessoa_nome']),
			'{email-do-inscrito}'    => $str_pessoa_email,
			'{numero-da-inscricao}'	 => $r_form['num_inscricao'],
			'{link-do-boleto}'		 => $boleto_link
		);
		
		// SUSTITUO O QUE FOI PRE DEFINIDO
		
		foreach ($substituicoes as $procurar=>$substituir) {
		
			$msg_agradecimento 	= str_ireplace($procurar, $substituir, $msg_agradecimento);
			$msg_email 			= str_ireplace($procurar, $substituir, $msg_email);
			$email_assunto 		= str_ireplace($procurar, $substituir, $email_assunto);
			
		}
		
		// IMPRIMO A MENSAGEM DE AGRADESCIMENTO
		
		echo '<div id="msg_agradecimento">',$msg_agradecimento,'<p align="center"><a href="">FINALIZAR O PROCESSO</a></p></div>';


		// ENVIO UM EMAIL PARA USUÁRIO COM AS INSTRUÇÕES PRE DEFINIDAS NO BANCO
		
		// try {

		// 	$_mail = new PHPMailer(true);
		// 	$_mail->CharSet = 'utf8';
		// 	$_mail->IsHTML(true);
		// 	$_mail->IsSMTP();
		// 	$_mail->SMTPAuth = true;
		// 	$_mail->Host = "smtp.idpsp.edu.br";
		// 	$_mail->Port = 587;
		// 	$_mail->WordWrap = 50;

		
		// 	$_mail->Username = 'vestibular@idpsp.edu.br';
		// 	$_mail->Password = 'vestIUbul@r3';
			
		// 	/* Remetente */
		// 	$_mail->From 	 = $str_responsavel_email;
		// 	$_mail->FromName = $str_responsavel_nome;
			
		// 	/* Destinatário */
		// 	$_mail->AddAddress($str_pessoa_email, $r_form['str_pessoa_nome']);
			
		// 	/* Copias */
		// 	$_mail->AddBCC($str_responsavel_email, $str_responsavel_nome);

		// 	if (strlen(trim($processo->txt_mensagem_email_copias))) {
		// 		if (strpos($processo->txt_mensagem_email_copias, ',') === false) {
		// 			$_mail->AddBCC($processo->txt_mensagem_email_copias);
		// 		} else {
		// 			$txt_mensagem_email_copias = explode(',', $processo->txt_mensagem_email_copias);
		// 			foreach ($txt_mensagem_email_copias as $txt_mensagem_email_copia) {
		// 				$_mail->AddBCC($txt_mensagem_email_copia);
		// 			}
		// 		}
		// 	}
			
		// 	//$_mail->AddBCC('desenvolvimento@slmandic.edu.br', utf8_decode('Desenvolvimento SLMandic'));

		// 	$_mail->Subject = $email_assunto;
		// 	$_mail->MsgHTML(utf8_decode($msg_email));
			
		// 	$_mail->Send(); 
		// } 
		// catch (phpmailerException $e) {
		// 	$e->errorMessage();
		// }		


}

// Já salvei os dados, então somente recupero as informações.
else{

	?>
	<div id="msg_agradecimento2">
		
		<center>
		<p>Parabéns, <?=utf8_encode($r_form['str_pessoa_nome'])?>, <br/>você concluiu o seu cadastro.</p>
		
		<p><a href="sair.php">Clique aqui e acesse a Área do Inscrito.</a></p>
		
		<p>Obrigado!</p>
	
		</center>

	</div>
	
	<!--<script>$("#msg_agradecimento2").show("slow");</script>-->
	<?php

}
	
	
?>




