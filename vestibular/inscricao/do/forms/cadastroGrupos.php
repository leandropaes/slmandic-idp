<div id="selecionarGrupo">

	<div class="GrupoCabecalho">
		Seja Bem-vindo <?=utf8_encode($r_form['str_pessoa_nome'])?>!
	</div>
	
	<div class="GrupoMensagem">
		O processo de transferência para ingresso no curso de graduação em <?=$r_cursos[$r_form['id_curso']]['nome']?> do IDP/SP
		será permitido exclusivamente para candidatos que se enquadrem em uma das categorias abaixo:
	</div>
	
	<div class="GrupoLinha"></div>
	
	<form id="formGrupos" action="javascript:func()" method="POST">
	
		<input type="hidden" id="id_inscricao" name="id_inscricao" value="<?=$r_form['id_inscricao']?>" />
	
		<table border="0">
		<?php 
			while($linha = mysql_fetch_object($resultado)){	
				
				$id_grupo 		= $linha->id_grupo;
				$str_nome 		= utf8_encode($linha->str_nome);
				$str_descricao 	= utf8_encode($linha->str_descricao);
				
				echo"
				
				<tr>
					<td width='25px'><INPUT TYPE='RADIO'  ID='{$id_grupo}' NAME='id_grupo' VALUE='{$id_grupo}'></td>
					<td width='80px'><label for='{$id_grupo}'>{$str_nome}</label></td>
					<td class='GrupoDescricao'><label for='{$id_grupo}'>{$str_descricao}</label></td>
				</tr>
			
				";
			}
		?>
		</table>
	
		<br/>
		
		<div id="msg_grupo"><b>Alerta:</b> Selecione um grupo.</div>

		<center><input type="submit" style="width: 300px" value="Li, concordo e aceito os termos acima." /></center>
		<br/>
		
		
		<div id="carregando_grupos"><br/><center><img src="images/carregando.gif" /></center></div>

	</form>

	<script type="text/javascript" src="do/script/validaFormGrupos.js"></script>

</div>

<div id="selecionarGrupo_sair">
<a href="sair.php" style="text-decoration:none;"><div id="sair">Sair</div></a>
</div>
					
					