<div id="questionarioSocio">
	
	<!-- Mensagem de Erro -->
	<div id="msg_questionario"><b>Alerta:</b> É obrigatório o preenchimento de todos os campos.</div>
	<?php if(!$eValido){?><script>$("#msg_questionario").show("slow");</script><?php } ?>

	<!-- Formulário -->
	<form action="index.php" method="POST">
				
		<?php 				
		foreach($vestibular as $key => $row){ ?>
			<br/>
			<center>
			
			<table border="0">
				
				<!-- TITULO DA PERGUNTA -->
				<tr>
					<td colspan="2" ><b><?= $key; ?></b></td>
				</tr>
				
				<?php
				foreach($row as $key_2 => $row_2){
					
					// PERGUNTAS TEXTAREA
					if($row_2['tipo'] == 'T'){
						if (($validacao[$row_2['id']]['msg']) and (!$validacao[$row_2['id']]['msgExibida'])){ $validacao[$row_2['id']]['msgExibida'] = TRUE; echo $validacao[$row_2['id']]['msg']; }
					?>
						<tr>
							<td colspan="2" ><textarea rows="4" cols="50" name="pergunta[<?= $row_2['id'] ?>]" id="nome" /><?=$validacao[$row_2['id']]['valor']?></textarea><?= $key_2; ?></td>
						
						</tr>
					<?php
					
					// PERGUNTAS DATA
					}else if($row_2['tipo'] == 'D'){
						if (($validacao[$row_2['id']]['msg']) and (!$validacao[$row_2['id']]['msgExibida'])){ $validacao[$row_2['id']]['msgExibida'] = TRUE; echo $validacao[$row_2['id']]['msg']; }
					?>
						<tr class="alternativa">
							<td colspan="2" ><input type="text" name="pergunta[<?= $row_2['id'] ?>]" id="nome" class="data" data-mask="00/00/0000" value="<?=$validacao[$row_2['id']]['valor']?>" /><?= $key_2; ?></td>
						
						</tr>
					<?php
					
					// PERGUNTAS RADIO
					}else if($row_2['tipo'] == 'R'){
						if (($validacao[$row_2['id']]['msg']) and (!$validacao[$row_2['id']]['msgExibida'])){ $validacao[$row_2['id']]['msgExibida'] = TRUE; echo $validacao[$row_2['id']]['msg']; }
					
						($validacao[$row_2['id']]['valor'] == $row_2['value'])? $checked = 'CHECKED' : $checked = NULL; 
					?>
						<tr class="alternativa">
							<td width="5%" ><input class="r" type="radio" name="pergunta[<?= $row_2['id'] ?>]" id="radio" value="<?= $row_2['value'] ?>" <?=$checked?> /></td>
							<td><?= $key_2; ?></label></td>
						</tr>
					<?php
					
					// PERGUNTAS RADIO (OUTRO)
					}else if($row_2['tipo'] == 'RS'){
					
						if (($validacao[$row_2['id']]['msg']) and (!$validacao[$row_2['id']]['msgExibida'])){ $validacao[$row_2['id']]['msgExibida'] = TRUE; echo $validacao[$row_2['id']]['msg']; }
						
						
						if((is_numeric($validacao[$row_2['id']]['valor'])) || ($validacao[$row_2['id']]['valor'] == NULL)){

							($validacao[$row_2['id']]['valor'] == $row_2['value'])? $checked = 'CHECKED' : $checked = NULL; ?>

							<tr class="alternativa">
								<td width="5%"><input class="rs" add="n" tname="pergunta[<?= $row_2['id'] ?>]" type="radio" name="pergunta[<?= $row_2['id'] ?>]" id="radio" value="<?= $row_2['value'] ?>" <?=$checked?> /></td>
								<td><?= $key_2; ?></td>
							</tr>
						
						<?php
						}
						else{?>

							<tr class="alternativa">
								<td width="5%"><input class="rs" add="s" tname="pergunta[<?= $row_2['id'] ?>]" type="radio" name="pergunta[<?= $row_2['id'] ?>]" id="radio" value="<?= $row_2['value'] ?>" CHECKED /></td>
								<td><?= $key_2; ?></td>
							</tr>
							
							<tr class='remove'><td></td><td><input style='width: 350px;' name="pergunta[<?= $row_2['id'] ?>]" type='text' value="<?=$validacao[$row_2['id']]['valor']?>" /></td></tr>
						
						<?php
						}
					
					}
				}
				?>
			</table>
			
			</center>
			<br/>
			
			<?php
		}// fim do foreach
		?>

		<input type="hidden" name="salvarQuestionario" id="salvarQuestionario" value="salvarQuestionario" />	
		
		<center><input type="submit" name="" id="" value="Salvar Questionário" /></center>

	</form>

</div>


					
					