<div id="questionario">

<div id="msg_cadastroPessoaFisica"><b>Alerta:</b> Os Campos em Vermelho são de preenchimento obrigatório.</div>

<?php
if(!$validacao){
	?><script>$("#msg_cadastroPessoaFisica").show("slow");</script><?php
}
?>

<center>

<form action="index.php<?php if(isset($goto_url)) echo $goto_url;?>" method="POST">
	
	<?php 
	if (isset($goto)){echo "<INPUT TYPE='hidden' NAME='goto' VALUE='{$goto}'>";}
	?>
	
	<INPUT TYPE="hidden" NAME="salvarPessoaFisica" VALUE="salvarPessoaFisica">
	
	<table border="0">
		
		<!-- str_pessoa_cpf -->
		<tr>
			<td width="200px"><label for="str_pessoa_cpf">CPF:</label></td>
			<td ><input type="text" name="str_pessoa_cpf" id="str_pessoa_cpf" value="<?=$r_form['str_pessoa_cpf']?>"  <?=$str_pessoa_cpf?> disabled /></td>
		</tr>
		
		<?php 
		// exibo senha somente se for o primeiro cadastro
		if($r_etapa[1]['permissao']){
		
			?>
			<input type="hidden" name="str_sistema_senha" id="str_sistema_senha" value="<?=$r_cadastro['str_sistema_senha']?>" <?=$str_sistema_senha?> />
			<input type="hidden" name="confir_str_sistema_senha" id="confir_str_sistema_senha" value="<?=$r_cadastro['confir_str_sistema_senha']?>" <?=$confir_str_sistema_senha?> />
			<?php
		
		}
		else{
		?>
		
		<!-- str_sistema_senha -->
		<tr>
			<td><label for="str_sistema_senha">Senha de acesso:</label></td>
			<td><input type="password" name="str_sistema_senha" id="str_sistema_senha" value="<?=$r_cadastro['str_sistema_senha']?>" <?=$str_sistema_senha?> /> <?php if($msg_senha) echo ' <span class="campo_erro">',$msg_senha,'</span>';?></td>
		</tr>

		<!-- confirma str_sistema_senha -->
		<tr>
			<td><label for="confir_str_sistema_senha">Confirmação da senha:</label></td>
			<td><input type="password" name="confir_str_sistema_senha" id="confir_str_sistema_senha" value="<?=$r_cadastro['confir_str_sistema_senha']?>" <?=$confir_str_sistema_senha?> /></td>
		</tr>
		
		<?php }?>
		
		<!-- str_pessoa_email -->
		<tr>
			<td><label for="str_pessoa_email">E-mail:</label></td>
			<td><input type="text" name="str_pessoa_email" id="str_pessoa_email" value="<?=$r_cadastro['str_pessoa_email']?>" <?=$str_pessoa_email?>/><?php if($msg_email) echo '  <span class="campo_erro">',$msg_email,'</span>';?></td>
		</tr>
		
		<!-- NULO -->
		<tr class="no-hover"><td colspan="2">&nbsp;</td></tr>
		
		<!-- str_pessoa_nome -->
		<tr>
			<td><label for="str_pessoa_nome">Nome:</label></td>
			<td><input type="text" name="str_pessoa_nome" id="str_pessoa_nome" value="<?=$r_cadastro['str_pessoa_nome']?>" <?=$str_pessoa_nome?>/></td>
		</tr>
		
		<!-- str_pessoa_rgrne -->
		<tr>
			<td><label for="str_pessoa_rgrne">RG:</label></td>
			<td><input  type="text" name="str_pessoa_rgrne" id="str_pessoa_rgrne" value="<?=$r_cadastro['str_pessoa_rgrne']?>" <?=$str_pessoa_rgrne?>/></td>
		</tr>
		
		<!-- str_pessoa_ssp -->
		<tr>
			<td><label for="str_pessoa_ssp">Órgão Expedidor:</label></td>
			<td><input type="text" name="str_pessoa_ssp" id="str_pessoa_ssp" value="<?=$r_cadastro['str_pessoa_ssp']?>" <?=$str_pessoa_ssp?>/></td>
		</tr>
		
		<!-- ch_pessoa_sexo -->
		<tr>
			<td><label for="ch_pessoa_sexo">Sexo:</label></td>
			<td>
				<select name="ch_pessoa_sexo" id="ch_pessoa_sexo" <?=$ch_pessoa_sexo?>>
					<option>(Selecione)</option>
					<?php 
					foreach($r_sexo as $sigla => $sexo){
						($r_cadastro['ch_pessoa_sexo'] == $sigla)? $selected = 'selected' : $selected = NULL;
						echo "<option value='{$sigla}' {$selected}>{$sexo}</option>";
					}
					?>
				</select>
			</td>
		</tr>
		
		<!-- dt_pessoa_nascimento -->
		<tr>
			<td><label for="dt_pessoa_nascimento">Data de nascimento:</label></td>
			<td><input data-mask="00/00/0000" type="text" name="dt_pessoa_nascimento" id="dt_pessoa_nascimento" value="<?=$r_cadastro['dt_pessoa_nascimento']?>" <?=$dt_pessoa_nascimento?>/><?php if($msg_data) echo ' <span class="campo_erro">',$msg_data,'</span>';?></td>
		</tr>

		<!-- str_telefone_residencial -->
		<tr>
			<td><label for="str_telefone_residencial">Telefone residencial:</label></td>
			<td><input data-mask="(00) 0000-0000" type="text" name="str_telefone_residencial" id="str_telefone_residencial" value="<?=$r_cadastro['str_telefone_residencial']?>" <?=$str_telefone_residencial?>/></td>
		</tr>
		
		<!-- str_telefone_comercial -->
		<tr>
			<td><label for="str_telefone_comercial">Telefone comercial:</label></td>
			<td><input data-mask="(00) 0000-0000" type="text" name="str_telefone_comercial" id="str_telefone_comercial" value="<?=$r_cadastro['str_telefone_comercial']?>" <?=$str_telefone_comercial?>/></td>
		</tr>
		
		<!-- str_telefone_celular -->
		<tr>
			<td><label for="str_telefone_celular">Telefone celular:</label></td>
			<td><input  maxlength="15" onkeyup="mascara( this, mtel );" type="text" name="str_telefone_celular" id="str_telefone_celular" value="<?=$r_cadastro['str_telefone_celular']?>" <?=$str_telefone_celular?>/></td>
		</tr>
		
		
		
		<!-- str_pessoa_naturalde -->
		<tr>
			<td><label for="str_pessoa_naturalde">Naturalidade:</label></td>
			<td><input type="text" name="str_pessoa_naturalde" id="str_pessoa_naturalde" value="<?=$r_cadastro['str_pessoa_naturalde']?>" <?=$str_pessoa_naturalde?>/></td>
		</tr>
		
		<!-- str_endereco_cep -->
		<tr>
			<td><label for="str_endereco_cep">CEP:</label></td>
			<td><input type="text" data-mask="00.000-000" name="str_endereco_cep" id="str_endereco_cep" value="<?=$r_cadastro['str_endereco_cep']?>" <?=$str_endereco_cep?>/></td>
		</tr>
		
		<!-- str_endereco_pais -->
		<tr>
			<td><label for="str_endereco_pais">País:</label></td>
			<td><input type="text" name="str_endereco_pais" id="str_endereco_pais" value="<?=$r_cadastro['str_endereco_pais']?>" <?=$str_endereco_pais?>/></td>
		</tr>
		
		<!-- str_endereco_estado -->
		<tr>
			<td><label for="str_endereco_estado">Estado:</label></td>
			<td>
				<select name="str_endereco_estado" id="str_endereco_estado" <?=$str_endereco_estado?>>
					<option>(Selecione)</option>
					<?php 
					foreach($r_estado as $sigla => $estado){
						($r_cadastro['str_endereco_estado'] == $sigla)? $selected = 'selected' : $selected = NULL;
						echo "<option value='{$sigla}' {$selected}>{$sigla}</option>";
					}
					?>
				</select>
			</td>
		</tr>
		
		<!-- str_endereco_cidade -->
		<tr>
			<td><label for="str_endereco_cidade">Cidade:</label></td>
			<td><input type="text" name="str_endereco_cidade" id="str_endereco_cidade" value="<?=$r_cadastro['str_endereco_cidade']?>" <?=$str_endereco_cidade?>/></td>
		</tr>
		
		
		
		<!-- str_endereco_logradouro -->
		<tr>
			<td><label for="str_endereco_logradouro">Endereço:</label></td>
			<td><input type="text" name="str_endereco_logradouro" id="str_endereco_logradouro" value="<?=$r_cadastro['str_endereco_logradouro']?>" <?=$str_endereco_logradouro?>/></td>
		</tr>
		
		<!-- str_endereco_bairro -->
		<tr>
			<td><label for="str_endereco_bairro">Bairro:</label></td>
			<td><input type="text" name="str_endereco_bairro" id="str_endereco_bairro" value="<?=$r_cadastro['str_endereco_bairro']?>" <?=$str_endereco_bairro?>/></td>
		</tr>
		
		<!-- str_endereco_numero -->
		<tr>
			<td><label for="str_endereco_numero">Número:</label></td>
			<td><input type="text" name="str_endereco_numero" id="str_endereco_numero" value="<?=$r_cadastro['str_endereco_numero']?>" <?=$str_endereco_numero?>/></td>
		</tr>
		
		<!-- str_endereco_complemento -->
		<tr >
			<td ><label for="str_endereco_complemento">Complemento:</label></td>
			<td ><input type="text" name="str_endereco_complemento" id="str_endereco_complemento" value="<?=$r_cadastro['str_endereco_complemento']?>" <?=$str_endereco_complemento?>/></td>
		</tr>
		
		

		<!-- dt_pessoa_registro 
		
		<tr>
			<td><label for="dt_pessoa_registro">dt_pessoa_registro</label></td>
			<td><input type="text" name="dt_pessoa_registro" id="dt_pessoa_registro" value="<?=$r_cadastro['dt_pessoa_registro']?>" <?=$dt_pessoa_registro?>/></td>
		</tr>
		
		-->
		
		<!-- ch_comunicacao_sms -->
		<tr>
			<td ><label for="ch_comunicacao_sms">Aceito receber SMS:</label></td>
			<td >
				<select name="ch_comunicacao_sms" id="ch_comunicacao_sms" <?=$ch_comunicacao_sms?>>
					<?php 
					foreach($r_sms as $silga => $sms){
						($r_cadastro['ch_comunicacao_sms'] == $silga)? $selected = 'selected' : $selected = NULL;
						echo "<option value='{$silga}' {$selected}>{$sms}</option>";
					}
					?>
				</select>
				
			</td>
		</tr>
		
		<?php 
		// Se existe opções do enem
		if($r_enem){ 
		
			?>
		
			<tr class="no-hover">
				<td colspan="2"><h2>Enem</h2></td>
			</tr>
			
			
			
			<tr class="no-hover">
				<td colspan="2">
					
					<table>
						<tr class="no-hover">
							<td width="250px">Deseja utilizar sua nota do ENEM?</td>
							<td style="width:30px;"><input type="radio"   style="width:20px;" name="ch_enem" id="ch_enemS" value="S" <?=($r_cadastro['ch_enem']=='S')?'checked':NULL?>  ></td>
							<td style="width:62px;"><label for="ch_enemS" >Sim</label></td>
							<td style="width:30px;"><input type="radio"   style="width:20px;" name="ch_enem" id="ch_enemN" value="N" <?=($r_cadastro['ch_enem']=='N')?'checked':NULL?>  ></td>
							<td style="width:62px;"><label for="ch_enemN" >Não</label></td>
						</tr>
						
						<tr class="no-hover" id="enem_ano" style="<?=($r_cadastro['ch_enem']=='N')?'display: none':NULL?>">
							<td>Ano de Exame:<?=$r_cadastro['ch_enem_ano']?></td>
							<?php 
							
							
							foreach($r_enem as $key => $value){
									
								$checked = ($value == $r_cadastro['str_enem_ano'])?'checked':NULL;
											
								?>
								<td><input type="radio" style="width:20px;" name="str_enem_ano" id="str_enem_ano<?=$key?>" value="<?=$value?>" <?=$checked?> ></td>
								<td><label for="str_enem_ano<?=$key?>" <?=($str_enem_ano)?"style='color:red'":NULL?>><?=$value?></label></td>
								<?php		
									
							}
							?>
						</tr>
						
						<tr class="no-hover" id="enem_inscricao" style="<?=($r_cadastro['ch_enem']=='N')?'display: none':NULL?>">
							<td>Nº de Inscrição:</td>
							<td colspan="4"><input style="width:100%;" type="text" name="str_enem_inscricao" value="<?=$r_cadastro['str_enem_inscricao']?>" <?=$str_enem_inscricao?> ></td>
						</tr>
					</table>
					
					<script>
					$("#ch_enemS").click(function(){
						$("#enem_ano").show();
						$("#enem_inscricao").show();
					});
					$("#ch_enemN").click(function(){
						$("#enem_ano").hide();
						$("#enem_inscricao").hide();
					});
					</script>
					
				</td>
			</tr>
		
			<?php
		}
		else
			echo "<input type='hidden' name='ch_enem' value='N'>";
			
		
		?>
		
		<tr class="no-hover">
			<td colspan="2"><h2>Informações</h2></td>
		</tr>
		
		<tr class="no-hover">
			
			<td colspan="2">
				
				<p>Colégio em que concluiu o ensino médio:
				<br><input type="text" name="str_ensinomedio_local" value="<?=$r_cadastro['str_ensinomedio_local']?>" style="width:100%" <?=$str_ensinomedio_local?> ></p>
				
				<p>Ano de Conclusão:
				<br><input type="text" name="str_ensinomedio_conclusao" value="<?=$r_cadastro['str_ensinomedio_conclusao']?>" style="width:30%" <?=$str_ensinomedio_conclusao?> ></p>
				
			</td>
			
		</tr>
		
		<tr class="no-hover">
			<td colspan="2">
				
				<table style="width: 100%">
					
					<tr class="no-hover">
						<td width="250px">Você é portador de alguma necessidade especial?</td>
						<td style="width:30px;"><input type="radio"   style="width:20px;" name="ch_necessidade_especial" id="ch_neS" value="S" <?=($r_cadastro['ch_necessidade_especial']=='S')?'checked':NULL?> ></td>
						<td style="width:62px;"><label for="ch_neS">Sim</label></td>
						<td style="width:30px;"><input type="radio"   style="width:20px;" name="ch_necessidade_especial" id="ch_neN" value="N" <?=($r_cadastro['ch_necessidade_especial']=='N')?'checked':NULL?> ></td>
						<td style="width:62px;"><label for="ch_neN">Não</label></td>
					</tr>
					
					<tr class="no-hover" style="<?=($r_cadastro['ch_necessidade_especial']=='N')?'display: none':NULL?>" id="necessidade_especial">
						<td colspan="5"><br>Qual(is) necessidade(s)?<br><input type="text" name="str_necessidade_especial" value="<?=$r_cadastro['str_necessidade_especial']?>" <?=$str_necessidade_especial?> style="width: 100%"></td>
					</tr>
					
					<tr class="no-hover"><td>&nbsp;</td></tr>
					
					<tr class="no-hover">
						<td width="250px">Necessita de carteira para canhoto?</td>
						<td style="width:30px;"><input type="radio"   style="width:20px;" name="ch_necessidade_canhoto" id="ch_cS" value="S" <?=($r_cadastro['ch_necessidade_canhoto']=='S')?'checked':NULL?> ></td>
						<td style="width:62px;"><label for="ch_cS">Sim</label></td>
						<td style="width:30px;"><input type="radio"   style="width:20px;" name="ch_necessidade_canhoto" id="ch_cN" value="N" <?=($r_cadastro['ch_necessidade_canhoto']=='N')?'checked':NULL?> ></td>
						<td style="width:62px;"><label for="ch_cN">Não</label></td>
					</tr>
					
					<tr class="no-hover"><td>&nbsp;</td></tr>
					
					<tr class="no-hover">
						<td width="250px">Está prestando vestibular em outra faculdade?</td>
						<td style="width:30px;"><input type="radio"   style="width:20px;" name="ch_comunicacao_outrafaculdade" id="ch_ofS" value="S" <?=($r_cadastro['ch_comunicacao_outrafaculdade']=='S')?'checked':NULL?> ></td>
						<td style="width:62px;"><label for="ch_ofS">Sim</label></td>
						<td style="width:30px;"><input type="radio"   style="width:20px;" name="ch_comunicacao_outrafaculdade" id="ch_ofN" value="N" <?=($r_cadastro['ch_comunicacao_outrafaculdade']=='N')?'checked':NULL?> ></td>
						<td style="width:62px;"><label for="ch_ofN">Não</label></td>
					</tr>
					
					<tr class="no-hover" style="<?=($r_cadastro['ch_comunicacao_outrafaculdade']=='N')?'display: none':NULL?>" id="outrafaculdade1">
						<td colspan="5"><br>Qual(is) curso(s)?<br><input type="text" name="str_comunicacao_qualcurso" value="<?=$r_cadastro['str_comunicacao_qualcurso']?>" <?=$str_comunicacao_qualcurso?> style="width: 100%"></td>
					</tr>
					
					<tr class="no-hover" style="<?=($r_cadastro['ch_comunicacao_outrafaculdade']=='N')?'display: none':NULL?>" id="outrafaculdade2">
						<td colspan="5"><br>Qual(s) faculdade(s)?<br><input type="text" name="str_comunicacao_quaislocais" value="<?=$r_cadastro['str_comunicacao_quaislocais']?>" <?=$str_comunicacao_quaislocais?> style="width: 100%"></td>
					</tr>
					
					<tr class="no-hover"><td>&nbsp;</td></tr>
					
					<tr class="no-hover">
						<td width="250px">Como ficou sabendo do vestibular?</td>
						<td colspan="4">
							<select name="str_comunicacao_ficousabendo" style="width: 100%;" <?=$str_comunicacao_ficousabendo?> >
								<option>(Selecione)</option>
								<?php 
								foreach ($r_ficousabendo as $key => $value) {
										
									$selected = ($r_cadastro['str_comunicacao_ficousabendo'] == $value)?'selected':NULL;
									
									echo "<option value='{$key}' {$selected} >{$value}</option>";
								}
								?>
							</select>
						</td>
					</tr>
				
				</table>
				
				
				<script>
				$("#ch_neS").click(function(){ $("#necessidade_especial").show(); });
				$("#ch_neN").click(function(){ $("#necessidade_especial").hide(); });
				
				$("#ch_ofS").click(function(){ $("#outrafaculdade1").show(); $("#outrafaculdade2").show(); });
				$("#ch_ofN").click(function(){ $("#outrafaculdade1").hide(); $("#outrafaculdade2").hide(); });
				</script>
				
			</td>
		</tr>

	</table>

	<br/><br/>
	<input type="submit" value="Salvar Cadastro">
</form>
</center>


</div>