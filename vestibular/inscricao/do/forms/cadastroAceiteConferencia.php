<?php
	
	// RECUPERO O TEXTO DO TERMO DE RESPONSABILIDADE
	
		$query = "SELECT txt_mensagem_inscricao_termos, str_arquivo_manual FROM ".DB_PREFIXO."Processos WHERE id_processo = ".$id_processo.";";

		$resultado = mysql_query($query, $conectar);
	
		$linha = mysql_fetch_object($resultado);
		
		$txt_mensagem_inscricao_termos = $linha->txt_mensagem_inscricao_termos;
		$str_arquivo_manual = $linha->str_arquivo_manual;
	
	
	
	// RECUPERO AS INFORMAÇÕES DE CADASTRO
	
		$query = "SELECT * FROM ".DB_PREFIXO."PessoasFisicas WHERE id_pessoafisica = ".$r_form['id_pessoafisica'].";";
	
		$resultado = mysql_query($query, $conectar);
	
		$linha = mysql_fetch_object($resultado);
		
		$r_cadastro = array(
			
			'str_pessoa_nome' 		  	=> array(0,'Nome:', 				utf8_encode($linha->str_pessoa_nome)),
			'str_pessoa_cpf' 			=> array(0,'CPF:', 					$linha->str_pessoa_cpf),
			'str_sistema_senha' 		=> array(0,'Senha:', 				$linha->str_sistema_senha),
			'str_pessoa_email'			=> array(1,'E-mail:', 				$linha->str_pessoa_email),
			'dt_pessoa_nascimento' 		=> array(1,'Data de Nascimento:', 	formatarData($linha->dt_pessoa_nascimento, '')),
			'ch_pessoa_sexo' 			=> array(1,'Sexo:', 				(($linha->ch_pessoa_sexo == 'M')? 'Masculino':'Feminino')),
			'str_pessoa_rgrne' 		 	=> array(1,'RG:', 					$linha->str_pessoa_rgrne),
			'str_pessoa_ssp' 			=> array(1,'SSP:', 					strtoupper($linha->str_pessoa_ssp)),
			'str_telefone_residencial' 	=> array(1,'Telefone residencial:', $linha->str_telefone_residencial),
			'str_telefone_comercial'   	=> array(1,'Telefone comercial:', 	$linha->str_telefone_comercial),
			'str_telefone_celular' 	  	=> array(1,'Telefone celular:', 	$linha->str_telefone_celular),			
			'str_pessoa_naturalde' 		=> array(1,'Naturalidade:', 		utf8_encode($linha->str_pessoa_naturalde)),
			'str_endereco_cep' 			=> array(1,'CEP:', 					$linha->str_endereco_cep),
			'str_endereco_pais' 	    => array(1,'Pais:', 				utf8_encode($linha->str_endereco_pais)),
			'str_endereco_estado' 		=> array(1,'Estado:', 				$linha->str_endereco_estado),
			'str_endereco_cidade' 		=> array(1,'Cidade:', 				utf8_encode($linha->str_endereco_cidade)),
			'str_endereco_logradouro' 	=> array(1,'Endereço:', 			utf8_encode($linha->str_endereco_logradouro)),
			'str_endereco_bairro' 		=> array(1,'Bairro:', 				utf8_encode($linha->str_endereco_bairro)),
			'str_endereco_numero' 		=> array(1,'Numero:', 				$linha->str_endereco_numero),
			'str_endereco_complemento'	=> array(1,'Complemento:', 			utf8_encode($linha->str_endereco_complemento)),
			'dt_pessoa_registro' 		=> array(0,'Registro:', 			$linha->dt_pessoa_registro),		
			'ch_comunicacao_sms' 		=> array(1,'Desejo receber SMS?', 	(($linha->ch_comunicacao_sms == 'S')? 'Sim':'Não') )

		);
		
	// 	RECUPERO OS INFORMAÇÕES DO PROCESSO
	
		$query = "SELECT * FROM ".DB_PREFIXO."Inscricoes WHERE id_inscricao = {$_SESSION['form']['id_inscricao']}";
	
		$resultado = mysql_query($query, $conectar);
	
		$linha = mysql_fetch_object($resultado);
	
		
		
		$r_incricao[] = array(1, 'Utilizar sua nota do ENEM?', (($linha->ch_enem == 'S')? 'Sim':'Não') );
		
		if($linha->ch_enem == 'S'){
			
			$r_incricao[] = array(1, 'ENEM Ano:', utf8_encode($linha->str_enem_ano) );
			$r_incricao[] = array(1, 'ENEM Inscrição:', utf8_encode($linha->str_enem_inscricao) );
			
		}

		$r_incricao[] = array(1, '&nbsp;', '&nbsp;');
		
		$r_incricao[] = array(1, 'Portador de alguma necessidade especial?', (($linha->ch_necessidade_especial == 'S')? 'Sim':'Não') );
		
		if($linha->ch_necessidade_especial == 'S'){
			
			$r_incricao[] = array(1, 'Qual(is) necessidade(s)?', utf8_encode($linha->str_necessidade_especial) );
		
		}
		
		$r_incricao[] = array(1, '&nbsp;', '&nbsp;');

		$r_incricao[] = array(1, 'Necessita de carteira para canhoto?', (($linha->ch_necessidade_canhoto == 'S')? 'Sim':'Não') );


?>

<!-- -------------------------------------------------------
  --  CONFIREIR OS DADOS CADASTRADOS
  -- ------------------------------------------------------->
<div id="aceite">
  
<fieldset>

    <legend>&nbsp;Confira seus dados:&nbsp;</legend>
	<center>
	<div id="divConferenciaLeitura">
		<table border="0" class="confira_dados" >
			
			<?php
			foreach($r_cadastro as $chave => $valor){
				if($valor[0]) echo "<tr'><td>{$valor[1]}</td><td>{$valor[2]}</td><tr>";
			}
			?>
			
			<tr>
			
				<?php 
				$link = $r_etapa['atual'].',1';
				$link = base64_encode($link);
				?>

				<td align="right" colspan="2"><button id="btnConferenciaLeitura" class="button" onclick="window.location.href='index.php?do=<?=$link?>'" >Editar</button></td>

			</tr>
		</table>
	</div>
	</center>

</fieldset>

<!-- -------------------------------------------------------
  --  CONFIREIR OS DADOS DO PROCESSO
  -- ------------------------------------------------------->

<br/>

<fieldset>

    <legend>&nbsp;Confira suas informações:&nbsp;</legend>
	<center>
	<div id="divConferenciaLeitura">
		<table border="0" class="confira_dados" >
			
			<?php
			foreach($r_incricao as $chave => $valor){
				if($valor[0]) echo "<tr'><td>{$valor[1]}</td><td>{$valor[2]}</td><tr>";
			}
			?>
			
			<tr>
			
				<?php 
				$link = $r_etapa['atual'].',1';
				$link = base64_encode($link);
				?>

				<td align="right" colspan="2"><button id="btnConferenciaLeitura" class="button" onclick="window.location.href='index.php?do=<?=$link?>'" >Editar</button></td>

			</tr>
		</table>
	</div>
	</center>

</fieldset>

<br/>

<!-- -------------------------------------------------------
  --  TERMO DE RESPONSABILIDADE
  -- ------------------------------------------------------->

<fieldset class="termo">
    <legend>&nbsp;Termo:&nbsp;</legend>
	
	<?= $txt_mensagem_inscricao_termos?>

</fieldset>

<br/>

<!-- -------------------------------------------------------
  --  EDITAL
  -- ------------------------------------------------------->

<fieldset>
    <legend>&nbsp;Edital:&nbsp;</legend>
	
	<center><a href="<?='../arquivos/'.$str_arquivo_manual?>" target="_blank">Clique aqui para visualizar.</a></center>

</fieldset>

<!-- -------------------------------------------------------
  --  ACEITE DOS TERMOS/EDITAL
  -- ------------------------------------------------------->

<div id="declaracao">
	<p>Declaro sob pena da lei, que os dados por mim fornecidos foram conferidos, s&atilde;o verdadeiros e assumo total responsabilidade sobre eles. Declaro ainda que estou ciente que no caso de diverg&ecirc;ncia das informa&ccedil;&otilde;es o formul&aacute;rio estar&aacute; sujeito &agrave; rejei&ccedil;&atilde;o.</p>
	
	<p style="color: red;">Ao clicar no bot&atilde;o abaixo, voc&ecirc; finalizar&aacute; sua inscri&ccedil;&atilde;o no vestibular. Tenha certeza que revisou todas as informa&ccedil;&otilde;es.</p></p>
	<br/>
	
	<form id="formAceite" action="javascript:func()" method="POST">

		<center><input type="submit" value="Li, entendi e aceito todos os Termos do Edital." /></center>
		
		<div id="carregando_aceite"><br/><center><img src="images/carregando.gif" /></center></div>
		<div id="msg_aceite"><b>Alerta:</b> Não foi possível finalizar o processo nesse momento. Tente mais tarde.</div>
	
	</form>
	
	<script type="text/javascript" src="do/script/validaFormAceite.js"></script>

</div>

</div>



