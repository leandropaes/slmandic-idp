
	$("#formRecuperar").submit(function(event) {
		
		// limpo campo mensagem
		$('#msg-inscrito').html("");
		$("#carregando_login").css("display", "none");
		
		// Recupero so valores
		var str_senha       = $("#str_senha").val();	
		var str_senha_c     = $("#str_senha_c").val();	
		var id_pessoafisica = $("#id_pessoafisica").val();	
		
		// Considero que a validação esta OK
		var eValido = true;
		
		// Valido os Campos
		
		if (!str_senha)	 { eValido = false; $("#str_senha").css("border", "1px solid red"); $("#str_senha").focus(); $('#msg-inscrito').html("* Informe as senhas.");} 
		if (!str_senha_c){ eValido = false; $("#str_senha_c").css("border", "1px solid red"); $("#str_senha_c").focus(); $('#msg-inscrito').html("* Informe as senhas.");} 
		
		if ((!eValido) && (!str_senha) && (!str_senha_c)){
			$("#str_senha").focus();
		}
		
		if(str_senha != str_senha_c){
			eValido = false;
			
			$("#str_senha").css("border", "1px solid red");	
			$("#str_senha_c").css("border", "1px solid red");	
			
			$("#str_senha").val('');
			$("#str_senha_c").val('');
			
			$("#str_senha").focus();
			$('#msg-inscrito').html("* As senhas não coincide.");
		
		}
		
		

		// Se for valido então salvo no banco
		if(eValido){
			
			$("#carregando_login").css("display", "inline");
			
			$.post('do/crud/validaRecuperarHash.php', {str_senha: str_senha, id_pessoafisica:id_pessoafisica}, function(resposta) {
					
				if(resposta == true){
										
					$('#msg_cadastroSUCESSO').show("slow");
					$('.inscritos').hide();
					$('#msg-inscrito').html("");
					$("#carregando_login").css("display", "none");
				
				}
				
				// erro ao efetuar login
				else{
					$('#msg_cadastroERRO').show("slow");
					$('#msg-inscrito').html("");
					$("#carregando_login").css("display", "none");

				}
			});
		
		}
		
		event.preventDefault();
	
	});
