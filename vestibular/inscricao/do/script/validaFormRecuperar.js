
	$("#formRecuperar").submit(function(event) {
		
		// limpo campo mensagem
		$('#msg-inscrito').html("");
		$("#carregando_login").css("display", "none");
		
		// Recupero so valores
		var str_cpf      = $("#str_cpf").val();	
		
		// Considero que a validação esta OK
		var eValido = true;
		
		// Valido os Campos
		
		if (!str_cpf)	{ eValido = false; $("#str_cpf").css("border", "1px solid red"); $("#str_cpf").focus(); $('#msg-inscrito').html("* Informe o CPF");} 
		
		else{
			// ele não é nulo
			$("#str_cpf").css("border", "1px solid #afafaf");
		
			// verifico se ele é valido
			var value = str_cpf;
			value = value.replace('.','');  
			value = value.replace('.','');  
			cpf = value.replace('-','');  
			while(cpf.length < 11) cpf = "0"+ cpf;  
			var expReg = /^0+$|^1+$|^2+$|^3+$|^4+$|^5+$|^6+$|^7+$|^8+$|^9+$/;  
			var a = [];  
			var b = new Number;  
			var c = 11;  
			for (i=0; i<11; i++){  
				a[i] = cpf.charAt(i);  
				if (i < 9) b += (a[i] * --c);  
			}  
			if ((x = b % 11) < 2) { a[9] = 0 } else { a[9] = 11-x }  
			b = 0;  
			c = 11;  
			for (y=0; y<10; y++) b += (a[y] * c--);  
			if ((x = b % 11) < 2) { a[10] = 0; } else { a[10] = 11-x; }  
			if ((cpf.charAt(9) != a[9]) || (cpf.charAt(10) != a[10]) || cpf.match(expReg)){ 
				eValido = false; $("#str_cpf").css("border", "1px solid red");
				$('#msg-inscrito').html("* CPF Inv&aacute;lido");
			} 
			else{
				$("#str_cpf").css("border", "1px solid #afafaf");	
				$('#msg-inscrito').html("");
			}
		
		}
		

		// Se for valido então salvo no banco
		if(eValido){
			
			$("#carregando_login").css("display", "inline");
			
			$.post('do/crud/validaRecuperar.php', {str_cpf: str_cpf}, function(resposta) {
					
				if(resposta == true){
									
					$('#msg_cadastroSUCESSO').show("slow");
					$('.inscritos').hide();
					$('#msg-inscrito').html("");
					$("#carregando_login").css("display", "none");
				
				}
				
				// erro ao efetuar login
				else{
				
					$('#msg-inscrito').html(resposta);
					$("#carregando_login").css("display", "none");

				}
			});
		
		}
		
		event.preventDefault();
	
	});
