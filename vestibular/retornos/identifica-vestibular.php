<?php
require_once 'funcoes.php';
// abre o diretorio dos arquivos
$diretorio = opendir("./conteiner/");
// quantos arquivos foram identificados
$total_identificados = 0;
// arquivos lidos
$arquivos_lidos = array();

function limpa($string){
	return trim($string);
}

// para cada item neste diretório
while ($item = readdir($diretorio)) {
    // se não for um diretório
    if ((is_dir($item) != true) && (substr($item, -4, 4) == '.RET')) {
        // apaga o lixo das variáveis
        unset($comando, $consulta, $dados, $contagem, $contador, $retorno);
        // ve se o item já não existe
        $comando = "SELECT str_arquivo FROM proBoletosRetorno WHERE str_arquivo = '" . $item . "';";
        // executa com sucesso
        #$consulta = mysql_query($comando, $dbintranet) or die(mysql_get_last_message());
        $consulta = mysql_query($comando);
        // se não houver o registro
        if (mysql_num_rows($consulta) < 1) {
		
            // incrementa
            $total_identificados++;
            // mostra o arquivo
            #print '<p> - Identificado: '.$item.'</p>';
            $arquivos_lidos[] = $item;
            // abre o arquivo
            $retorno = fopen("./conteiner/" . $item, "r");
            // se o arquivo foi aberto
            if ($retorno != false) {
                // seta o contador de registros como zero
                $contador = 0;
                // seta a matriz
                $vetor_pagamentos = array();
                // para cada linha do arquivo enquanto não for o final do arquivo
                while (feof($retorno) != true) {
                    // guarda a linha
                    $linha_retorno = trim(fgets($retorno, 512));
                    // se a linha possuir algo
                    if (limpa($linha_retorno) != '') {
                        // recupera a linha tratada
                        $linha_retorno = retornoItau($linha_retorno);
                        // se tratou a linha
                        if (isset($linha_retorno['Tipo de Registro']) != false) {
                            // se for retorno
                            if ($linha_retorno['Tipo de Registro'] == '1') {
                                // conta o retorno
                                $contador++;
                                // guarda as informações
                                $vetor_pagamentos[] = $linha_retorno;
                            }
                            // se for retorno
                            else if ($linha_retorno['Tipo de Registro'] == '0') {
                                // recupera a data da geracao
                                $arquivo_data = $linha_retorno['Data de Geração'];
                            }
                        }
                    }
                }
                // remonta a data
                $arquivo_data = substr($arquivo_data, 4, 2) . '-' . substr($arquivo_data, 2, 2) . '-' . substr($arquivo_data, 0, 2);
                // se for 2000
                if (intval(substr($arquivo_data, 0, 2)) < 70) {
                    $arquivo_data = '20' . $arquivo_data;
                }
                // caso seja 1900
                else {
                    $arquivo_data = '19' . $arquivo_data;
                }
                /*
                  INSERE O ARQUIVO NO BANCO
                 */
                $comando = "INSERT INTO proBoletosRetorno (str_arquivo, dt_arquivo, num_pagamentos, dt_identificado_em) VALUES ('" . $item . "', '" . $arquivo_data . "', '" . $contador . "', CURRENT_TIMESTAMP);";
                // executa com sucesso
                $consulta = mysql_query($comando);
                #$consulta = mysql_query($comando, $dbintranet) or die(mysql_get_last_message() . '<pre>' . $comando . '</pre>');
                // apaga os dados
                unset($comando, $consulta, $dados, $contagem);
                /*
                  SELECIONA O ID DO ARQUIVO INSERIDO NO BANCO
                 */
                // seleciona o índice do arquivo adicionado
                $comando = "SELECT id_arquivoretorno AS id FROM proBoletosRetorno WHERE str_arquivo = '" . $item . "';";
                // executa com sucesso
                $consulta = mysql_query($comando);
                #$consulta = mysql_query($comando, $dbintranet) or die(mysql_get_last_message());
                // seleciona os dados
                $dados = mysql_fetch_assoc($consulta);
                #$dados = mysql_fetch_assoc($consulta);
                // salva o arquivo
                $id_arquivo = $dados['id'];
                // apaga os dados
                unset($comando, $consulta, $dados, $contagem);
                // para cada registro na matriz
                foreach ($vetor_pagamentos as $transacao) {
                    // seleciona o índice do arquivo adicionado
                    $comando = "SELECT id_boleto AS id FROM proBoletos WHERE id_inscricao = '" . $transacao['Nosso Número'] . "';";
                    // executa com sucesso
                    $consulta = mysql_query($comando);
                    #$consulta = mysql_query($comando, $dbintranet) or die(mysql_get_last_message());
                    // se houver boletos
                    if (mysql_num_rows($consulta) > 0) {
                        // seleciona os dados
                        #$dados = mysql_fetch_assoc($consulta);
                        $dados = mysql_fetch_assoc($consulta);
                        // salva o arquivo
                        $id_boleto = $dados['id'];
                        // apaga os dados
                        unset($comando, $consulta, $dados, $contagem);
                        /*
                          INSERE OS PAGAMENTOS NO BANCO
                         */
                        // corrige a data
                        $transacao['Data Crédito'] = substr($transacao['Data Crédito'], 4, 2) . '-' . substr($transacao['Data Crédito'], 2, 2) . '-' . substr($transacao['Data Crédito'], 0, 2);
                        // se for 2000
                        if (intval(substr($transacao['Data Crédito'], 0, 2)) < 70) {
                            $transacao['Data Crédito'] = '20' . $transacao['Data Crédito'];
                        }
                        // caso seja 1900
                        else {
                            $transacao['Data Crédito'] = '19' . $transacao['Data Crédito'];
                        }
                        // insere o arquivo no banco
                        $comando = "
                            INSERT INTO proBoletosPagamentos (
                                id_arquivoretorno,
                                id_boleto,
                                num_inscricao,
                                val_valor_pago,
                                str_banco_codigo,
                                str_banco_agencia,
                                str_banco_dac,
                                val_valor_credito,
                                dt_pagamento,
                                str_codigoliquidacao,
                                str_numerosequencial,
                                dt_identificado_em
                            )
                            VALUES (
                                " . $id_arquivo . ",
                                " . $id_boleto . ",
                                " . $transacao['Nosso Número'] . ",
                                " . $transacao['Valor do Título'] . ",
                                " . $transacao['Código do Banco'] . ",
                                " . $transacao['Agência Cobradora'] . ",
                                " . $transacao['Dac Agência Cobradora'] . ",
                                " . $transacao['Valor principal'] . ",
                                '" . $transacao['Data Crédito'] . "',
                                '" . $transacao['Código de Liquidação'] . "',
                                '" . $transacao['Número Sequencial'] . "',
                                CURRENT_TIMESTAMP
                            );";

                        // executa com sucesso
                        #$consulta = mysql_query($comando, $dbintranet)
                                // ou encerra com erro
                                #or die(mysql_get_last_message() . '<pre>' . $comando . '</pre>');
                        $consulta = mysql_query($comando);
                        // apaga os dados
                        unset($comando, $consulta, $dados, $contagem);
                    }
                }
                // fecha o arquivo
                fclose($retorno);
            }
        }
    }
}
if ($total_identificados > 0) {
   ?>
<p><h3>VESTIBULAT: Arquivos processados!</h3></p>
    <?php
    $conta = 0;
    foreach ($arquivos_lidos as $arquivo) {
        print '<p>' . ++$conta . ' - <b>' . $arquivo . '</b></p>';
    }
} else {
    ?>
    <br/><p><h3>VESTIBULAR: Sem arquivos novos para identificar!</h3></p><br/>
    <?php
}
?>