<?php
    // função que gera o vetor do cabeçalho
    function retornoItau($linha_inteira) {
        // chave que diz o tipo do registro
        $tipo_registro = trim(substr($linha_inteira, 0, 1));
        // chaveia o tipo
        switch ($tipo_registro) {
            // caso seja cabeçalho
            case '0':
                // dados da linha
                $vetor_registro = array(
                    "Tipo de Registro"              => trim(substr($linha_inteira, 0, 1)),
                    "Código de Retorno"             => trim(substr($linha_inteira, 1, 1)),
                    "Literal de Retorno"            => trim(substr($linha_inteira, 2, 7)),
                    "Código do Serviço"             => trim(substr($linha_inteira, 9, 2)),
                    "Literal do Serviço"            => trim(substr($linha_inteira, 11, 15)),
                    "Agência"                       => trim(substr($linha_inteira, 26, 4)),
                    "Zeros"                         => trim(substr($linha_inteira, 30, 2)),
                    "Conta"                         => trim(substr($linha_inteira, 32, 5)),
                    "DAC"                           => trim(substr($linha_inteira, 37, 1)),
                    "Brancos A"                     => trim(substr($linha_inteira, 38, 8)),
                    "Nome da Empresa"               => trim(substr($linha_inteira, 46, 30)),
                    "Código do Banco"               => trim(substr($linha_inteira, 76, 3)),
                    "Nome do Banco"                 => trim(substr($linha_inteira, 79, 15)),
                    "Data de Geração"               => trim(substr($linha_inteira, 94, 6)),
                    "Densidade"                     => trim(substr($linha_inteira, 100, 5)),
                    "Unidade de Densidade"          => trim(substr($linha_inteira, 105, 3)),
                    "Número Sequencial Arq. Ret."   => trim(substr($linha_inteira, 108, 5)),
                    "Data de Crédito"               => trim(substr($linha_inteira, 113, 6)),
                    "Brancos B"                     => trim(substr($linha_inteira, 119, 275)),
                    "Número Sequêncial"             => trim(substr($linha_inteira, 394, 6))
                );
                // encerra o caso
                break;
            // caso seja transação
            case '1':
                // dados da linha
                $vetor_registro = array(
                    "Tipo de Registro"      => trim(substr($linha_inteira, 0, 1)), 
                    "Código de Inscrição"   => trim(substr($linha_inteira, 1, 2)),
                    "Número de Inscrição"   => trim(substr($linha_inteira, 3, 14)),
                    "Agência"               => trim(substr($linha_inteira, 17, 4)),
                    "Zeros A"               => trim(substr($linha_inteira, 21, 2)),
                    "Conta"                 => trim(substr($linha_inteira, 23, 5)),
                    "Dac"                   => trim(substr($linha_inteira, 28, 1)),
                    "Brancos A"             => trim(substr($linha_inteira, 29, 8)),
                    "Uso da Empresa"        => trim(substr($linha_inteira, 37, 25)),
                    "Nosso Número"          => trim(substr($linha_inteira, 62, 8)),
                    "Brancos B"             => trim(substr($linha_inteira, 70, 12)),
                    "Carteira"              => trim(substr($linha_inteira, 82, 3)),
                    "Nosso Número"          => trim(substr($linha_inteira, 85, 8)),
                    "Dac Nosso Número"      => trim(substr($linha_inteira, 93, 1)),
                    "Brancos C"             => trim(substr($linha_inteira, 94, 13)),
                    "Carteira"              => trim(substr($linha_inteira, 107, 1)),
                    "Código de Ocorrência"  => trim(substr($linha_inteira, 108, 2)), 
                    "Data de Ocorrência"    => trim(substr($linha_inteira, 110, 6)),
                    "Número do Documento"   => trim(substr($linha_inteira, 116, 10)),
                    "Nosso Número"          => trim(substr($linha_inteira, 126, 8)),
                    "Brancos D"             => trim(substr($linha_inteira, 134, 12)),
                    "Vencimento"            => trim(substr($linha_inteira, 146, 6)),
                    "Valor do Título"       => trim(substr($linha_inteira, 152, 13)),
                    "Código do Banco"       => trim(substr($linha_inteira, 165, 3)),
                    "Agência Cobradora"     => trim(substr($linha_inteira, 168, 4)),
                    "Dac Agência Cobradora" => trim(substr($linha_inteira, 172, 1)),  
                    "Espécie"               => trim(substr($linha_inteira, 173, 2)),
                    "Tarifa de Cobrança"    => trim(substr($linha_inteira, 175, 13)), 
                    "Brancos E"             => trim(substr($linha_inteira, 188, 26)),
                    "Valor do Iof"          => trim(substr($linha_inteira, 214, 13)),
                    "Valor Abatimento"      => trim(substr($linha_inteira, 227, 13)),
                    "Descontos"             => trim(substr($linha_inteira, 240, 13)),
                    "Valor principal"       => trim(substr($linha_inteira, 253, 13)),
                    "Juros de Mora/Multa"   => trim(substr($linha_inteira, 266, 13)),
                    "Outros Créditos"       => trim(substr($linha_inteira, 279, 13)),
                    "Brancos F"             => trim(substr($linha_inteira, 292, 3)),
                    "Data Crédito"          => trim(substr($linha_inteira, 295, 6)),
                    "Instr. Cancelada"      => trim(substr($linha_inteira, 301, 4)),
                    "Brancos G"             => trim(substr($linha_inteira, 305, 6)),
                    "Zeros B"               => trim(substr($linha_inteira, 311, 13)),
                    "Nome do Sacado"        => trim(substr($linha_inteira, 324, 30)),
                    "Brancos H"             => trim(substr($linha_inteira, 354, 23)),
                    "Erros"                 => trim(substr($linha_inteira, 377, 8)),
                    "Brancos I"             => trim(substr($linha_inteira, 385, 7)),
                    "Código de Liquidação"  => trim(substr($linha_inteira, 392, 2)),
                    "Número Sequencial"     => trim(substr($linha_inteira, 394, 6))
                );
                // encerra o caso
                break;
            case '9':
                // dados da linha
                $vetor_registro = array(
                    "Tipo de Registro"          => trim(substr($linha_inteira, 0, 1)), 
                    "Código de retorno"         => trim(substr($linha_inteira, 1, 2)),
                    "Código de Serviço"         => trim(substr($linha_inteira, 3, 14)),
                    "Código do Banco"           => trim(substr($linha_inteira, 7, 4)),
                    "Brancos A"                 => trim(substr($linha_inteira, 1, 2)),
                    "Quantidade de Títulos"     => trim(substr($linha_inteira, 3, 5)),
                    "Valor Total"               => trim(substr($linha_inteira, 0, 0)),
                    "Aviso Bancário"            => trim(substr($linha_inteira, 0, 0)),
                    "Brancos B"                 => trim(substr($linha_inteira, 0, 0)),
                    "Quantidade de Títulos"     => trim(substr($linha_inteira, 0, 0)),
                    "Valor Total"               => trim(substr($linha_inteira, 0, 0)),
                    "Aviso Bancário"            => trim(substr($linha_inteira, 0, 0)),
                    "Brancos C"                 => trim(substr($linha_inteira, 0, 0)),
                    "Quantidade de Títulos"     => trim(substr($linha_inteira, 0, 0)),
                    "Valor Total"               => trim(substr($linha_inteira, 0, 0)),
                    "Aviso Bancário"            => trim(substr($linha_inteira, 0, 0)),
                    "Controle do Arquivo"       => trim(substr($linha_inteira, 0, 0)),
                    "Quantidade de Detalhes"    => trim(substr($linha_inteira, 0, 0)),
                    "Valor Total Informado"     => trim(substr($linha_inteira, 0, 0)),
                    "Brancos D"                 => trim(substr($linha_inteira, 0, 0)),
                    "Número Sequencial"         => trim(substr($linha_inteira, 0, 0))
                );
                // encerra o caso
                break;
            default:
                // mantém a linha original
                $vetor_registro = $linha_inteira;
        }
        /* 
        PAG 17
            "Tipo de Registro" => trim(substr($linha_inteira, 0, 1)),
            "Código de Inscrição" => trim(substr($linha_inteira, 0, 1)),
            "Número de Inscrição" => trim(substr($linha_inteira, 0, 1)),
            "Agência" => trim(substr($linha_inteira, 0, 1)),
            "Zeros" => trim(substr($linha_inteira, 0, 1)),
            "Conta" => trim(substr($linha_inteira, 0, 1)),
            "Dac" => trim(substr($linha_inteira, 0, 1)),
            "Brancos" => trim(substr($linha_inteira, 0, 1)),
            "Uso da Empresa" => trim(substr($linha_inteira, 0, 1)),
            "Nosso Número" => trim(substr($linha_inteira, 0, 1)),
            "Agência Conta do Cheque" => trim(substr($linha_inteira, 0, 1)),
            "Carteira" => trim(substr($linha_inteira, 0, 1)),
            "Nosso Número" => trim(substr($linha_inteira, 0, 1)),
            "Dac Nosso Núemro" => trim(substr($linha_inteira, 0, 1)),
            "Brancos" => trim(substr($linha_inteira, 0, 1)),
            "Carteira" => trim(substr($linha_inteira, 0, 1)),
            "Código de Ocorrência" => trim(substr($linha_inteira, 0, 1)),
            "Data de Ocorrência" => trim(substr($linha_inteira, 0, 1)),
            "Número do Documento" => trim(substr($linha_inteira, 0, 1)),
            "Nosso Número" => trim(substr($linha_inteira, 0, 1)),
            "Brancos" => trim(substr($linha_inteira, 0, 1)),
            "Zeros" => trim(substr($linha_inteira, 0, 1)),
            "Valor do Título" => trim(substr($linha_inteira, 0, 1)),
            "Código do Banco" => trim(substr($linha_inteira, 0, 1)),
            "Agência Cobradora" => trim(substr($linha_inteira, 0, 1)),
            "Dac Agência Cobradora" => trim(substr($linha_inteira, 0, 1)),
            "Brancos" => trim(substr($linha_inteira, 0, 1)),
            "Zeros" => trim(substr($linha_inteira, 0, 1)),
            "Valor do Cheque" => trim(substr($linha_inteira, 0, 1)),
            "Zeros" => trim(substr($linha_inteira, 0, 1)),
            "Brancos" => trim(substr($linha_inteira, 0, 1)),
            "Zeros" => trim(substr($linha_inteira, 0, 1)),
            "Banda Magnética" => trim(substr($linha_inteira, 0, 1)),
            "Brancos" => trim(substr($linha_inteira, 0, 1)),
            "Número Sequencial" => trim(substr($linha_inteira, 0, 1)),
            "Registro" => trim(substr($linha_inteira, 0, 1)),
            "Registro" => trim(substr($linha_inteira, 0, 1)),
        */
        // retorna o vetor gerado
        return $vetor_registro;
    }
?>