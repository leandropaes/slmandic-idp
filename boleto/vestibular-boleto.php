
<?php
	if ((strlen(trim($_GET['inscricao'])) <= 10) || (!isset($_GET['inscricao']))) {
		//header('location: http://idpsp.edu.br/');
		exit('Sem permiss�o');
	}
	

	require_once('../vestibular/inscricao/do/conectar.php');
	

	$vetor = explode(',', $_GET['inscricao']);
	
	$id_processo   = $vetor[1];
	$id_inscricao  = $vetor[2];
	$id_boleto 	   = $vetor[4];

	
	// comando para consultar os boletos de uma matr�cula em um curso
	$query = "SELECT * FROM proBoletos WHERE id_boleto = '".$id_boleto."';";
	
	$resultado = mysql_query($query, $conectar);
	
	if(!mysql_num_rows($resultado)){
		//header('location: http://idpsp.edu.br/');
		exit('Boleto n�o encontrado');	
	}
	
	// Recupero os dados do Boleto
	$boleto = mysql_fetch_array($resultado);

	
	foreach ($boleto as $chave=>$valor){
		$boleto[$chave] = htmlentities(utf8_decode(trim($valor)));
	}
	
	// s� a data
	$boleto['dt_vencimento'] = substr($boleto['dt_vencimento'], 0, 10);
	$boleto['dt_gerado_em'] = substr($boleto['dt_gerado_em'], 0, 10);
	$boleto['dt_ultimavia'] = substr($boleto['dt_ultimavia'], 0, 10);
	// com v�rgula decimal, sem separador de milhares
	$boleto['val_valor'] = number_format($boleto['val_valor'], 2, ',', '');
	
	$query = "SELECT * FROM proInscricoes WHERE id_inscricao = '".$id_inscricao."';";
	
	$resultado = mysql_query($query, $conectar);
	
	if(!mysql_num_rows($resultado)){
		#header('location: http://www.slmandic.edu.br');
		exit('Cliente n�o encontrado');
	}

	$inscricao = mysql_fetch_array($resultado);
	
	foreach ($inscricao as $chave=>$valor) {
		$inscricao[$chave] = htmlentities(utf8_decode(trim($valor)));
	}
	//------- Recupera informacoes da pessoa -----------//

	$query = "SELECT Pf.* FROM proInscricoes Ins INNER JOIN proPessoasFisicas Pf ON Ins.id_pessoafisica = Pf.id_pessoafisica AND Ins.id_inscricao='".$id_inscricao."';";

	$resultado = mysql_query($query, $conectar);
	
	if(!mysql_num_rows($resultado)){
		#header('location: http://www.slmandic.edu.br');
		exit('cliente n�o encontrado 2');
	}

	$PessoaInscricao = mysql_fetch_array($resultado);

	foreach ($PessoaInscricao as $chave=>$valor) {
		$PessoaInscricao[$chave] = htmlentities(utf8_decode(trim($valor)));
	}
	//-------------
	$query = "SELECT * FROM proProcessos WHERE id_processo = '".$id_processo."';";
	
	$resultado = mysql_query($query, $conectar);
	
	if(!mysql_num_rows($resultado)){
		#header('location: http://www.slmandic.edu.br');
		exit('vestibular n�o encontrado 3');
	}

	$vestibular = mysql_fetch_array($resultado);
	/*
	foreach ($vestibular as $chave=>$valor) {
		$vestibular[$chave] = htmlentities(utf8_decode(trim($valor)));
	}*/
    
  	
	$endereco_linha1 = $PessoaInscricao['str_endereco_logradouro'];
	if (strlen(trim($PessoaInscricao['str_endereco_numero'])) > 0) {
		$endereco_linha1 .= ', '.$PessoaInscricao['str_endereco_numero'];
	}
	if (strlen(trim($PessoaInscricao['str_endereco_complemento'])) > 0) {
		$endereco_linha1 .= ' - '.$PessoaInscricao['str_endereco_complemento'];
	}
	
	$endereco_linha2  = $PessoaInscricao['str_endereco_bairro'];
	$endereco_linha2 .= ' - '.$PessoaInscricao['str_endereco_cidade'];
	$endereco_linha2 .= '/'.$PessoaInscricao['str_endereco_estado'];
	$endereco_linha2 .= ' CEP: '.$PessoaInscricao['str_endereco_cep'];
	
// +----------------------------------------------------------------------+
// | BoletoPhp - Vers�o Beta                                              |
// +----------------------------------------------------------------------+
// | Este arquivo est� dispon�vel sob a Licen�a GPL dispon�vel pela Web   |
// | em http://pt.wikipedia.org/wiki/GNU_General_Public_License           |
// | Voc� deve ter recebido uma c�pia da GNU Public License junto com     |
// | esse pacote; se n�o, escreva para:                                   |
// |                                                                      |
// | Free Software Foundation, Inc.                                       |
// | 59 Temple Place - Suite 330                                          |
// | Boston, MA 02111-1307, USA.                                          |
// +----------------------------------------------------------------------+

// +----------------------------------------------------------------------+
// | Originado do Projeto BBBoletoFree que tiveram colabora��es de Daniel |
// | William Schultz e Leandro Maniezo que por sua vez foi derivado do	  |
// | PHPBoleto de Jo�o Prado Maia e Pablo Martins F. Costa				        |
// | 														                                   			  |
// | Se vc quer colaborar, nos ajude a desenvolver p/ os demais bancos :-)|
// | Acesse o site do Projeto BoletoPhp: www.boletophp.com.br             |
// +----------------------------------------------------------------------+

// +----------------------------------------------------------------------+
// | Equipe Coordena��o Projeto BoletoPhp: <boletophp@boletophp.com.br>   |
// | Desenvolvimento Boleto Ita�: Glauber Portella                        |
// +----------------------------------------------------------------------+

// DADOS DO BOLETO
$dadosboleto["nosso_numero"] = $inscricao['num_inscricao'];  // Nosso numero - REGRA: 8 caracteres!
$dadosboleto["numero_documento"] = substr($inscricao['num_inscricao'], -4);	// Num do pedido ou nosso numero - REGRA: 4 caracteres!
$dadosboleto["data_vencimento"] = implode('/', array_reverse(explode('-', $boleto['dt_vencimento']))); // Data de Vencimento do Boleto - REGRA: Formato DD/MM/AAAA
$dadosboleto["data_documento"] = implode('/', array_reverse(explode('-', $boleto['dt_gerado_em']))); // Data de emiss�o do Boleto
$dadosboleto["data_processamento"] = implode('/', array_reverse(explode('-', $boleto['dt_ultimavia']))); // Data de processamento do boleto (opcional) (ultima via)
$dadosboleto["valor_boleto"] = $boleto['val_valor']; 	// Valor do Boleto - REGRA: Com v�rgula e sempre com duas casas depois da virgula

// DADOS DO SEU CLIENTE
$dadosboleto["sacado"] = $PessoaInscricao['str_pessoa_nome']; // Nome do Cliente
$dadosboleto["endereco1"] = $endereco_linha1; // Endere�o do seu Cliente
$dadosboleto["endereco2"] = $endereco_linha2; // Cidade - Estado -  CEP: 00000-000

// DADOS DA SUA CONTA - ITA�
$dadosboleto["agencia"] = $vestibular['str_banco_agencia']; // Num da agencia, sem digito
$dadosboleto["conta"] = $vestibular['str_banco_conta']; // Num da conta, sem digito
$dadosboleto["conta_dv"] = $vestibular['str_banco_conta_dv']; // Digito do Num da conta

// DADOS PERSONALIZADOS - ITA�
$dadosboleto["carteira"] = $vestibular['str_banco_carteira']; // C�digo da Carteira: pode ser 175, 174, 104, 109, 178, ou 157

// INFORMACOES PARA O CLIENTE
$dadosboleto["demonstrativo1"] = $vestibular['str_boleto_demonstrativo_linha1'];
$dadosboleto["demonstrativo2"] = $vestibular['str_boleto_demonstrativo_linha2'];
$dadosboleto["demonstrativo3"] = $vestibular['str_boleto_demonstrativo_linha3'];

// INSTRU��ES (TEXTO DE RESPONSABILIDADE DO CEDENTE)
$dadosboleto["instrucoes1"] = $vestibular['str_boleto_instrucao_linha1'];
$dadosboleto["instrucoes2"] = $vestibular['str_boleto_instrucao_linha2'];
$dadosboleto["instrucoes3"] = $vestibular['str_boleto_instrucao_linha3'];
$dadosboleto["instrucoes4"] = $vestibular['str_boleto_instrucao_linha4'];



$substituicoes = array(
    '{numero-da-inscricao}' => $inscricao['num_inscricao'],
    '{data-inscricoes-fim}' => implode('/', array_reverse(explode('-', substr($vestibular['dt_inscricao_fim'], 0, 10)))),
    '{data-divulgacao-local-prova}' => implode('/', array_reverse(explode('-', substr($vestibular['dt_liberacao_local'], 0, 10)))),
    '{data-prova-primeiro-dia}' => implode('/', array_reverse(explode('-', substr($datadaprova['dt_inicio'], 0, 10)))),
    '{hora-prova-primeiro-dia}' => substr($datadaprova['dt_inicio'], 11, 5)
);


foreach ($substituicoes as $procurar=>$substituir) {
    $dadosboleto["instrucoes1"] = str_ireplace($procurar, $substituir, $dadosboleto["instrucoes1"]);
    $dadosboleto["instrucoes2"] = str_ireplace($procurar, $substituir, $dadosboleto["instrucoes2"]);
    $dadosboleto["instrucoes3"] = str_ireplace($procurar, $substituir, $dadosboleto["instrucoes3"]);
    $dadosboleto["instrucoes4"] = str_ireplace($procurar, $substituir, $dadosboleto["instrucoes4"]);
}




// SEUS DADOS
$dadosboleto["identificacao"] = $vestibular['str_cedente_identificacao']; #Identifica��o do Boleto
$dadosboleto["cpf_cnpj"] = $vestibular['str_cedente_cpfcnpj']; #CNPJ
$dadosboleto["endereco"] = $vestibular['str_cedente_endereco']; #Endere�o da Empresa
$dadosboleto["cidade_uf"] = $vestibular['str_cedente_cidade'].' / '.$vestibular['str_cedente_estado']; #Cidade / Estado
$dadosboleto["cedente"] = $vestibular['str_cedente_razaosocial']; #Raz�o Social

// DADOS OPCIONAIS DE ACORDO COM O BANCO OU CLIENTE
$dadosboleto["quantidade"] = "";
$dadosboleto["valor_unitario"] = "";
$dadosboleto["aceite"] = "N";		
$dadosboleto["especie"] = "R$";
$dadosboleto["especie_doc"] = "DM";

include("include/funcoes_itau.php"); 
include("include/layout_itau.php");

?>
<script type="text/javascript">
	window.load = function() {
		window.print();
	};
</script>
